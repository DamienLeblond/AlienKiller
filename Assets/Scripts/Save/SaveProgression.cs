using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class SaveProgressionData
{
    public int scene;
    public int turn;
    public List<PlayerData> playerDataList = new();
    public List<bool> roomsCleared = new();
    public List<bool> roomsEntered = new();
    public List<bool> roomsExplored = new();
    public List<bool> availableChests = new();
}

[System.Serializable]
public class PlayerData
{
    public string playerName;
    public Vector3 playerPosition;

    public PlayerData(string playerName, Vector3 playerPosition)
    {
        this.playerName = playerName;
        this.playerPosition = playerPosition;
    }
}

public class SaveProgression : ScriptableObject
{
    private string saveFileName = "progression.json";


    public void SaveProgressionData(SaveProgressionData progressionData)
    {
        string saveFilePath = Path.Combine(Application.persistentDataPath, saveFileName);
        string json = JsonUtility.ToJson(progressionData);
        File.WriteAllText(saveFilePath, json);
    }

    public SaveProgressionData LoadProgressionData()
    {
        string saveFilePath = Path.Combine(Application.persistentDataPath, saveFileName);
        Debug.Log(saveFilePath);

        if (File.Exists(saveFilePath))
        {
            string json = File.ReadAllText(saveFilePath);
            return JsonUtility.FromJson<SaveProgressionData>(json);
        }
        else
        {
            Debug.LogWarning("Progression file not found: " + saveFilePath);
            return null;
        }
    }

    public void DeleteProgressionFile()
    {
        string saveFilePath = Path.Combine(Application.persistentDataPath, saveFileName);
        if (File.Exists(saveFilePath))
        {
            File.Delete(saveFilePath);
            Debug.Log("Progression file deleted: " + saveFilePath);
        }
        else
        {
            Debug.LogWarning("Progression file does not exist: " + saveFilePath);
        }
    }
}
