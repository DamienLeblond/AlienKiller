using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProgressionEvents : MonoBehaviour
{
    public static UnityEvent OnProgressionLoaded = new UnityEvent();
}
