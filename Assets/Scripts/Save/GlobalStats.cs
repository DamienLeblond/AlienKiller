using System.Globalization;
using System.IO;
using UnityEngine;

[System.Serializable]
public class GlobalStatsData
{
    public int shotsFired;
    public int bulletsHit;
    public int bulletsMissed;
    public int crit;
    public int enemyDefeated;
    public int hpRecovered;
    public int totalDamageDealt;
    public int highestDamageDealt;
    public int totalDamageReceived;

    public GlobalStatsData(int shotsFired, int bulletsHit, int bulletsMissed, int crit, int enemyDefeated, int hpRecovered, int totalDamageDealt, int highestDamageDealt, int totalDamageReceived)
    {
        this.shotsFired = shotsFired;
        this.bulletsHit = bulletsHit;
        this.bulletsMissed = bulletsMissed;
        this.crit = crit;
        this.enemyDefeated = enemyDefeated;
        this.hpRecovered = hpRecovered;
        this.totalDamageDealt = totalDamageDealt;
        this.highestDamageDealt = highestDamageDealt;
        this.totalDamageReceived = totalDamageReceived;
    }
}

public class GlobalStats
{
    private string saveFileName = "globalStats";
    private string saveFileExtension = ".json";

    public void SaveGlobalStats(GlobalStatsData globalStats, int characterId)
    {
        string saveFilePath = Path.Combine(Application.persistentDataPath, saveFileName + characterId.ToString() + saveFileExtension);
        string json = JsonUtility.ToJson(globalStats);
        File.WriteAllText(saveFilePath, json);
    }

    public GlobalStatsData LoadGlobalStats(int characterId)
    {
        string saveFilePath = Path.Combine(Application.persistentDataPath, saveFileName + characterId.ToString() + saveFileExtension);
        if (File.Exists(saveFilePath))
        {
            string json = File.ReadAllText(saveFilePath);
            return JsonUtility.FromJson<GlobalStatsData>(json);
        }
        else
        {
            Debug.LogWarning("Global stats file not found: " + saveFilePath);
            return new GlobalStatsData(0,0,0,0,0,0,0,0,0);
        }
    }
}
