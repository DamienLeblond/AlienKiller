using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetingHealthBarUpdate : MonoBehaviour
{
    private RectTransform panelRemainingHealth; // R�f�rence au RectTransform du panel PanelRemainingHealth
    private RectTransform panelPotentialDamage;

    private float pulseDuration = 1.5f; // Dur�e d'une boucle de clignotement (en secondes)
    private float minAlpha = 0.4f; // Valeur minimale de l'alpha
    private float maxAlpha = 1f; // Valeur maximale de l'alpha



    public void Awake()
    {
        panelRemainingHealth = gameObject.transform.Find("PanelRemainingHealth").GetComponent<RectTransform>();
        panelPotentialDamage = gameObject.transform.Find("PanelRemainingHealth/PanelPossibleDamage").GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        StartCoroutine(PulseEffect());
    }

    public void UpdateHealthBar(float healthReaminingPercentage, float potentialDamagePercentage)
    {

        // D�finition de la valeur max de l'ancre en x
        panelRemainingHealth.anchorMax = new Vector2(healthReaminingPercentage, panelRemainingHealth.anchorMax.y);

        // R�glage de la propri�t� right de l'ancre en 0
        panelRemainingHealth.offsetMax = new Vector2(0f, panelRemainingHealth.offsetMax.y);

        panelPotentialDamage.anchorMin = new Vector2(potentialDamagePercentage, panelPotentialDamage.anchorMin.y);

        panelPotentialDamage.offsetMin = new Vector2(0f, panelPotentialDamage.offsetMin.y);
    }


    private IEnumerator PulseEffect()
    {
        while (true)
        {
            float t = 0f;
            while (t < pulseDuration)
            {
                // Calcul de l'alpha en utilisant une interpolation douce entre minAlpha et maxAlpha
                float smoothStep = Mathf.SmoothStep(0f, 1f, t / pulseDuration);
                float alpha = Mathf.Lerp(minAlpha, maxAlpha, smoothStep);

                Image image = panelPotentialDamage.GetComponent<Image>();
                Color color = image.color;
                color.a = alpha;
                image.color = color;

                t += Time.deltaTime;
                yield return null;
            }

            // Inverser les valeurs de minAlpha et maxAlpha pour obtenir l'effet de pulse
            float temp = minAlpha;
            minAlpha = maxAlpha;
            maxAlpha = temp;
        }
    }
}
