using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthBarUpdater : MonoBehaviour
{
    private Camera mainCamera; // R�f�rence � la cam�ra principale

    [SerializeField] private EnemyHealth enemyHealth;

    private RectTransform panelRemainingHealth; // R�f�rence au RectTransform du panel PanelRemainingHealth

    private void Start()
    {
        mainCamera = Camera.main;
        panelRemainingHealth = gameObject.transform.Find("PanelRemainingHealth").GetComponent<RectTransform>();
        UpdateValue();
    }

    void Update()
    {
        // Faire en sorte que la barre de vie reste toujours face � la cam�ra
        transform.LookAt(transform.position + mainCamera.transform.rotation * Vector3.forward,
                         mainCamera.transform.rotation * Vector3.up);
    }

    public void UpdateValue()
    {
       float healthReaminingPercentage = enemyHealth.GetRemainingPercentage();

        // D�finition de la valeur max de l'ancre en x
        panelRemainingHealth.anchorMax = new Vector2(healthReaminingPercentage, panelRemainingHealth.anchorMax.y);

        // R�glage de la propri�t� right de l'ancre en 0
        panelRemainingHealth.offsetMax = new Vector2(0f, panelRemainingHealth.offsetMax.y);
    }
}
