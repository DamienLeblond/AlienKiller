using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEffect : MonoBehaviour
{
    public void PlayEffect()
    {
        StartCoroutine(RemoveExplosionOnceEffectIsFullyPlayed());
    }

    private IEnumerator RemoveExplosionOnceEffectIsFullyPlayed()
    {
        GetComponent<AudioSource>().Play();
        GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
