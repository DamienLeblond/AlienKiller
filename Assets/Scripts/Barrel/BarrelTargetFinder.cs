using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelTargetFinder : VisibleTargetFinder
{
    protected override string TargetTag => Harmony.Tags.Barrel;
    private CharacterWeaponInventory characterWeaponInventory;
    private DetectObstacle detectObstacle;

    private void Start()
    {
        characterWeaponInventory = GetComponent<CharacterWeaponInventory>();
        detectObstacle = GetComponent<DetectObstacle>();
    }

    public override void FindVisibleTargets()
    {
        Vector3 position = transform.position;

        float maxRange = characterWeaponInventory.GetCurrentWeaponRange() * tileSize;

        Collider[] hitColliders = Physics.OverlapSphere(position, maxRange);

        visibleTargets = new List<GameObject>();


        foreach (var collider in hitColliders)
        {
            GameObject target = collider.gameObject;

            if (IsTargetValid(target) && !DetectObstacle(position, target) && !target.GetComponent<BarrelEffect>().IsHit)
            {
                AddVisibleTarget(target);
            }
        }
    }

    protected virtual bool DetectObstacle(Vector3 position, GameObject target)
    {
        Vector3 endPoint = target.transform.position;
        return detectObstacle.IsObstacleBetween(position, endPoint);
    }
}
