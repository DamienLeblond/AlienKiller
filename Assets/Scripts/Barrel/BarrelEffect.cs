using Harmony;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class BarrelEffect : MonoBehaviour
{
    private const int MAX_DAMAGE = 300;
    private const int MIN_DAMAGE = 150;
    private GameObject explosionPrefab;
    private GridObject gridObject;
    private CustomGrid targetGrid;
    private ActionBarManager actionBarManager;
    public bool IsHit { get; private set; }

    private void Start()
    {
        IsHit = false;
        explosionPrefab = GameObject.Find("Explosion");
        gridObject = GetComponent<GridObject>();
        targetGrid = FindAnyObjectByType<CustomGrid>();
        actionBarManager = FindAnyObjectByType<ActionBarManager>();
    }

    public int GetPossibleDamage(GridObject targetedGameObject)
    {
        Vector2Int barrelPosition = gridObject.CurrentPositionOnGrid;
        Vector2Int affectedGameObjectPosition = targetedGameObject.CurrentPositionOnGrid;
        if (Mathf.Abs(barrelPosition.x - affectedGameObjectPosition.x) <= 1 && Mathf.Abs(barrelPosition.y - affectedGameObjectPosition.y) <= 1) return MAX_DAMAGE;
        return MIN_DAMAGE;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!IsHit && collision.gameObject.layer == Harmony.Layers.Projectile.Index)
        {
            targetGrid.ResetGridObject(gridObject.CurrentPositionOnGrid);
            IsHit = true;
            GameObject explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            explosion.GetComponent<ExplosionEffect>().PlayEffect();
            gameObject.SetActive(false);
            actionBarManager.InitializeActions();
        }
    }

}
