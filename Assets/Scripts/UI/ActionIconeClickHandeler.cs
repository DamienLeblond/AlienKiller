using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ActionIconeClickHandeler : MonoBehaviour, IPointerClickHandler
{
    public ActionWithImage actionWithImage;

    // M�thode appel�e lorsque l'image est cliqu�e
    public void OnPointerClick(PointerEventData eventData)
    {
        // V�rifiez si l'action associ�e existe
        if (actionWithImage != null && actionWithImage.action != null)
        {
            // Ex�cutez l'action associ�e � l'image
            actionWithImage.action.Invoke();
        }
        else
        {
            Debug.LogError("L'action associ�e � l'image est nulle ou non d�finie.");
        }
    }
}
