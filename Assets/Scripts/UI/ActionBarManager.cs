using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class ActionBarManager : MonoBehaviour
{
    [SerializeField] private CustomGrid targetGrid;
    [SerializeField] private PathFinding pathFinding;
    public Canvas canvas; // R�f�rence au transform du canvas o� placer les images
    [SerializeField] private Sprite runIcon;
    [SerializeField] private Sprite shootEnemyIcon;
    [SerializeField] private Sprite openTreasureIcon; 
    [SerializeField] private Sprite switchWeaponIcon;
    [SerializeField] private Sprite reloadIcon;
    [SerializeField] private Sprite passTurnIcon;
    [SerializeField] private Sprite shootBarrelIcon;
    [SerializeField] private AudioClip switchWeaponClip;
    [SerializeField] private AudioClip reloadClip;
    [SerializeField] private AudioClip passTurnClip;
    [SerializeField] private AudioClip openTreasureClip;
    public static string RUN_ACTION_NAME = "Run";
    public static string SHOOT_ENEMY_ACTION_NAME = "Shoot enemy";
    public static string RELOAD_ACTION_NAME = "Reload";
    public static string PASS_TURN_ACTION_NAME = "Pass Turn";
    public static string OPEN_TREASURE_ACTION_NAME = "Open Treasure";
    public static string SWITCH_WEAPON_ACTION_NAME = "Switch weapon";
    public static string SHOOT_BARREL_ACTION_NAME = "Shoot barrel";
    public static int ACTION_POINTS_TO_RELOAD = 1;
    [SerializeField] private GameObject cursorIndicator;
    [SerializeField] private GameObject buildingSystem;
    [SerializeField] private UIController uIController;
    [SerializeField] private GameManager gameManager;
    [SerializeField] private TextMeshProUGUI hitChanceTxt;
    [SerializeField] private TextMeshProUGUI critChanceTxt;
    [SerializeField] private ActionPoints actionPoints;
    [SerializeField] private AttackManager attackManager;
    [SerializeField] private WeaponAndMunitionUiController weaponAndMunitionUiController;
    [SerializeField] private TargetingHealthBarUpdate healthBarUpdate;
    private PlayerAnimatorManager playerAnimator;
    private CameraEventManager cameraEventManager;
    private List<ActionWithImage> possibleActions = new List<ActionWithImage>();
    private float actionBarHeight = 50f; // Hauteur de la barre d'actions
    private float padding = 10f; // Espacement entre les actions
    private ActionWithImage runAction;
    private ActionWithImage shootEnemyAction;
    private ActionWithImage openTreasureAction;
    private GridObject currentTreasureNearPlayer;
    private ActionWithImage reloadAction;
    private ActionWithImage passTurnAction;
    private ActionWithImage switchWeaponAction;
    private ActionWithImage shootBarrelAction;
    CreateTargetIcon createIcon;
    private string currentAction;
    private GameObject currentPlayer, fogPlane;
    private CharacterWeaponInventory currentCharacterWeaponInventory;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        cameraEventManager = FindAnyObjectByType<CameraEventManager>();
        createIcon = FindFirstObjectByType<CreateTargetIcon>();
        openTreasureAction = new ActionWithImage(OPEN_TREASURE_ACTION_NAME, openTreasureIcon, OpenTreasureAction);
        runAction = new ActionWithImage(RUN_ACTION_NAME, runIcon, RunAction);
        shootEnemyAction = new ActionWithImage(SHOOT_ENEMY_ACTION_NAME, shootEnemyIcon, ShootEnemyAction);
        reloadAction = new ActionWithImage(RELOAD_ACTION_NAME, reloadIcon, ReloadAction);
        passTurnAction = new ActionWithImage(PASS_TURN_ACTION_NAME, passTurnIcon, PassTurnAction);
        switchWeaponAction = new ActionWithImage(SWITCH_WEAPON_ACTION_NAME, switchWeaponIcon, SwitchWeaponAction);
        shootBarrelAction = new ActionWithImage(SHOOT_BARREL_ACTION_NAME, shootBarrelIcon, ShootBarrelAction);
    }

    private void Start()
    {
        InitializeActions();
    }

    // Mthode pour initialiser les actions
    public void InitializeActions()
    {
        currentPlayer = gameManager.GetCurrentPlayerTurn();
        ResetIcons();
        if (currentPlayer.CompareTag(Harmony.Tags.MainCharacter))
        {
            GetAvailableActions();
            CreateActionBarImages();
            CreateActionBarBackground();
            SetNullAction();
        }
    }

    private void ResetIcons()
    {
        foreach (Transform child in canvas.transform)
        {
            // Vrifiez si l'enfant correspond  ce que vous souhaitez dtruire
            if (child.CompareTag(Harmony.Tags.ActionBarImage))
            {
                Destroy(child.gameObject);
            }
        }
    }
    public void GetAvailableActions() //quand une nouvelle action est possible � faire, il faut rajouter les conditions pour la faire
        //exemple, l'action de grenade, il faut avoir la grenade pour pouvoir l'avoir comme option
        //recharger doit manquer des balles dans le fusils pour le faire, etc
    {

        List<ActionWithImage> actionList = new List<ActionWithImage>();
        EnemyTargetFinder currentPlayerTargetFinder = currentPlayer.GetComponent<EnemyTargetFinder>(); 
        currentCharacterWeaponInventory = currentPlayer.GetComponent<CharacterWeaponInventory>();
        BarrelTargetFinder currentPlayerBarrelTargetFinder = currentPlayer.GetComponent<BarrelTargetFinder>();

        if (currentPlayer.GetComponent<Move>().canMove)
        {
            actionList.Add(runAction);//ActionWithImage de courir
        }

        currentPlayerTargetFinder.FindVisibleTargets();
        if (currentPlayerTargetFinder.GetVisibleTargets().Count > 0 && currentCharacterWeaponInventory.MunitionRemaining(0) > 0)
        {
            actionList.Add(shootEnemyAction);//ActionWithImage de tirer
        }

        currentTreasureNearPlayer = pathFinding.GetGameObjectByTagNearSelectedPosition(targetGrid.GetGridPosition(currentPlayer.transform.position), Harmony.Tags.Treasure);
        if (currentTreasureNearPlayer != null)
        {
            actionList.Add(openTreasureAction);

        }

        if(currentCharacterWeaponInventory.IsWeaponAtMaxMunition() == false)
        {
            actionList.Add(reloadAction);
        }

        currentPlayerBarrelTargetFinder.FindVisibleTargets();
        if (currentPlayerBarrelTargetFinder.GetVisibleTargets().Count > 0)
        {
            actionList.Add(shootBarrelAction);
        }


        actionList.Add(switchWeaponAction);
        actionList.Add(passTurnAction);
        possibleActions = actionList;
    }

    public string GetCurrentAction() { return currentAction; }

    private void CreateActionBarBackground()
    {
        // Cr�ez la barre
        GameObject actionBarBackground = new("ActionBarBackground");
        RectTransform barRectTransform = actionBarBackground.AddComponent<RectTransform>();
        Image backgroundImage = actionBarBackground.AddComponent<Image>();
        actionBarBackground.tag = Harmony.Tags.ActionBarImage;

        // Changez la couleur du barre
        backgroundImage.color = Color.black;

        // Calculez la largeur totale en fonction du nombre d'actions disponibles
        float totalWidth = possibleActions.Count * (actionBarHeight + padding) - padding;

        // D�finissez la taille de la barre
        barRectTransform.sizeDelta = new Vector2(totalWidth * 1.3f, actionBarHeight * 2f);

        // D�finissez la position de la barre dans le canvas 
        barRectTransform.SetParent(canvas.transform, false);
        barRectTransform.transform.position = new Vector2(Screen.width / 2, 50);

        // Assurez que la barre est affich�e derri�re les ic�nes
        barRectTransform.SetAsFirstSibling();

    }


    private void CreateActionBarImages()
    {

        // Obtenez la taille de l'�cran
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        // Calculez la position de dpart en Y pour placer les images au bas de l'cran
        float startY = actionBarHeight / 2f;

        // Calculez la position en X pour centrer les images horizontalement
        float totalWidth = possibleActions.Count * (actionBarHeight + padding) - padding;
        float startX = -totalWidth / 2f + actionBarHeight / 2f; 

        // Parcourez la liste des actions pour crer et placer les images dans le canvas
        for (int i = 0; i < possibleActions.Count; i++)
        {
            // Crez un GameObject pour reprsenter l'image d'action
            GameObject actionGameObject = new GameObject(possibleActions[i].actionName);
            RectTransform imageRectTransform = actionGameObject.AddComponent<RectTransform>();
            Image actionImage = actionGameObject.AddComponent<Image>();

            // Assurez-vous que l'image est centre horizontalement et aligne en bas
            imageRectTransform.anchorMin = new Vector2(0.5f, 0);
            imageRectTransform.anchorMax = new Vector2(0.5f, 0);
            imageRectTransform.pivot = new Vector2(0.5f, 0);

            // Dfinissez la taille de l'image
            imageRectTransform.sizeDelta = new Vector2(actionBarHeight, actionBarHeight);

            // Attribuez le sprite de l'action  l'image
            actionImage.sprite = possibleActions[i].actionImage;

            actionGameObject.tag = Harmony.Tags.ActionBarImage;

            // Ajoutez le script ImageClickHandler  l'image et attribuez-lui l'action associe
            ActionIconeClickHandeler clickHandler = actionGameObject.AddComponent<ActionIconeClickHandeler>();
            clickHandler.actionWithImage = possibleActions[i];

            // Dfinissez la position de l'image dans le canvas
            imageRectTransform.SetParent(canvas.transform, false);
            imageRectTransform.anchoredPosition = new Vector2(startX, startY);

            // Mettez  jour la position en X pour la prochaine image
            startX += actionBarHeight + padding;

        }
    }

    private void SetNullAction()
    {
        InstantiateFogPlane();
        cursorIndicator.SetActive(false);
        buildingSystem.SetActive(false);
        if(fogPlane != null) fogPlane.SetActive(true);
        cameraEventManager.ReturnToAerialCamera();
        uIController.DeactivatePlayerTargetEnemyCanvas();
        uIController.DeactivatePlayerTargetBarrelCanvas();
        weaponAndMunitionUiController.UpdateWeaponAndAmmoUI();
        currentAction = null;
    }


    private void RunAction()
    {
        SetNullAction();
        currentAction = runAction.actionName;
        cursorIndicator.SetActive(true);
        buildingSystem.SetActive(true);
    }

    private void PlaySound(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }

    private void OpenTreasureAction()
    {
        SetNullAction();
        currentAction = openTreasureAction.actionName;
        currentTreasureNearPlayer.gameObject.GetComponent<OpenTreasure>().Interact();
        PlaySound(openTreasureClip);
    }

    private void ShootAction(GameObject defaultTarget, List<GameObject> visibleTargets)
    {
        SetNullAction();
        if (fogPlane != null) fogPlane.SetActive(false);
        attackManager.SetCurrentTarget(defaultTarget);
        currentPlayer.GetComponent<CharacterAiming>().RotateTowardsTarget(defaultTarget.transform);
        cameraEventManager.SetPlayer(currentPlayer);
        cameraEventManager.AimCameraPlayer(defaultTarget);
        createIcon.CreateTargetsIcon(defaultTarget.tag, visibleTargets, attackManager, currentCharacterWeaponInventory);
    }

    private void ShootEnemyAction()
    {
        EnemyTargetFinder currentPlayerEnemyTargetFinder = currentPlayer.GetComponent<EnemyTargetFinder>();
        currentAction = shootEnemyAction.actionName;
        List<GameObject> enemies = currentPlayerEnemyTargetFinder.GetVisibleTargets();
        currentPlayerEnemyTargetFinder.CalculateChanceToHit();
        GameObject mostVulnerableEnemy = currentPlayerEnemyTargetFinder.GetMostVulnerableEnemy();
        ShootAction(mostVulnerableEnemy, enemies);
        uIController.ActivatePlayerTargetEnemyCanvas();
        SetTextForTargetingEnemy(mostVulnerableEnemy);
        EnemyHealth currentEnemyHealth = mostVulnerableEnemy.GetComponent<EnemyHealth>();
        healthBarUpdate.UpdateHealthBar(currentEnemyHealth.GetRemainingPercentage(), currentEnemyHealth.GetPotentialDamagePercentage(currentCharacterWeaponInventory.GetMaxDamage(mostVulnerableEnemy)));
    }

    private void ShootBarrelAction()
    {
        currentAction = shootBarrelAction.actionName;
        List<GameObject> barrels =  currentPlayer.GetComponent<BarrelTargetFinder>().GetVisibleTargets();
        ShootAction(barrels[0], barrels);
        uIController.ActivatePlayerTargetBarrelCanvas();
    }



    private void ReloadAction()
    {
        SetNullAction();

        currentCharacterWeaponInventory.RefillAmmo();
        PlaySound(reloadClip);
        actionPoints.UseActionPoints(ACTION_POINTS_TO_RELOAD);
        currentAction = shootEnemyAction.actionName;

        InitializeActions();
    }

    private void SwitchWeaponAction()
    {
        SetNullAction();

        PlaySound(switchWeaponClip);
        currentPlayer = gameManager.GetCurrentPlayerTurn();

        currentCharacterWeaponInventory.SwitchWeapon();
        currentAction = switchWeaponAction.actionName;

        playerAnimator = currentPlayer.GetComponent<PlayerAnimatorManager>();

        playerAnimator.ChangeWeaponAnimation();

        weaponAndMunitionUiController.UpdateWeaponAndAmmoUI();
        currentAction = null;

        InitializeActions();
    }

    private void PassTurnAction()
    {
        SetNullAction();
        PlaySound(passTurnClip);
        currentAction = passTurnAction.actionName;
        actionPoints.PassTurn();
    }



    private void SetTextForTargetingEnemy(GameObject enemy)
    {
        EnemyInfo enemyInfo = enemy.GetComponent<EnemyInfo>();
        hitChanceTxt.SetText(enemyInfo.GetChanceToBeHit().ToString() + "%");
        critChanceTxt.SetText(enemyInfo.GetChanceToBeCrit() + "%");
    }

    private void InstantiateFogPlane()
    {
        if (fogPlane == null)
        {
            fogPlane = GameObject.FindWithTag(Harmony.Tags.FogPlane);
        }
    }


}
