using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class AttackButton : MonoBehaviour
{
    [SerializeField] private GameObject targetUI;
    [SerializeField] private GameObject ActionUI;
    [SerializeField] private GameObject currentCharacter;
    void Start()
    {
        targetUI.SetActive(false);
    }

    private void OnAttackButtonClick()
    {
        ActionUI.SetActive(false);
        targetUI.SetActive(true);
    }
}

