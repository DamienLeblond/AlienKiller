using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectUiController : MonoBehaviour
{
    public Canvas canvas; // canvas
    public Vector2 itemSpacing = new Vector2(20f, 20f); // Espace entre chaque objet dans le canvas
    public Vector2 itemSize = new Vector2(40f, 40f); // Taille de chaque objet dans le canvas
    public int maxItemsPerRow = 6; // Nombre maximal d'objets par ligne
    [SerializeField] Font font;
    [SerializeField] private ToolTip toolTip;

    private static string OBJECT_IMAGE_TAG = "ObjectImage";

    public GameManager gameManager;


    // Mthode pour mettre  jour l'interface utilisateur des objets
    public void UpdateInventoryUI(List<TrinketObject> objects)
    {
        // Effacez les anciens objets de l'inventaire
        ClearInventory();

        // Initialisez les variables pour le positionnement des objets
        int itemIndex = 0;
        int rowIndex = 0;
        int colIndex = 0;
        float startX = 40f;
        float startY = 30f;

        Dictionary<TrinketObject, int> itemCounts = new Dictionary<TrinketObject, int>();

        // Parcourez la liste des objets et crez des images pour chaque objet
        foreach (TrinketObject obj in objects)
        {
            if (!itemCounts.ContainsKey(obj))
            {
                itemCounts[obj] = 0;
            }

            // Incrmentez le compteur d'occurrences de l'objet
            itemCounts[obj]++;

            // Crez l'image de l'objet si c'est sa premire occurrence dans l'inventaire
            if (itemCounts[obj] == 1)
            {
                GameObject itemGameObject = new GameObject(obj.name);
                itemGameObject.tag = OBJECT_IMAGE_TAG;

                itemGameObject.AddComponent<RectTransform>();
                itemGameObject.AddComponent<Image>();
                ItemHover itemHover = itemGameObject.AddComponent<ItemHover>();
                itemHover.SetDescription(obj.effectDescription);
                itemHover.SetToolTip(toolTip);


                // Configurez la position de l'image dans le canvas
                RectTransform itemRectTransform = itemGameObject.GetComponent<RectTransform>();
                itemRectTransform.sizeDelta = itemSize;
                // Calcul de la position
                float xPos = startX + (colIndex * (itemSize.x + itemSpacing.x));
                float yPos = startY + (rowIndex * (itemSize.y + itemSpacing.y));

                // Ajustement pour placer l'objet compltement  gauche et en bas
                xPos -= canvas.GetComponent<RectTransform>().sizeDelta.x / 2f;
                yPos -= canvas.GetComponent<RectTransform>().sizeDelta.y / 2f;

                itemRectTransform.anchoredPosition = new Vector2(xPos, yPos);

                // Configurez le sprite de l'image
                Image imageComponent = itemGameObject.GetComponent<Image>();
                imageComponent.sprite = obj.sprite;

                // Vrifiez s'il y a plusieurs occurrences du mme objet dans l'inventaire
                int itemCount = CountItems(objects, obj);
                if (itemCount > 1)
                {
                    // Crez un texte pour afficher le nombre d'occurrences
                    GameObject itemCountTextObject = new GameObject("ItemCountText", typeof(RectTransform));
                    RectTransform textRectTransform = itemCountTextObject.GetComponent<RectTransform>();
                    textRectTransform.SetParent(itemRectTransform);
                    textRectTransform.anchorMin = new Vector2(1f, 0f);
                    textRectTransform.anchorMax = new Vector2(1f, 0f);
                    textRectTransform.pivot = new Vector2(1f, 0f);
                    textRectTransform.anchoredPosition = new Vector2(5f, 0f);
                    textRectTransform.sizeDelta = new Vector2(16f, 16f);
                    Text itemCountText = itemCountTextObject.AddComponent<Text>();
                    itemCountText.text = "x" + itemCount.ToString();
                    itemCountText.alignment = TextAnchor.LowerRight;
                    itemCountText.color = Color.white;
                    itemCountText.font = font;

                }
                itemRectTransform.SetParent(canvas.transform, false);

                // Mettez  jour les indices de ligne et de colonne
                itemIndex++;
                colIndex++;
                if (colIndex >= maxItemsPerRow)
                {
                    colIndex = 0;
                    rowIndex++;
                }
            }
        }

    }

    public int CountItems(List<TrinketObject> objects, TrinketObject targetObject)
    {
        int count = 0;
        foreach (TrinketObject obj in objects)
        {
            if (obj == targetObject)
            {
                count++;
            }
        }
        return count;
    }

    // Mthode pour effacer les objets de l'inventaire
    public void ClearInventory()
    {
        foreach (Transform child in canvas.transform)
        {
            if (child.CompareTag(OBJECT_IMAGE_TAG))
            {
                Destroy(child.gameObject);
            }
        }

    }

}
