using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private ToolTip tooltip;
    private string description;

    public void SetToolTip(ToolTip tooltip)
    {
        this.tooltip = tooltip;
    }

    public void SetDescription(string description)
    {
        this.description = description;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tooltip.ShowTooltip(description);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.HideTooltip();
    }
}
