using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerActionInitiator : MonoBehaviour
{
    public Sprite runIcon;
    public Sprite shootIcon;

    private void Start()
    {
        List<ActionWithImage> actionList = new List<ActionWithImage>();
        actionList.Add(new ActionWithImage("Run", runIcon, RunAction));
        actionList.Add(new ActionWithImage("Shoot", shootIcon, ShootAction));

        ActionBarManager actionBarManager = GetComponent<ActionBarManager>();
        actionBarManager.InitializeActions();
    }

    private void RunAction()
    {
        Debug.Log("Running action activated!");
    }

    private void ShootAction()
    {
        Debug.Log("Shooting action activated!");
    }
}


