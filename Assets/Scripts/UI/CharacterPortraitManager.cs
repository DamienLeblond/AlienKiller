using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEngine;
using UnityEngine.UI;

public class CharacterPortraitManager : MonoBehaviour
{
    public GameManager gameManager;


    public Canvas canvas; //canvas pour l'ordre des tours
    private List<Image> characterImages;
    public Sprite cadreSprite;
    private void Awake()
    {
        // Initialiser la liste des images de portrait
        characterImages = new List<Image>();
    }

    private void Start()
    {
        // R�cup�rer la liste des GameObjects de personnages � partir du GameManager

        List<GameObject> characterObjects = gameManager.GetNextCharacters();

        // Cr�er et afficher les portraits des personnages
        CreateCharacterPortraits(characterObjects);
    }

    public void UpdatePortrait()
    {
        // R�cup�rer la liste des GameObjects de personnages � partir du GameManager
        List<GameObject> characterObjects = gameManager.GetNextCharacters();

        // Cr�er et afficher les portraits des personnages
        CreateCharacterPortraits(characterObjects);
    }

    private void CreateCharacterPortraits(List<GameObject> characterObjects)
    {
        ClearCharacterPortraits();
        // V�rifier si le canvas est d�fini
        if (canvas == null)
        {
            Debug.LogWarning("Canvas non d�fini.");
            return;
        }

        // Calculer les dimensions de l'�cran
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        // Calculer la largeur et la hauteur des portraits
        float portraitWidthPercent = 0.08f; // 20% de la largeur de l'�cran
        float portraitHeightPercent = 0.2f; // 20% de la hauteur de l'�cran
        float portraitWidth = screenWidth * portraitWidthPercent;
        float portraitHeight = screenHeight * portraitHeightPercent;

        // Calculer l'espacement entre les portraits
        float spacingPercent = 0.05f; // 5% de la largeur de l'�cran
        float spacing = screenWidth * spacingPercent;

        // Calculer le nombre total de portraits affich�s
        int totalPortraits = characterObjects.Count;

        // Calculer la position de d�part du premier portrait en X
        float startX = -screenWidth / 4f;

        // Calculer la position de d�part du premier portrait en Y (au lieu de screenHeight)
        float startY = screenHeight / 2f - portraitHeight / 2f;

        // Parcourir la liste des GameObjects de personnages
        for (int i = 0; i < totalPortraits; i++)
        {
 
            // Instancier une nouvelle image UI dans le canvas
            GameObject portraitGO = new GameObject("CharacterPortrait", typeof(RectTransform), typeof(Image));

            // D�finir le parent de l'image dans le canvas
            portraitGO.transform.SetParent(canvas.transform, false);
            Sprite characterSprite;
            if (characterObjects[i].CompareTag(Harmony.Tags.Enemy))
            {
                characterSprite = characterObjects[i].GetComponent<EnemyInfo>().GetPortraitSprite();
            }
            else
            {
                Image characterImage = characterObjects[i].GetComponent<Image>();

                // R�cup�rer le sprite de l'image du composant Image du GameObject de personnage
               characterSprite = characterImage.sprite;
            }

                // Assigner le sprite � l'image UI
                Image imageComponent = portraitGO.GetComponent<Image>();
                imageComponent.sprite = characterSprite;

                // Ajouter le composant Image de l'image UI � la liste des images de portrait
                characterImages.Add(imageComponent);

                // Obtenir le RectTransform de l'image
                RectTransform imageRectTransform = portraitGO.GetComponent<RectTransform>();

                // D�finir la taille rectangulaire de l'image
                imageRectTransform.sizeDelta = new Vector2(portraitWidth, portraitHeight);

                // Calculer la position horizontale du portrait
                float xPos = startX + i * (portraitWidth + spacing);

                // D�finir la position du portrait
                imageRectTransform.anchoredPosition = new Vector2(xPos, startY);

                // Ajouter l'image de cadre
                if (cadreSprite != null)
                {
                    GameObject cadreGO = new GameObject("Cadre", typeof(RectTransform), typeof(Image));

                    // D�finir le parent du cadre dans l'image du portrait
                    cadreGO.transform.SetParent(portraitGO.transform, false);

                    // Assigner le sprite du cadre � l'image UI du cadre
                    Image cadreImage = cadreGO.GetComponent<Image>();
                    cadreImage.sprite = cadreSprite;

                    // Redimensionner le cadre pour qu'il soit de la m�me taille que le portrait
                    RectTransform cadreRectTransform = cadreGO.GetComponent<RectTransform>();
                    cadreRectTransform.sizeDelta = new Vector2(portraitWidth, portraitHeight);

                    // Changer la couleur du cadre en fonction du tag
                    string tag = characterObjects[i].tag;
                    cadreImage.color = tag == "Enemy" ? Color.red : Color.blue;
                }
        }

    }

    private void ClearCharacterPortraits()
    {
        // Parcourir toutes les images de portrait et les d�truire
        foreach (Image characterImage in characterImages)
        {
            Destroy(characterImage.gameObject);
        }

        // Effacer la liste des images de portrait
        characterImages.Clear();
    }
}