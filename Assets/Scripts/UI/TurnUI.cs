using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TurnUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI currentTurnTxt;
    private GameManager gameManager;

    void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();
        
    }

    private void OnEnable()
    {
        GameManager.OnTurnEnd += UpdateTxtTurn;
    }

    private void OnDisable()
    {
        GameManager.OnTurnEnd -= UpdateTxtTurn;
    }

    private void UpdateTxtTurn()
    {
        currentTurnTxt.SetText("Tour: " + gameManager.GetCurrentTurn());
    }
}
