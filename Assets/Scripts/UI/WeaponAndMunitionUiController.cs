using UnityEngine;
using UnityEngine.UI;

public class WeaponAndMunitionUiController : MonoBehaviour
{
    [SerializeField] private Canvas canvas; //canvas
    [SerializeField] private GameManager gameManager;
    private static string WEAPON_HUD_TAG = "WeaponHud";
    [SerializeField] Font font;



    // M�thode pour mettre � jour l'interface utilisateur des armes et des munitions
    public void UpdateWeaponAndAmmoUI()
    {

        ClearCanvas();
        // Obtenez le joueur actuel
        GameObject currentPlayer = gameManager.GetCurrentPlayerTurn();

        // Obtenez l'arme �quip�e du joueur
        CharacterWeaponInventory weaponInventory = currentPlayer.GetComponent<CharacterWeaponInventory>();
        Sprite weaponIcon = weaponInventory.GetWeaponIcon(0);

        GameObject weaponHudGameObject = new GameObject(weaponInventory.GetCurrentWeapon().name);
        weaponHudGameObject.tag = WEAPON_HUD_TAG;

        RectTransform rectTransform = weaponHudGameObject.AddComponent<RectTransform>();

        weaponHudGameObject.AddComponent<Image>();

        // D�finir le parent du GameObject sur le canvas
        weaponHudGameObject.transform.SetParent(canvas.transform, false);

        // Ajouter un Image au GameObject pour afficher l'ic�ne de l'arme
        Image weaponImage = weaponHudGameObject.GetComponent<Image>();
        weaponImage.sprite = weaponIcon;


        rectTransform.anchorMin = new Vector2(1f, 0f);
        rectTransform.anchorMax = new Vector2(1f, 0f);
        rectTransform.pivot = new Vector2(1f, 0f);

        // D�finir la position de l'arme l�g�rement plus haute et plus grande
        rectTransform.sizeDelta = new Vector2(300f, 125f); // Taille de l'ic�ne de l'arme
        rectTransform.anchoredPosition = new Vector2(-15f, 25f);



        int currentAmmo = weaponInventory.MunitionRemaining(0);
        int maxAmmo = weaponInventory.MaxMunition(0);


        // Ajouter le texte pour afficher les munitions
        GameObject ammoTextGameObject = new GameObject("AmmoText");
        ammoTextGameObject.tag = WEAPON_HUD_TAG;
        RectTransform ammoTextRectTransform = ammoTextGameObject.AddComponent<RectTransform>();
        Text ammoText = ammoTextGameObject.AddComponent<Text>();
        ammoText.font = font;
        ammoText.fontSize = 40;
        

        // D�finir le parent du GameObject sur le canvas
        ammoTextGameObject.transform.SetParent(canvas.transform, false);

        // Positionner le texte � droite de l'image de l'arme
        ammoTextRectTransform.anchorMin = new Vector2(1f, 0f);
        ammoTextRectTransform.anchorMax = new Vector2(1f, 0f);
        ammoTextRectTransform.pivot = new Vector2(1f, 0f);
        ammoTextRectTransform.anchoredPosition = new Vector2(-315f, 25f); // Ajustez la position selon vos besoins


        // Mettre � jour le texte des munitions
        if (maxAmmo == -1)
        {
            ammoText.text = "\u221E / \u221E";
        }
        else
        {
            ammoText.text = currentAmmo + " / " + maxAmmo;
        }


    }

    public void ClearCanvas()
    {
        foreach (Transform child in canvas.transform)
        {
            if (child.CompareTag(WEAPON_HUD_TAG))
            {
                Destroy(child.gameObject);
            }
        }

    }

}

