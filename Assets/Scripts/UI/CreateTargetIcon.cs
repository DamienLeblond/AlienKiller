using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class CreateTargetIcon : MonoBehaviour
{
    private float actionBarHeight = 40f; // Hauteur de la barre d'actions
    private float padding = 10f; // Espacement entre les actions
    [SerializeField] private TextMeshProUGUI hitChanceTxt;
    [SerializeField] private TextMeshProUGUI critChanceTxt;
    [SerializeField] private Sprite barrelIcon;
    [SerializeField] Canvas playerTargetEnemyCanvas; // R�f�rence au transform du canvas o� placer les images
    [SerializeField] Canvas playerTargetBarrelCanvas;
    private AttackManager attackManager;
    [SerializeField] TargetingHealthBarUpdate targetingHealthBarUpdate;

    public void CreateTargetsIcon(String typeOfTarget, List<GameObject> targets, AttackManager attack, CharacterWeaponInventory weaponInventory)
    {

        attackManager = attack;
        DestroyIconsFromEachCanvas();

        // Obtenez la taille de l'�cran
        float screenHeight = Screen.height;

        // Calculez la position de d�part en Y pour placer les images au bas de l'�cran
        float startY = (screenHeight / 4f) - 60;

        // Calculez la position en X pour centrer les images horizontalement
        float totalWidth = targets.Count * (actionBarHeight + padding) - padding;
        float startX = -totalWidth / 2f;

        // Parcourez la liste des actions pour cr�er et placer les images dans le canvas
        for (int i = 0; i < targets.Count; i++)
        {
            // Cr�ez un GameObject pour repr�senter l'image d'action
            GameObject actionGameObject = new GameObject(targets[i].name);
            actionGameObject.AddComponent<RectTransform>();
            actionGameObject.AddComponent<Image>();


            // Obtenez le RectTransform de l'image
            RectTransform imageRectTransform = actionGameObject.GetComponent<RectTransform>();

            // Assurez-vous que l'image est centr�e horizontalement et align�e en bas
            imageRectTransform.anchorMin = new Vector2(0.5f, 0);
            imageRectTransform.anchorMax = new Vector2(0.5f, 0);
            imageRectTransform.pivot = new Vector2(0.5f, 0);

            // D�finissez la taille de l'image
            imageRectTransform.sizeDelta = new Vector2(actionBarHeight, actionBarHeight);

            // Obtenez le composant Image de l'image
            Image actionImage = actionGameObject.GetComponent<Image>();

            // Attribuez le sprite de l'action � l'image
            // D�finissez la position de l'image dans le canvas
            if (typeOfTarget == Harmony.Tags.Enemy)
            {
                actionImage.sprite = targets[i].GetComponent<EnemyInfo>().GetIconImage();
                imageRectTransform.SetParent(playerTargetEnemyCanvas.transform, false);
            }
            else if (typeOfTarget == Harmony.Tags.Barrel)
            {
                actionImage.sprite = barrelIcon;
                imageRectTransform.SetParent(playerTargetBarrelCanvas.transform, false);
            }

            imageRectTransform.anchoredPosition = new Vector2(startX, startY);

            actionGameObject.tag = Harmony.Tags.TargetIcon;

            // Ajoutez le script ImageClickHandler � l'image et attribuez-lui l'action associ�e
            ChangeTarget clickHandler = actionGameObject.AddComponent<ChangeTarget>();
            clickHandler.SetVariable(targets[i], hitChanceTxt, critChanceTxt, attackManager, targetingHealthBarUpdate, weaponInventory);

            // Mettez � jour la position en X pour la prochaine image
            startX += actionBarHeight + padding;
        }
    }
    private void DestroyIconsFromEachCanvas()
    {
        DestroyIcons(playerTargetEnemyCanvas);
        DestroyIcons(playerTargetBarrelCanvas);
    }

    private void DestroyIcons(Canvas canvas)
    {
        foreach (Transform child in canvas.transform)
        {
            // Check if the child has the target icon tag
            if (child.CompareTag(Harmony.Tags.TargetIcon))
            {
                Destroy(child.gameObject);
            }
        }
    }
}
