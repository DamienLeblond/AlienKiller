using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuTakeObject : MonoBehaviour
{
    [SerializeField] public List<GameObject> list = new List<GameObject>();

    private Vector2 itemSpacing = new Vector2(24f, 0f); // Espace entre chaque objet dans le canvas
    private Vector2 itemSize = new Vector2(60f, 60f); // Taille de chaque objet dans le canvas
    private int maxItemsPerRow = 4; // Nombre maximal d'objets par ligne
    [SerializeField] Font font;
    private UIController controller;


    public void OpenMenu(List<GameObject> listeDeGameObjects, TrinketObject newObject, UIController uIController)
    {
        controller = uIController;
        GameObject objectImageGameObject = GameObject.Find("ObjectImage");
        objectImageGameObject.GetComponent<Image>().sprite = newObject.sprite;

        GameObject objectTextGameObject = GameObject.Find("ObjectDescriptionText");
        objectTextGameObject.GetComponent<TextMeshProUGUI>().text = newObject.effectDescription;


        for (int i = 0; i < listeDeGameObjects.Count; i++)
        {
            GameObject character = listeDeGameObjects[i];

            string panelName = "CharacterPanel" + (i + 1);

            GameObject panel = GameObject.Find(panelName);

            CharacterInactiveStatus status = character.GetComponent<CharacterInactiveStatus>();

            
            CreateCharacterPortrait(panel, character, status);
            EquipCharacterWeapons(panel, character);
            DisplayCharacterObjects(panel, character);
            if(!status.IsUnconscious && !status.IsDead)
            {
                CreateActionButton(panel, character, newObject);
            }
        }
    }



private void CreateCharacterPortrait(GameObject panel, GameObject character, CharacterInactiveStatus status)
    {
        GameObject portraitGO = new GameObject("CharacterPortrait", typeof(RectTransform), typeof(Image));
        portraitGO.transform.SetParent(panel.transform, false);

        Sprite characterSprite = character.GetComponent<Image>().sprite;
        Image imageComponentPortrait = portraitGO.GetComponent<Image>();
        imageComponentPortrait.sprite = characterSprite;

        RectTransform imageRectTransform = imageComponentPortrait.GetComponent<RectTransform>();
        imageRectTransform.anchorMin = new Vector2(0.25f, 1f);
        imageRectTransform.anchorMax = new Vector2(0.75f, 1f);
        imageRectTransform.pivot = new Vector2(0.5f, 1f);
        imageRectTransform.anchoredPosition = new Vector2(0f, 200f);
        imageRectTransform.sizeDelta = new Vector2(0, 200);

        if (status.IsUnconscious)
        {
            CreateTextComponentForStatus(portraitGO, "INCONSCIENT");   
        }

        if (status.IsDead)
        {
            CreateTextComponentForStatus(portraitGO, "MORT");
        }
    }

    private void CreateTextComponentForStatus(GameObject portraitGO, string statusString)
    {
        GameObject text = new GameObject("CharacterStatusText", typeof(RectTransform), typeof(Text));
        text.transform.SetParent(portraitGO.transform, false);

        Text textComponent = text.GetComponent<Text>();
        textComponent.text = statusString;
        textComponent.color = Color.red;
        textComponent.fontSize = 31;
        textComponent.font = font;
        textComponent.fontStyle = FontStyle.Bold;
        textComponent.alignment = TextAnchor.MiddleCenter;
        textComponent.horizontalOverflow = HorizontalWrapMode.Overflow;
        textComponent.verticalOverflow = VerticalWrapMode.Overflow;

        RectTransform textRectTransform = textComponent.GetComponent<RectTransform>();
        textRectTransform.sizeDelta = new Vector2(0, 50);
        textRectTransform.localRotation = Quaternion.Euler(0f, 0f, 45f);
        textRectTransform.anchorMin = new Vector2(0f, 0f);
        textRectTransform.anchorMax = new Vector2(1f, 1f);
        textRectTransform.pivot = new Vector2(0.5f, 0.5f);
    }

    private void EquipCharacterWeapons(GameObject panel, GameObject character)
    {
        CharacterWeaponInventory characterWeaponInventory = character.GetComponent<CharacterWeaponInventory>();

        Sprite weapon1Sprite = characterWeaponInventory.GetWeaponIcon(1);

        GameObject firstWeaponGO = new GameObject("FirstWeapon", typeof(RectTransform), typeof(Image));

        firstWeaponGO.transform.SetParent(panel.transform, false);

        // Assigner le sprite � l'image UI
        Image imageWeapon1Component = firstWeaponGO.GetComponent<Image>();
        imageWeapon1Component.sprite = weapon1Sprite;

        RectTransform weapon1RectTransform = imageWeapon1Component.GetComponent<RectTransform>();

        weapon1RectTransform.anchorMin = new Vector2(0.29f, 0.83f);
        weapon1RectTransform.anchorMax = new Vector2(0.71f, 0.99f);
        weapon1RectTransform.pivot = new Vector2(0.5f, 1f);
        weapon1RectTransform.anchoredPosition = new Vector2(0f, 0f);
        weapon1RectTransform.sizeDelta = new Vector2(150, 0);


        // deuxi�me arme
        Sprite weapon2Sprite = characterWeaponInventory.GetWeaponIcon(2);

        GameObject secondWeaponGO = new GameObject("SecondWeapon", typeof(RectTransform), typeof(Image));

        secondWeaponGO.transform.SetParent(panel.transform, false);

        // Assigner le sprite � l'image UI
        Image imageWeapon2Component = secondWeaponGO.GetComponent<Image>();
        imageWeapon2Component.sprite = weapon2Sprite;

        RectTransform weapon2RectTransform = imageWeapon2Component.GetComponent<RectTransform>();

        weapon2RectTransform.anchorMin = new Vector2(0.29f, 0.83f);
        weapon2RectTransform.anchorMax = new Vector2(0.71f, 0.99f);
        weapon2RectTransform.pivot = new Vector2(0.5f, 1f);
        weapon2RectTransform.anchoredPosition = new Vector2(0f, -100f);
        weapon2RectTransform.sizeDelta = new Vector2(150, 0);

    }

    public void DisplayCharacterObjects(GameObject panel, GameObject character)
    {
        CharacterObjectInventory characterObjectInventory = character.GetComponent<CharacterObjectInventory>();
        List<TrinketObject> objectList = characterObjectInventory.GetObjects();
        Dictionary<TrinketObject, int> itemCounts = new Dictionary<TrinketObject, int>();

        int itemIndex = 0;
        int rowIndex = 0;
        int colIndex = 0;
        float startX = -125f; // Gardons la m�me valeur de d�part pour startX
        float startY = panel.GetComponent<RectTransform>().sizeDelta.y / 4f + 45;

        // Parcourez la liste des objets et cr�ez des images pour chaque objet
        foreach (TrinketObject obj in objectList)
        {
            if (!itemCounts.ContainsKey(obj))
            {
                itemCounts[obj] = 0;
            }

            // Incr�mentez le compteur d'occurrences de l'objet
            itemCounts[obj]++;

            // Cr�ez l'image de l'objet si c'est sa premi�re occurrence dans l'inventaire
            if (itemCounts[obj] == 1)
            {
                GameObject itemGameObject = new GameObject(obj.name);
                //itemGameObject.tag = OBJECT_IMAGE_TAG;

                itemGameObject.AddComponent<RectTransform>();
                itemGameObject.AddComponent<Image>();

                // Configurez la position de l'image dans le canvas
                RectTransform itemRectTransform = itemGameObject.GetComponent<RectTransform>();
                itemRectTransform.sizeDelta = itemSize;
                // Calcul de la position
                float xPos = startX + (colIndex * (itemSize.x + itemSpacing.x));
                float yPos = startY - (rowIndex * (itemSize.y + itemSpacing.y));

                // Ajustement pour placer l'objet compl�tement � gauche et en bas
                xPos -= panel.GetComponent<RectTransform>().sizeDelta.x / 2f;
                yPos -= panel.GetComponent<RectTransform>().sizeDelta.y / 2f;

                itemRectTransform.anchoredPosition = new Vector2(xPos, yPos);

                // Configurez le sprite de l'image
                Image imageComponent = itemGameObject.GetComponent<Image>();
                imageComponent.sprite = obj.sprite;

                // V�rifiez s'il y a plusieurs occurrences du m�me objet dans l'inventaire
                int itemCount = CountItems(objectList, obj);
                if (itemCount > 1)
                {
                    // Cr�ez un texte pour afficher le nombre d'occurrences
                    GameObject itemCountTextObject = new GameObject("ItemCountText", typeof(RectTransform));
                    RectTransform textRectTransform = itemCountTextObject.GetComponent<RectTransform>();
                    textRectTransform.SetParent(itemRectTransform);
                    textRectTransform.anchorMin = new Vector2(1f, 0f);
                    textRectTransform.anchorMax = new Vector2(1f, 0f);
                    textRectTransform.pivot = new Vector2(1f, 0f);
                    textRectTransform.anchoredPosition = new Vector2(2f, 0f);
                    Text itemCountText = itemCountTextObject.AddComponent<Text>();
                    itemCountText.text = "x" + itemCount.ToString();
                    itemCountText.alignment = TextAnchor.LowerRight;
                    itemCountText.color = Color.white;
                    itemCountText.font = font;
                }
                itemRectTransform.SetParent(panel.transform, false);

                // Mettez � jour les indices de ligne et de colonne
                itemIndex++;
                colIndex++;
                if (colIndex >= maxItemsPerRow)
                {
                    colIndex = 0;
                    rowIndex++;
                }
                if (rowIndex == 5)
                {
                    break;
                }
            }
        }
    }


    private void CreateActionButton(GameObject panel, GameObject character, TrinketObject newObject)
    {
        GameObject boutonObjet = new GameObject("Bouton");
        Button button = boutonObjet.AddComponent<Button>();
        boutonObjet.transform.SetParent(panel.transform, false);

        Image imageButton = boutonObjet.AddComponent<Image>();
        imageButton.color = Color.white;

        RectTransform rectTransform = boutonObjet.GetComponent<RectTransform>();
        rectTransform.anchorMin = new Vector2(0.09f, 1f);
        rectTransform.anchorMax = new Vector2(0.91f, 0.99f);
        rectTransform.pivot = new Vector2(0.5f, 1f);
        rectTransform.offsetMin = new Vector2(0f, -50f);
        rectTransform.anchoredPosition = new Vector2(0, -575);

        GameObject textObject = new GameObject("Texte");
        textObject.transform.SetParent(boutonObjet.transform, false);

        Text buttonText = textObject.AddComponent<Text>();
        buttonText.text = "Prendre";
        buttonText.font = font;
        buttonText.fontSize = 30;
        buttonText.color = Color.black;
        buttonText.alignment = TextAnchor.MiddleCenter;
        RectTransform textRecTransform = buttonText.GetComponent<RectTransform>();
        textRecTransform.anchorMin = new Vector2(0f, 0f);
        textRecTransform.anchorMax = new Vector2(1f, 1f);
        textRecTransform.pivot = new Vector2(0.5f, 1f);
        textRecTransform.offsetMin = new Vector2(0f, 0f);
        textRecTransform.anchoredPosition = new Vector2(0, 0);



        button.onClick.AddListener(() => { OnActionButtonClick(character, newObject); });
    }

    public int CountItems(List<TrinketObject> objects, TrinketObject targetObject)
    {
        int count = 0;
        foreach (TrinketObject obj in objects)
        {
            if (obj == targetObject)
            {
                count++;
            }
        }
        return count;
    }

    public void OnActionButtonClick(GameObject character,TrinketObject obj)
    {
        // R�cup�rer le composant du personnage pour appeler la fonction souhait�e
        CharacterObjectInventory characterInventory = character.GetComponent<CharacterObjectInventory>();
        // Appeler la fonction souhait�e sur le composant du personnage
        characterInventory.AddObject(obj);
        AchievementSaveManager.itemCollected++;
        if(obj.rarity == Rarity.Rare)
        {
            AchievementSaveManager.rareItemCollected++;
        }

        if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.FIRST_COLLECT))
        {
            AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.FIRST_COLLECT);
        }

        if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.HOARDER))
        {
            if (AchievementSaveManager.itemCollected > AchievementSaveManager.ITEM_TO_COLLECT)
            {
                AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.HOARDER);
            }
        }

        if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.RARE_ITEM_COLLECTOR))
        {
            if (AchievementSaveManager.rareItemCollected > AchievementSaveManager.RARE_ITEM_TO_COLLECT)
            {
                AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.RARE_ITEM_COLLECTOR);
            }
        }

        controller.DeactivateGetObject();
        
    }
}
