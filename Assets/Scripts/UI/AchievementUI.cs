using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AchievementUI : MonoBehaviour
{
    [SerializeField] private Canvas achievementCanvas;

    public void ShowAchievement(string achievementName)
    {
        TextMeshProUGUI achievementText = achievementCanvas.GetComponentInChildren<TextMeshProUGUI>();

        if (achievementText != null)
        {
            achievementText.text = achievementName;
            achievementCanvas.gameObject.SetActive(true);
            Invoke("HideAchievement", 3f);
        }
        else
        {
            Debug.LogWarning("No text component found in the achievement panel.");
        }
    }

    private void HideAchievement()
    {
        achievementCanvas.gameObject.SetActive(false);
    }
}
