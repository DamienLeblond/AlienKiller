using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChangeTarget : MonoBehaviour, IPointerClickHandler
{
    private GameObject target;
    private TextMeshProUGUI hitChanceTxt;
    private TextMeshProUGUI critChanceTxt;
    private GameManager gameManager;
    private AttackManager attackManager;
    private TargetingHealthBarUpdate healthBarUpdate;
    private CharacterWeaponInventory weaponInventory;
    private EnemyHealth enemyHealth;
    private CameraEventManager cameraEventManager;

    void Start()
    {
        gameManager = FindFirstObjectByType<GameManager>();
        cameraEventManager = FindFirstObjectByType<CameraEventManager>();
    }

    // M�thode appel�e lorsque l'image est cliqu�e
    public void OnPointerClick(PointerEventData eventData)
    {
        GameObject player = gameManager.GetCurrentPlayerTurn();

        if (target != null)
        {
            player.GetComponent<CharacterAiming>().RotateTowardsTarget(target.transform);
            attackManager.SetCurrentTarget(target);
            cameraEventManager.AimCameraPlayer(target);
            
            if (target.CompareTag(Harmony.Tags.Enemy))
            {
                SetText(target);
                healthBarUpdate.UpdateHealthBar(enemyHealth.GetRemainingPercentage(), enemyHealth.GetPotentialDamagePercentage(weaponInventory.GetMaxDamage(target)));
            }
        }
        else
        {
            Debug.LogError("L'action associ�e � l'image est nulle ou non d�finie.");
        }
    }

    public void SetVariable(GameObject targetEnemy, TextMeshProUGUI hitChance, TextMeshProUGUI critChance, AttackManager attack, TargetingHealthBarUpdate targetingHealthBarUpdate, CharacterWeaponInventory characterWeaponInventory)
    {
        target = targetEnemy;
        hitChanceTxt = hitChance;
        critChanceTxt = critChance;
        attackManager = attack;
        healthBarUpdate = targetingHealthBarUpdate;
        weaponInventory = characterWeaponInventory;
        if (target.CompareTag(Harmony.Tags.Enemy)) enemyHealth = target.GetComponent<EnemyHealth>();
    }

    private void SetText(GameObject enemy)
    {
        EnemyInfo enemyInfo = enemy.GetComponent<EnemyInfo>();
        hitChanceTxt.SetText(enemyInfo.GetChanceToBeHit().ToString() + "%");
        critChanceTxt.SetText(enemyInfo.GetChanceToBeCrit() + "%");
    }
}
