using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] ObjectUiController objectUiController;
    [SerializeField] Canvas ennemiTurnCanvas;
    [SerializeField] Canvas playerTurnCanvas;
    [SerializeField] Canvas playerTargetEnemyCanvas;
    [SerializeField] Canvas playerTargetBarrelCanvas;
    [SerializeField] Canvas getObject;
    [SerializeField] Canvas turnOrderCanvas;
    [SerializeField] Canvas currentTurnCanvas;
    [SerializeField] GameManager gameManager;
    [SerializeField] Canvas endGame;
    [SerializeField] AudioClip takeObjectClip;
    private AudioSource audioSource;
    private MenuTakeObject menuTakeObject;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        menuTakeObject = getObject.GetComponent<MenuTakeObject>();
    }

    // Activer le Canvas de tour de l'ennemi
    public void ActivateEnnemiTurnCanvas()
    {
        ennemiTurnCanvas.gameObject.SetActive(true);
    }

    // Dsactiver le Canvas de tour de l'ennemi
    public void DeactivateEnnemiTurnCanvas()
    {
        ennemiTurnCanvas.gameObject.SetActive(false);
    }

    // Activer le Canvas de tour du joueur
    public void ActivatePlayerTurnCanvas()
    {
        playerTurnCanvas.gameObject.SetActive(true);
    }

    // Dsactiver le Canvas de tour du joueur
    public void DeactivatePlayerTurnCanvas()
    {
        playerTurnCanvas.gameObject.SetActive(false);
    }

    // Activer le Canvas de cible du joueur (ennemi)
    public void ActivatePlayerTargetEnemyCanvas()
    {
        playerTargetEnemyCanvas.gameObject.SetActive(true);
    }

    // Dsactiver le Canvas de cible du joueur (ennemi)
    public void DeactivatePlayerTargetEnemyCanvas()
    {
        playerTargetEnemyCanvas.gameObject.SetActive(false);
    }

    // Activer le Canvas de cible du joueur (baril)
    public void ActivatePlayerTargetBarrelCanvas()
    {
        playerTargetBarrelCanvas.gameObject.SetActive(true);
    }

    // Dsactiver le Canvas de cible du joueur (baril)
    public void DeactivatePlayerTargetBarrelCanvas()
    {
        playerTargetBarrelCanvas.gameObject.SetActive(false);
    }

    public void ActivatePortraitCanvas()
    {
        turnOrderCanvas.gameObject.SetActive(true);
    }

    // Dsactiver le Canvas de cible du joueur
    public void DeactivatePortraitCanvas()
    {
        turnOrderCanvas.gameObject.SetActive(false);
    }

    private void DeactivateCurrentTurnCanvas()
    {
        currentTurnCanvas.gameObject.SetActive(false);
    }

    public void ActivateGetObject(TrinketObject obj)
    {
        DeactivatePlayerTurnCanvas();
        getObject.gameObject.SetActive(true);
        getObject.GetComponent<MenuTakeObject>().OpenMenu(gameManager.GetPlayers(), obj, this);
    }

    public void DeactivateGetObject()
    {
        ActivatePlayerTurnCanvas();
        getObject.gameObject.SetActive(false);
        audioSource.clip = takeObjectClip;
        audioSource.Play();
        objectUiController.UpdateInventoryUI(gameManager.GetCurrentPlayerTurn().GetComponent<CharacterObjectInventory>().GetObjects());
        FindAnyObjectByType<ActionBarManager>().InitializeActions();
    }

    public void ActivateEndGameCanvas(bool victory)
    {
        // Désactiver tous les autres canvas
        DeactivateGetObject();
        DeactivateEnnemiTurnCanvas();
        DeactivatePlayerTurnCanvas();
        DeactivatePlayerTargetEnemyCanvas();
        DeactivatePlayerTargetBarrelCanvas();
        DeactivatePortraitCanvas();
        DeactivateCurrentTurnCanvas();

        // Activer le canvas de fin de jeu
        endGame.gameObject.SetActive(true);
        endGame.gameObject.GetComponent<EndGameScreen>().OpenEndMenu(victory);
    }

    
}

