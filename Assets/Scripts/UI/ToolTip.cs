using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour
{
    private TextMeshProUGUI tooltipText;
    [SerializeField] private GameObject tooltipPanel;
    private RectTransform panelRectTransform;
    private GameObject tooltip;

    private float padding = 10f;

    private void Start()
    {
        tooltipText = GetComponentInChildren<TextMeshProUGUI>();
        panelRectTransform = tooltipPanel.GetComponent<RectTransform>();
        tooltip = gameObject;
        // Assurez-vous que le panneau du tooltip est d�sactiv� au d�but
        tooltip.SetActive(false);
    }

    private void Update()
    {
        // Assurez-vous que le tooltip suit la souris tout en tenant compte de la hauteur du panel
        Vector3 mousePosition = Input.mousePosition;

        tooltip.transform.position = new Vector3(mousePosition.x + 190, mousePosition.y +40f, 0);
    }

    public void ShowTooltip(string text)
    {
        // Afficher le tooltip avec le texte sp�cifi�
        Debug.Log(text);
        tooltipText.text = text;
        ResizePanelToFitText();
        tooltip.SetActive(true);
    }

    public void HideTooltip()
    {
        // Cacher le tooltip
        tooltip.SetActive(false);
    }

    private void ResizePanelToFitText()
    {
        // D�finir la largeur d�sir�e du texte
        float desiredWidth = 350f;

        // Forcer la mise � jour du mesh du texte pour refl�ter le contenu actuel
        tooltipText.ForceMeshUpdate();

        // Obtenir la taille du texte � partir du mesh
        TMP_TextInfo textInfo = tooltipText.textInfo;
        Vector2 textSize = textInfo.meshInfo[0].mesh.bounds.size;

        // Calculer le rapport d'�chelle pour ajuster la largeur du texte
        float scaleRatio = desiredWidth / textSize.x;

        // Ajuster la taille du RectTransform du texte pour qu'elle corresponde � la largeur d�sir�e
        tooltipText.rectTransform.sizeDelta = new Vector2(desiredWidth, tooltipText.preferredHeight);

        // Ajuster la taille du panneau en fonction de la taille du texte
        panelRectTransform.sizeDelta = new Vector2(desiredWidth + padding * 2, tooltipText.preferredHeight + padding * 2);
    }

}
