using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ActionWithImage
{
    public string actionName; // Nom de l'action
    public Sprite actionImage; // Image associ�e � l'action
    public System.Action action;

    // Constructeur pour cr�er un nouvel objet ActionWithImage
    public ActionWithImage(string name, Sprite image, System.Action action)
    {
        actionName = name;
        actionImage = image;
        this.action = action;
    }
}
