using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackTutorial : MonoBehaviour
{
    private static bool isAlreadyNearRobot;
    private GameManager gameManager;
    private TutorialManager tutorialManager;
    private PathFinding pathFinding;
    private CustomGrid targetGrid;

    private void Awake()
    {
        isAlreadyNearRobot = false;
        gameManager = FindAnyObjectByType<GameManager>();
        tutorialManager = FindAnyObjectByType<TutorialManager>();
        targetGrid = FindAnyObjectByType<CustomGrid>();
        pathFinding = FindAnyObjectByType<PathFinding>();
    }

    private void OnTriggerEnter(Collider collider)
    {
        GameObject currentPlayer = gameManager.GetCurrentPlayerTurn();
        if (collider.gameObject == currentPlayer)
        {
            Vector2Int currentPlayerGridPosition = targetGrid.GetGridPosition(currentPlayer.GetComponent<Move>().GetPositionToReach());
            if (!isAlreadyNearRobot && pathFinding.GetGameObjectByTagNearSelectedPosition(currentPlayerGridPosition, Harmony.Tags.Enemy))
            {
                isAlreadyNearRobot = true;
                tutorialManager.MeleeAttackTutorialCanvas.SetActive(true);
            }
        }

    }
}
