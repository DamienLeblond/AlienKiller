using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{
    private const int MAIN_MENU_INDEX_SCENE = 0;
    public GameObject MeleeAttackTutorialCanvas { get; private set; }
    public GameObject OpenTreasureTutorialCanvas { get; private set; }

    private void Awake()
    {
        MeleeAttackTutorialCanvas = GameObject.Find("MeleeAttackTutorialCanvas");
        OpenTreasureTutorialCanvas = GameObject.Find("OpenTreasureTutorialCanvas");
        MeleeAttackTutorialCanvas.SetActive(false);
        OpenTreasureTutorialCanvas.SetActive(false);
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
    }

    public void QuitTutorial()
    {
        SceneManager.LoadScene(MAIN_MENU_INDEX_SCENE);
    }

}
