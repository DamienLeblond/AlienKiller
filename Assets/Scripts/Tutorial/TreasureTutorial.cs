using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class TreasureTutorial : MonoBehaviour
{
    private static bool isAlreadyNearTreasure;
    private GameManager gameManager;
    private TutorialManager tutorialManager;
    private PathFinding pathFinding;
    private CustomGrid targetGrid;

    private void Awake()
    {
        isAlreadyNearTreasure = false;
        gameManager = FindAnyObjectByType<GameManager>();
        tutorialManager = FindAnyObjectByType<TutorialManager>();
        targetGrid = FindAnyObjectByType<CustomGrid>();
        pathFinding = FindAnyObjectByType<PathFinding>();

    }

    private void OnTriggerEnter(Collider collider)
    {
        GameObject currentPlayer = gameManager.GetCurrentPlayerTurn();
        if (collider.gameObject == currentPlayer)
        {
            Vector2Int currentPlayerGridPosition = targetGrid.GetGridPosition(currentPlayer.GetComponent<Move>().GetPositionToReach());
            if (!isAlreadyNearTreasure && pathFinding.GetGameObjectByTagNearSelectedPosition(currentPlayerGridPosition, Harmony.Tags.Treasure))
            {
                isAlreadyNearTreasure = true;
                tutorialManager.OpenTreasureTutorialCanvas.SetActive(true);
            }
        }
    }
}
