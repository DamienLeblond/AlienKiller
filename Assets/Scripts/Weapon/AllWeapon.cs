using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllWeapon : MonoBehaviour
{
    public List<Weapon> allWeapons = new List<Weapon>();

    public Weapon GetWeaponByName(string name)
    {
        foreach (Weapon weapon in allWeapons)
        {
            if (weapon.name == name)
            {
                return weapon;
            }
        }
        return null;
    }

    internal List<Weapon> GetBasicList()
    {
        List<Weapon> basicWeapons = new List<Weapon>();

        // Parcourez la liste compl�te des armes
        foreach (Weapon weapon in allWeapons)
        {
            // V�rifiez si l'arme a une raret� "basic"
            if (weapon.rarity == Rarity.Basic)
            {
                // Ajoutez l'arme � la liste des armes de base
                basicWeapons.Add(weapon);
            }
        }

        return basicWeapons;
    }
}
