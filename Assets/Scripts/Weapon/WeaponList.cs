using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;

public class WeaponList : MonoBehaviour
{
    private List<Weapon> weaponList; // R�f�rence � l'objet ScriptableObject contenant la liste d'armes

    internal Weapon GetFistWeapon()
    {
        foreach (Weapon weapon in weaponList)
        {
            if (weapon.name == "Fist")
            {
                return weapon;
            }
        }
        return null;
    }

    private void Start()
    {
        // Ajouter vos armes � la liste ici
        Weapon[] armes = Resources.LoadAll<Weapon>("Assets/ObjectData/Weapon/");
        foreach (Weapon arme in armes)
        {
            weaponList.Add(arme);
        }
    }

}
