using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Rarity
{
    Basic,
    Common,
    Uncommon,
    Rare,
    Legendary
}
[CreateAssetMenu(fileName = "WeaponCreator", menuName = "Weapon")]
public class Weapon : ScriptableObject
{
    public new string name;
    public int minDamage;
    public int maxDamage;
    public int range;
    public int maxMunition;
    public int numberOfActionToShoot;
    public Rarity rarity;
    public string effect;
    public GameObject prefab;
    public Sprite sprite;
    public bool isMelee;
}