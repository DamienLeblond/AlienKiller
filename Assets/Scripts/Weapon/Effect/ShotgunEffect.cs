using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunEffect : MonoBehaviour, IWeaponEffect
{
    private GameObject bulletPrefab;
    private PlayerAnimatorManager playerAnimatorManager;
    private Transform firePoint;
    private float bulletForce = 40f;
    private int numberOfShots = 12;
    private float spreadAngle = 1f; // Angle de dispersion des balles

    void Start()
    {
        bulletPrefab = GameObject.Find("ShotgunBulletPrefabs");

        // V�rifier si le chargement a r�ussi
        if (bulletPrefab == null)
        {
            Debug.LogError("Le prefab de la balle n'a pas �t� trouv� dans les ressources.");
            return;
        }

        // Trouver l'objet ShootFX qui est un enfant
        Transform shootFXChild = transform.Find("ShootFX");
        if (shootFXChild != null)
        {
            firePoint = shootFXChild;
            // Trouver le gestionnaire d'animation du joueur
            PlayerAnimatorManager playerAnimatorManager = GetComponentInParent<PlayerAnimatorManager>();
            if (playerAnimatorManager == null)
            {
                Debug.LogError("PlayerAnimatorManager non trouv� sur l'objet parent.");
            }
            else
            {
                this.playerAnimatorManager = playerAnimatorManager;
            }
        }
        else
        {
            Debug.LogError("L'objet ShootFX n'a pas �t� trouv� dans les enfants.");
        }
    }

    public void StartShooting(Transform target, bool miss)
    {
        GetComponent<AudioSource>().Play();
        StartCoroutine(Shoot(target, !miss));
    }

    IEnumerator Shoot(Transform target, bool aimAtTarget)
    {
        playerAnimatorManager.StartShootingAnimation();

        Vector3 exaggeratedDirection = firePoint.forward + Random.insideUnitSphere;


        for (int i = 0; i < numberOfShots; i++)
        {
            // D�terminer la direction dans laquelle la balle doit partir
            Vector3 direction;
            if (aimAtTarget)
            {
                direction = (target.position - firePoint.position).normalized;
                direction += Vector3.up * 0.2f; // Ajuster l�g�rement la composante Y pour tirer un peu plus haut
            }
            else
            {
                // G�n�rer une direction al�atoire dans un c�ne plus large pour simuler une dispersion
                direction = exaggeratedDirection;
            }

            // Appliquer une rotation � chaque balle pour qu'elle pointe dans la direction voulue
            Quaternion finalRotation = Quaternion.LookRotation(direction);

            Quaternion spreadRotation = Quaternion.Euler(Random.Range(-spreadAngle, spreadAngle), Random.Range(-spreadAngle, spreadAngle), Random.Range(-spreadAngle, spreadAngle));
            Quaternion rotationAvecSpread = finalRotation * spreadRotation;

            // Cr�er la balle avec la rotation calcul�e
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, rotationAvecSpread);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(bullet.transform.forward * bulletForce, ForceMode.Impulse);
        }

        yield return null; // Attendre une frame avant de terminer la coroutine
    }
}
