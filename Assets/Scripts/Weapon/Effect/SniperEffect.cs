using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperEffect : MonoBehaviour, IWeaponEffect
{
    private GameObject bulletPrefab;
    private Transform firePoint;
    private float bulletForce = 50f;
    private float angleVariation = 0f; // Variation d'angle pour chaque balle


    void Start()
    {

        bulletPrefab = GameObject.Find("AssaultRiffleBulletPrefabs");

        // V�rifier si le chargement a r�ussi
        if (bulletPrefab == null)
        {
            Debug.LogError("Le prefab de la balle n'a pas �t� trouv� dans les ressources.");
        }
        // Trouver l'objet ShootFX dans un enfant d'un enfant
        Transform shootFXChild = transform.GetChild(0);
        if (shootFXChild != null)
        {
            firePoint = shootFXChild;

        }
        else
        {
            Debug.LogError("L'objet ShootFX n'a pas �t� trouv� dans les enfants.");
        }
    }

    public void StartShooting(Transform target, bool miss)
    {
        GetComponent<AudioSource>().Play();
        StartCoroutine(Shoot(target, !miss));
    }

    IEnumerator Shoot(Transform target, bool aimAtTarget)
    {
        // D�terminer la direction dans laquelle la balle doit partir
        Vector3 direction;
        if (aimAtTarget)
        {
            direction = (target.position - firePoint.position).normalized;
            direction += Vector3.up * 0.2f;
        }
        else
        {
            // G�n�rer une direction al�atoire dans un c�ne plus large pour simuler une dispersion
            Vector3 directionToTarget = (target.position - firePoint.position).normalized;
            Vector3 randomOffset = Random.insideUnitSphere * angleVariation;
            direction = Vector3.Lerp(directionToTarget, randomOffset, Random.value);
        }

        // Appliquer une variation d'angle � chaque balle
        Quaternion finalRotation = Quaternion.LookRotation(direction);

        // Cr�er la balle avec la rotation calcul�e
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, finalRotation);

        // Ajouter une force � la balle pour la faire partir dans la direction voulue
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        rb.AddForce(bullet.transform.forward * bulletForce, ForceMode.Impulse);

        yield return null;
    }
}

