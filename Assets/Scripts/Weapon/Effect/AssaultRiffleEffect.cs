using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssaultRiffleEffect : MonoBehaviour, IWeaponEffect
{
    private GameObject bulletPrefab;
    private PlayerAnimatorManager playerAnimatorManager;
    private Transform firePoint;
    private float bulletForce = 30f;
    private float timeBetweenShots = 0.4f;
    private int numberOfShots = 3;
    private float angleVariation = 1f; // Variation d'angle pour chaque balle
    private AudioSource audioSource;


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        bulletPrefab = GameObject.Find("AssaultRiffleBulletPrefabs");

        // V�rifier si le chargement a r�ussi
        if (bulletPrefab == null)
        {
            Debug.LogError("Le prefab de la balle n'a pas �t� trouv� dans les ressources.");
        }
        // Trouver l'objet ShootFX dans un enfant d'un enfant
        Transform shootFXChild = transform.GetChild(0).Find("ShootFX");
        if (shootFXChild != null)
        {
            firePoint = shootFXChild;

            // Trouver l'objet parent de l'objet parent pour acc�der au composant d'animation
            Transform parentParent = transform.parent.parent;
            if (parentParent != null)
            {
                playerAnimatorManager = parentParent.GetComponent<PlayerAnimatorManager>();
            }
            else
            {
                Debug.LogError("L'objet parent de l'objet parent n'a pas �t� trouv�.");
            }
        }
        else
        {
            Debug.LogError("L'objet ShootFX n'a pas �t� trouv� dans les enfants.");
        }
    }

    public void StartShooting(Transform target, bool miss)
    {
        audioSource.Play();
        StartCoroutine(Shoot(target, !miss));
    }

    IEnumerator Shoot(Transform target, bool aimAtTarget)
    {
        for (int i = 0; i < numberOfShots; i++)
        {
            // D�terminer la direction dans laquelle la balle doit partir
            Vector3 direction;
            if (aimAtTarget)
            {
                direction = (target.position - firePoint.position).normalized;
            }
            else
            {
                // G�n�rer une direction al�atoire dans un c�ne plus large pour simuler une dispersion
                direction = firePoint.forward + Random.insideUnitSphere * angleVariation;
            }

            // Appliquer une variation d'angle � chaque balle
            Quaternion finalRotation = Quaternion.LookRotation(direction);

            playerAnimatorManager.StartShootingAnimation();
            // Cr�er la balle avec la rotation calcul�e
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, finalRotation);

            // Ajouter une force � la balle pour la faire partir dans la direction voulue
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(bullet.transform.forward * bulletForce, ForceMode.Impulse);

            yield return new WaitForSeconds(timeBetweenShots);
        }
    }
}
