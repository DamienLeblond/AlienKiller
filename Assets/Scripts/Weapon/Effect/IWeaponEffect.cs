using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponEffect
{
    void StartShooting(Transform target, bool miss);
}
