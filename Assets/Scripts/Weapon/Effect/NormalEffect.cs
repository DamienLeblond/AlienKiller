using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalEffect : MonoBehaviour, IWeaponEffect
{
    private GameObject bulletPrefab;
    private IEnnemiAnimatorManager animatorManager;
    private Transform firePoint;
    private float bulletForce = 30f;
    private float timeBetweenShots = 0.02f;
    private int numberOfShots = 10;
    private float angleVariation = 1.2f; // Variation d'angle pour chaque balle

    private WaitForSeconds shotDuration = new WaitForSeconds(0.001f); // Dur�e de la tra�n�e


    void Start()
    {

        bulletPrefab = GameObject.Find("AssaultRiffleBulletPrefabs");

        // V�rifier si le chargement a r�ussi
        if (bulletPrefab == null)
        {
            Debug.LogError("Le prefab de la balle n'a pas �t� trouv� dans les ressources.");
        }
        // Trouver l'objet ShootFX dans un enfant d'un enfant
        Transform shootFXChild = transform.Find("ShootFX");
        if (shootFXChild != null)
        {
            firePoint = shootFXChild;
        }
        else
        {
            Debug.LogError("L'objet ShootFX n'a pas �t� trouv� dans les enfants.");
        }
    }

    public void StartShooting(Transform target, bool miss)
    {
        GetComponent<AudioSource>().Play();
        StartCoroutine(Shoot(target, miss));
    }

    IEnumerator Shoot(Transform target, bool aimAtTarget)
    {
        for (int i = 0; i < numberOfShots; i++)
        {
            // D�terminer la direction dans laquelle la balle doit partir
            Vector3 direction;
            if (aimAtTarget)
            {
                direction = (target.position - firePoint.position).normalized;
                direction += Vector3.up * 0.2f;
            }
            else
            {
                // G�n�rer une direction al�atoire dans un c�ne plus large pour simuler une dispersion
                direction = firePoint.forward + Random.insideUnitSphere * angleVariation;
            }


            float randomAngle = Random.Range(-3f, 3f); // D�calage al�atoire de 3 degr�s environ
            Quaternion randomRotation = Quaternion.AngleAxis(randomAngle, firePoint.up);
            direction = randomRotation * direction;

            // Appliquer une variation d'angle � chaque balle
            Quaternion finalRotation = Quaternion.LookRotation(direction);

            // Cr�er la balle avec la rotation calcul�e
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, finalRotation);

            // Ajouter une force � la balle pour la faire partir dans la direction voulue
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(bullet.transform.forward * bulletForce, ForceMode.Impulse);

            yield return new WaitForSeconds(timeBetweenShots);
        }
    }
}
