using Harmony;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.InputSystem.Controls;

public class AttackManager : MonoBehaviour
{
    [SerializeField] private GameObject currentTarget;
    private GameManager gameManager;
    private PathFinding pathFinding;
    private CustomGrid targetGrid;
    private ActionPoints actionPoints;

    private void Awake()
    {
        gameManager = FindAnyObjectByType<GameManager>();
        pathFinding = FindAnyObjectByType<PathFinding>();
        targetGrid = FindAnyObjectByType<CustomGrid>();
        actionPoints = FindAnyObjectByType<ActionPoints>();
    }

    public void AttackEnemy()
    {
        GameObject player = gameManager.GetCurrentPlayerTurn();
        CharacterStat playerStats = player.GetComponent<CharacterStat>();
        PlayerAnimatorManager playerAnimatorManager = player.GetComponent<PlayerAnimatorManager>();
        CharacterWeaponInventory characterWeaponInventory = player.GetComponent<CharacterWeaponInventory>();
        EnemyInfo enemyInfo = currentTarget.GetComponent<EnemyInfo>();
        EnemyHealth enemyHealth = currentTarget.GetComponent<EnemyHealth>();

        int damage;
        bool isCrit;
        bool miss = true;
        if (currentTarget != null)
        {
            if (Mathf.FloorToInt(Random.Range(0, 100)) < enemyInfo.GetChanceToBeHit())
            {
                playerStats.globalStatsData.bulletsHit++;
                
                miss = false;
                isCrit = Mathf.FloorToInt(Random.Range(0, 100)) < enemyInfo.GetChanceToBeCrit();
                if (isCrit)
                {
                    playerStats.globalStatsData.crit++;
                }
                damage = characterWeaponInventory.GetDamage(isCrit, enemyHealth.IsFullHealth());
                if(playerStats.globalStatsData.highestDamageDealt < damage)
                {
                    playerStats.globalStatsData.highestDamageDealt = damage;
                }
                bool kill = enemyHealth.GetHit(damage);
                if (kill)
                {
                    playerStats.globalStatsData.enemyDefeated++;
                }
                playerStats.AttackHeal(damage, isCrit, kill);
            }

            if (characterWeaponInventory.IsWeaponFist())
            {
                playerAnimatorManager.StartRandomMeleeAttack();
                if(!miss)
                {
                    StartCoroutine(MeleeHitAnimation());
                }
               actionPoints.UseActionPoints(characterWeaponInventory.ActionCostToFire());
            }
            else
            {
                Shoot(player, miss);
                playerStats.globalStatsData.shotsFired++;
            }
        }
    }

    private IEnumerator MeleeHitAnimation()
    {
        yield return new WaitForSeconds(1f);

        currentTarget.GetComponent<IEnnemiAnimatorManager>().GetMeleeHit();
    }

    public void ShootBarrel()
    {
        if (currentTarget != null)
        {
            GameObject currentPlayer = gameManager.GetCurrentPlayerTurn();
            List<GameObject> targetsNearBarrel = pathFinding.GetGameObjectsNearSelectedPosition(targetGrid.GetGridPosition(currentTarget.transform.position));
            foreach (GameObject target in targetsNearBarrel)
            {
                GridObject targetAsGridObject = target.GetComponent<GridObject>();
                if (targetAsGridObject != null)
                {
                    int damage = currentTarget.GetComponent<BarrelEffect>().GetPossibleDamage(targetAsGridObject);
                    if (target.layer == Harmony.Layers.Player.Index)
                    {
                        target.GetComponent<CharacterStat>().GetHit(false, damage);
                        target.GetComponent<PlayerAnimatorManager>().GetHit(target.transform.position);
                    }
                    else if (target.layer == Harmony.Layers.Enemy.Index)
                    {
                        target.GetComponent<EnemyHealth>().GetHit(damage);
                        target.GetComponent<IEnnemiAnimatorManager>().GetHit(target.transform.position);
                    }
                }

            }
            Shoot(currentPlayer, false);
        }
    }


    private void Shoot(GameObject player, bool miss)
    {
        CharacterWeaponInventory characterWeaponInventory = player.GetComponent<CharacterWeaponInventory>();
        GameObject weaponR = player.transform.Find("Weapon_R").gameObject;
        GameObject weapon = weaponR.transform.GetChild(0).gameObject;
        IWeaponEffect weaponEffect = weapon.GetComponent<IWeaponEffect>();
        weaponEffect.StartShooting(currentTarget.transform, miss);
        characterWeaponInventory.UseAmmo();
        actionPoints.UseActionPoints(characterWeaponInventory.ActionCostToFire());
    }

    public void SetCurrentTarget(GameObject target)
    {
        this.currentTarget = target;
    }

    public GameObject GetCurrentTarget()
    {
        return currentTarget;
    }
}

