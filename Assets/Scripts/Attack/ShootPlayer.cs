using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;

public class ShootPlayer : MonoBehaviour
{
    private CharacterTargetFinder findTarget;
    private EnemyInfo enemyInfo;
    private bool hit;
    private IEnnemiAnimatorManager animationManager;

    private Transform enemyTransform;

    void Start()
    {
        findTarget = GetComponent<CharacterTargetFinder>();
        enemyInfo = GetComponent<EnemyInfo>();
        animationManager = GetComponent<IEnnemiAnimatorManager>();
    }

    public void CalculateChanceToHitVisibleTargets()
    {
        findTarget.FindVisibleTargets();
        findTarget.CalculateChanceToHit();
    }

    public bool CanShootCharacter()
    {
        if (findTarget.GetVisibleTargets().Count > 0)
        {
            return true;
        }
        return false;
    }

    public bool IsChanceToHitAbove60Percent()
    {
        if (findTarget.GetMostVulnerableCharacter() != null)
        {
            if (findTarget.GetMostVulnerableCharacter().GetComponent<CharacterInfo>().GetChanceToBeHit() >= 60)
            {
                return true;
            }
        }
        return false;
    }

    public GameObject GetTarget()
    {
        return findTarget.GetMostVulnerableCharacter();
    }

    public void ShootCharacter()
    {
        AttackCharacter(GetTarget());
    }

    private void AttackCharacter(GameObject character)
    {
        Vector3 direction = character.transform.position - transform.position;
        enemyTransform = character.transform;

        transform.rotation = Quaternion.LookRotation(direction);
        hit = false;
        if (Mathf.FloorToInt(UnityEngine.Random.Range(0, 100)) < character.GetComponent<CharacterInfo>().GetChanceToBeHit())
        {
            int damage = 0;
            hit = true;
            bool isCrit = Mathf.FloorToInt(UnityEngine.Random.Range(0, 100)) < character.GetComponent<CharacterInfo>().GetChanceToBeCrit();
            if (isCrit)
            {
                damage = (UnityEngine.Random.Range(enemyInfo.GetMinDamage(), enemyInfo.GetMaxDamage() + 1))*2;
            }
            else
            {
                damage = UnityEngine.Random.Range(enemyInfo.GetMinDamage(), enemyInfo.GetMaxDamage() + 1);
            }
            character.GetComponent<CharacterStat>().GetHit(isCrit, damage);
        }
        animationManager.StartShootingAnimation();
    }

    public void ShootEffect(IWeaponEffect weaponEffect)
    {
        weaponEffect.StartShooting(enemyTransform, hit);
    }
}
