using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.TextCore.Text;

public class EnemyTargetFinder : VisibleTargetFinder
{
    protected override string TargetTag => Harmony.Tags.Enemy; 

    public override void FindVisibleTargets()
    {
        Vector3 position = transform.position;

        float maxRange = gameObject.GetComponent<CharacterWeaponInventory>().GetCurrentWeaponRange() * tileSize;

        Collider[] hitColliders = Physics.OverlapSphere(position, maxRange);

        visibleTargets = new List<GameObject>();

        foreach (var collider in hitColliders)
        {
            GameObject target = collider.gameObject;

            if (IsTargetValid(target) && !DetectObstacle(position, target) && target.activeInHierarchy)
            {
                AddVisibleTarget(target);
            }
        }
    }

    protected virtual bool DetectObstacle(Vector3 position, GameObject target)
    {
        Vector3 endPoint = target.transform.position;
        return gameObject.GetComponent<DetectObstacle>().IsObstacleBetween(position, endPoint);
    }

    public GameObject GetMostVulnerableEnemy()
    {
        float highestChanceToHit = -1f;
        GameObject mostVulnerableEnemy = null;

        foreach (GameObject enemy in GetVisibleTargets())
        {
            EnemyInfo enemyInfo = enemy.GetComponent<EnemyInfo>();
            float chanceToHit = enemyInfo?.GetChanceToBeHit() ?? -1;

            if (chanceToHit >= 0 && chanceToHit > highestChanceToHit)
            {
                highestChanceToHit = chanceToHit;
                mostVulnerableEnemy = enemy;
            }
        }
        return mostVulnerableEnemy;
    }

    public void CalculateChanceToHit()
    {
        int bonusAccuracy = gameObject.GetComponent<CharacterStat>().Accuracy;
        int bonusCrit = gameObject.GetComponent<CharacterStat>().CritChance;
        CharacterWeaponInventory characterWeaponInventory = gameObject.GetComponent<CharacterWeaponInventory>();

        float maxDistance = gameObject.GetComponent<CharacterWeaponInventory>().GetCurrentWeaponRange() * tileSize;

        foreach (var enemy in visibleTargets)
        {
            EnemyInfo target = enemy.GetComponent<EnemyInfo>();
            float distance = Vector3.Distance(transform.position, enemy.transform.position);

            float basePercentage = Mathf.Clamp(100f - (distance / maxDistance) * 100f, 0f, 100f);

            float chanceToHit;
            if (characterWeaponInventory.IsWeaponFist() == true)
            {
                chanceToHit = 75f;
                SetInfo(chanceToHit, bonusCrit, target);
                chanceToHit = Mathf.Max(75f + bonusAccuracy, 0f);
            }
            else if (gameObject.GetComponent<RaycastDetection>().DetectObstacleWithRaycast(transform.position, enemy.transform.position))
            {
                float obstacleScaleY = CalculateObstacleScaleY(transform.position, enemy.transform.position);
                float enemyScaleY = enemy.transform.localScale.y;
                float obstacleCoverage = obstacleScaleY / (enemyScaleY+1);

                float adjustment = obstacleCoverage <= 0.5f ? 20f : 40f;
                chanceToHit = Mathf.Max(basePercentage - adjustment, 0f);
            }
            else
            {
                chanceToHit = basePercentage;
            }

            if (chanceToHit <= 0)
            {
                visibleTargets.Remove(enemy);
            }

            SetInfo(chanceToHit + bonusAccuracy, bonusCrit, target);
        }
    }

    private void SetInfo(float chanceToHit, int bonusCrit, EnemyInfo enemy)
    {
        enemy.SetChanceToBeHit((int)chanceToHit);
        enemy.IsEnemyFlanked(gameObject);
        enemy.SetChanceToBeCrit(bonusCrit);
    }

    private float CalculateObstacleScaleY(Vector3 playerPosition, Vector3 enemyPosition)
    {
        RaycastHit hitInfo;
        float obstacleScaleY = 0;

        if (Physics.Raycast(playerPosition, enemyPosition - playerPosition, out hitInfo))
        {
            float obstacleDistance = Vector3.Distance(playerPosition, hitInfo.point);
            if (hitInfo.collider.CompareTag(Harmony.Tags.Obstacle) && obstacleDistance > 1f)
            {
                obstacleScaleY = hitInfo.collider.transform.localScale.y;
            }
        }

        return obstacleScaleY;
    }
}
