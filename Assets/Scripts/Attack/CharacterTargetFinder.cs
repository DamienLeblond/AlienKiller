using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;
using UnityEngine.UIElements;

public class CharacterTargetFinder : VisibleTargetFinder
{
    protected override string TargetTag => Harmony.Tags.MainCharacter;
    private EnemyInfo enemyInfo;
    private DetectObstacle detectObstacle;
    private RaycastDetection raycastDetection;

    private void Start()
    {
        enemyInfo = GetComponent<EnemyInfo>();
        detectObstacle = GetComponent<DetectObstacle>();
        raycastDetection = GetComponent<RaycastDetection>();
    }
    public override void FindVisibleTargets()
    {
        Vector3 position = transform.position;

        float maxRange = enemyInfo.GetRange() * tileSize;

        Collider[] hitColliders = Physics.OverlapSphere(position, maxRange);

        visibleTargets = new List<GameObject>();

        foreach (var collider in hitColliders)
        {
            GameObject target = collider.gameObject;

            if (IsTargetValid(target) && IsTargetConscious(target) && !DetectObstacle(position, target))
            {
                AddVisibleTarget(target);
            }
        }
    }

    private bool IsTargetConscious(GameObject target)
    {
        CharacterInactiveStatus targetInactiveStatus = target.GetComponent<CharacterInactiveStatus>();
        if (targetInactiveStatus != null && !targetInactiveStatus.IsUnconscious) return true;
        return false;
    }

    protected virtual bool DetectObstacle(Vector3 position, GameObject target)
    {
        Vector3 endPoint = target.transform.position;
        return detectObstacle.IsObstacleBetween(position, endPoint);
    }

    public GameObject GetMostVulnerableCharacter()
    {
        float highestChanceToHit = -1f;
        GameObject mostVulnerableCharacter = null;

        foreach (GameObject character in GetVisibleTargets())
        {
            CharacterInfo info = character.GetComponent<CharacterInfo>();

            if (info != null && info.GetChanceToBeHit() >= 0)
            {
                float chanceToHit = info.GetChanceToBeHit();

                if (chanceToHit > highestChanceToHit)
                {
                    highestChanceToHit = chanceToHit;
                    mostVulnerableCharacter = character;
                }
            }
        }

        return mostVulnerableCharacter;
    }

    public void CalculateChanceToHit()
    {
        float maxDistance = enemyInfo.GetRange() * tileSize;

        foreach (var character in visibleTargets)
        {
            CharacterInfo target = character.GetComponent<CharacterInfo>();
            float distance = Vector3.Distance(transform.position, character.transform.position);

            float basePercentage = Mathf.Clamp(100f - (distance / maxDistance) * 100f, 0f, 100f);

            float chanceToHit;

            if (raycastDetection.DetectObstacleWithRaycast(transform.position, character.transform.position))
            {
                float obstacleScaleY = CalculateObstacleScaleY(transform.position, character.transform.position);
                float enemyScaleY = character.transform.localScale.y;
                float obstacleCoverage = obstacleScaleY / (enemyScaleY+1);

                float adjustment = obstacleCoverage <= 0.5f ? 20f : 40f;
                chanceToHit = Mathf.Max(basePercentage - adjustment, 0f);
            }
            else
            {
                chanceToHit = basePercentage;
            }

            SetInfo(chanceToHit, target);
        }
    }

    private void SetInfo(float chanceToHit, CharacterInfo character)
    {
        CharacterInfo characterInfo = character.GetComponent<CharacterInfo>();
        characterInfo.SetChanceToBeHit((int)chanceToHit);
        characterInfo.IsCharacterFlanked(gameObject);
        characterInfo.SetChanceToBeCrit(enemyInfo.GetCritChance());
    }

    private float CalculateObstacleScaleY(Vector3 position, Vector3 characterPosition)
    {
        RaycastHit hitInfo;

        if (Physics.Raycast(position, characterPosition - position, out hitInfo))
        {
            float obstacleDistance = Vector3.Distance(position, hitInfo.point);
            if (hitInfo.collider.CompareTag(Harmony.Tags.Obstacle) && obstacleDistance > 1f)
            {
                return hitInfo.collider.transform.localScale.y;
            }
        }

        return 0f;
    }
}
