using System.Collections.Generic;
using UnityEngine;

public abstract class VisibleTargetFinder : MonoBehaviour
{
    protected List<GameObject> visibleTargets;

    protected int tileSize = 2;

    protected abstract string TargetTag { get; }

    public abstract void FindVisibleTargets();

    public List<GameObject> GetVisibleTargets()
    {
        return visibleTargets;
    }

    protected virtual void AddVisibleTarget(GameObject target)
    {
        visibleTargets.Add(target);
    }

    protected virtual bool IsTargetValid(GameObject target)
    {
        return target != null && target.CompareTag(TargetTag);
    }
}