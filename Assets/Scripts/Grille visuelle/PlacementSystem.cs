using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlacementSystem : MonoBehaviour
{
    [SerializeField] private GameObject mouseIndicator, cellIndicator;
    [SerializeField] private InputManager inputManager;
    [SerializeField] private GameManager gameManager;
    private Renderer previewRenderer;
    private CellIndicatorCollision cellIndicatorCollision;
    Vector2Int visualGridPosition;
    Vector2Int targetGridPosition;

    [SerializeField] private CustomGrid targetGrid;
    private PathFinding pathFinding;
    private List<PathNode> path;

    private void Start()
    {
        previewRenderer = cellIndicator.GetComponentInChildren<Renderer>();
        cellIndicatorCollision = cellIndicator.GetComponentInChildren<CellIndicatorCollision>();
        pathFinding = targetGrid.GetComponent<PathFinding>();
        cellIndicator.transform.position = Vector3.zero;
    }

    private void Update()
    {
        Vector3 mousePosition = inputManager.GetSelectedMapPosition();
        
        targetGridPosition = targetGrid.GetGridPosition(mousePosition);
        visualGridPosition = targetGrid.GetGridVisualPosition(mousePosition);

        previewRenderer.material.color = GetIndicatorColor();

        mouseIndicator.transform.position = mousePosition;
        cellIndicator.transform.position = targetGrid.GetWorldPosition(visualGridPosition.x, visualGridPosition.y);
        cellIndicator.transform.position -= new Vector3(1, 0, 1);
    }

    private Color GetIndicatorColor()
    {
        if (CheckPlacementValidity())
        {
            return Color.white;
        }
        return Color.red;
    }

    private bool CheckPlacementValidity()
    {
        Vector2Int currentPlayerCustomGridPosition = targetGrid.GetGridPosition(gameManager.GetCurrentPlayerTurn().transform.position);
        return !cellIndicatorCollision.IsColliding() && !(CalculateDistanceBetweenTwoPointsPathfinding(currentPlayerCustomGridPosition, targetGridPosition) > gameManager.GetCurrentPlayerTurn().GetComponent<Move>().GetMaxTileCount());
    }

    private float CalculateDistanceBetweenTwoPointsPathfinding(Vector2Int origin, Vector2Int destination)
    {
        path = pathFinding.FindPath(origin.x, origin.y, destination.x, destination.y);
        if (path != null)
        {
            return path.Count;
        }
        return 100f;
    }
}
