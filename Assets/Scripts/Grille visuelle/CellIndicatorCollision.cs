using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellIndicatorCollision : MonoBehaviour
{
    private bool isColliding = false;
    private GameObject objectColliding;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8 || 
            other.gameObject.layer == 6 || 
            other.gameObject.layer == 10 ||
            other.gameObject.layer == 9)
            {
                isColliding = true;
                objectColliding = other.gameObject;
            }
    }

    private void OnTriggerExit(Collider other)
    {
        isColliding = false;
    }

    public bool IsColliding()
    {
        return isColliding;
    }

    public GameObject GetObjectColliding()
    {
        return objectColliding;
    }
}

