using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;

public class ActionPoints : MonoBehaviour
{
    private const int BASE_ACTION_POINTS = 2;
    private int currentActionPoints;
    private GameManager gameManager;
    private ActionBarManager actionBarManager;
    
    private void Awake()
    {
        ResetActionPoints();
        gameManager = GetComponent<GameManager>();
        actionBarManager = FindAnyObjectByType<ActionBarManager>();
    }

    public int GetActionPoints() { return currentActionPoints; }

    public void GiveActionPoints(int pointsToAdd) { currentActionPoints += pointsToAdd; }
    public void UseActionPoints(int pointsToSubtract) {

        currentActionPoints -= pointsToSubtract;
        actionBarManager.InitializeActions();
        if (currentActionPoints <= 0)
        {
            ResetActionPoints();
            gameManager.NextPlayerTurn();
        }
    }

    public void PassTurn()
    {
        UseActionPoints(currentActionPoints);
    }

    private void ResetActionPoints() { currentActionPoints = BASE_ACTION_POINTS; }
}
