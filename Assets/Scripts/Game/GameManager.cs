using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using TMPro;
using Harmony;
using System.Xml.Linq;
using DungeonArchitect;

[Findable(Tags.GameManager)]
public class GameManager : MonoBehaviour
{
    
    [SerializeField] private List<GameObject> playerList;
    [SerializeField] private List<GameObject> turnOrder;
    [SerializeField] private List<GameObject> unconsciousPlayersList;
    [SerializeField] Camera mainCamera;
    [SerializeField] GameObject aerialCameraHolder;
    [SerializeField] UIController uIController;
    [SerializeField] ObjectUiController objectUiController;
    [SerializeField] WeaponAndMunitionUiController weaponAndMunitionUiController;
    [SerializeField] CharacterPortraitManager characterPortraitManager;
    [SerializeField] ActionBarManager actionBarManager;

    [SerializeField] private GameObject currentPlayerTurn;
    private List<GameObject> spawnPoints = new();
    [SerializeField] private int numberOfActivePlayers;
    private int currentPlayerTurnIndex = -1;
    private Move currentPlayerMove;
    private GridControl cameraGridControl;
    private ActionPoints actionPoints;

    public delegate void DeleteEnemyAction();
    public static event DeleteEnemyAction OnDeleteEnemy;

    public delegate void TurnEnd();
    public static event TurnEnd OnTurnEnd;

    public static int LAST_LEVEL_INDEX = 2;

    private int currentTurn = 0;
    private bool turnCompleted = true;

    private SaveProgression progression;
    private SaveProgressionData progressionData;

    private GameObject dungeonItem;

    private List<DetectSpawnEnemies> detectSpawnEnemiesRooms = new();
    private List<DetectRoomExplored> detectRoomExplored = new();
    private List<OpenTreasure> treasureList = new();

    void Start()
    {
        progression = new SaveProgression();
        cameraGridControl = mainCamera.GetComponent<GridControl>();
        actionPoints = FindAnyObjectByType<ActionPoints>();
        numberOfActivePlayers = turnOrder.Count;
        GameObject.FindGameObjectsWithTag(Harmony.Tags.Respawn, spawnPoints);
        currentPlayerTurn = turnOrder[0];
        SpawnPlayers();
        SortTurnOrderBySpeed();
        CameraPosition();
        dungeonItem = GameObject.Find("DungeonItems");
        if (dungeonItem != null)
        {
            detectSpawnEnemiesRooms = dungeonItem.GetComponentsInChildren<DetectSpawnEnemies>().Where(x => x.CompareTag(Harmony.Tags.Room)).ToList();
            detectRoomExplored = dungeonItem.GetComponentsInChildren<DetectRoomExplored>().Where(x => x.CompareTag(Harmony.Tags.Room)).ToList();
            treasureList = dungeonItem.GetComponentsInChildren<OpenTreasure>().Where(x => x.CompareTag(Harmony.Tags.Treasure)).ToList();
            LoadGame();
            ChangeWeapon();
        }
    }

    private void LoadGame()
    {
        string loadType = PlayerPrefs.GetString("load");

        switch (loadType)
        {
            case "loadGame":
                LoadGameProgression();
                break;
            case "nextLevel":
                LoadInfo();
                break;
            case "newGame":
                LoadWeapon();
                currentTurn = 1;
                break;
            default:
                Debug.LogWarning("Type de chargement non reconnu : " + loadType);
                break;
        }
    }

    public void AddEnemyToTurnOrder(GameObject enemy)
    {
        turnOrder.Add(enemy);
    }

    public void RemoveEnemyFromTurnOrder(GameObject enemy)
    {
        if (turnOrder.Contains(enemy))
        {
            if (turnOrder.IndexOf(enemy) <= currentPlayerTurnIndex) currentPlayerTurnIndex--;
            turnOrder.Remove(enemy);
            OnDeleteEnemy?.Invoke();
            characterPortraitManager.UpdatePortrait();
        }
    }

    private void CameraPosition()
    {
        Vector3 cameraPosition = new Vector3(currentPlayerTurn.transform.position.x, aerialCameraHolder.transform.position.y, currentPlayerTurn.transform.position.z);
        aerialCameraHolder.transform.position = cameraPosition;
    }

    public void SaveCurrentProgression()
    {
        progressionData = new SaveProgressionData();
        progressionData.scene = SceneManager.GetActiveScene().buildIndex;
        progressionData.turn = GetCurrentTurn();
        PlayerPrefs.SetInt("loadedGameSeed", (int)GameObject.Find("DungeonSnapGridFlow").GetComponent<Dungeon>().Config.Seed);
        for (int i = 0; i < playerList.Count; i++)
        {
            progressionData.playerDataList.Add(new PlayerData(playerList[i].name, playerList[i].transform.position));
        }   
        foreach (DetectSpawnEnemies room in detectSpawnEnemiesRooms)
        {
            progressionData.roomsCleared.Add(room.roomCleared);
            progressionData.roomsEntered.Add(room.roomEntered);
        }
        foreach (DetectRoomExplored room in detectRoomExplored)
        {
            progressionData.roomsExplored.Add(room.roomExplored);
        }
        foreach (OpenTreasure treasure in treasureList)
        {
            if(treasure != null)
            {
                progressionData.availableChests.Add(treasure.isActiveAndEnabled);
            }
            else
            {
                progressionData.availableChests.Add(false);
            }
        }
        progression.SaveProgressionData(progressionData);
        SaveInfo();
    }

    private void SaveInfo()
    {
        PlayerPrefs.SetInt("Turn", GetCurrentTurn());
        for (int i = 0; i < playerList.Count; i++)
        {
            playerList[i].GetComponent<CharacterStat>().SaveCharacterStat(i);
            playerList[i].GetComponent<CharacterObjectInventory>().SaveCharacterObjectInventory(i);
            playerList[i].GetComponent<CharacterWeaponInventory>().SaveWeaponData(i);
        }
    }

    private void LoadGameProgression()
    {
        progressionData = progression.LoadProgressionData();
        PlayerPrefs.SetInt("Turn", progressionData.turn);
        for (int i = 0; i < Mathf.Min(playerList.Count, progressionData.playerDataList.Count); i++)
        {
            if (i < playerList.Count && i < progressionData.playerDataList.Count)
            {
                playerList[i].transform.position = progressionData.playerDataList[i].playerPosition;
            }
        }
        for (int i = 0; i < progressionData.roomsCleared.Count; i++)
        {
            detectSpawnEnemiesRooms[i].roomCleared = progressionData.roomsCleared[i];
            detectSpawnEnemiesRooms[i].roomEntered = progressionData.roomsEntered[i];
        }
        for (int i = 0; i < progressionData.roomsExplored.Count; i++)
        {
            detectRoomExplored[i].roomExplored = progressionData.roomsExplored[i];
        }
        for (int i = 0; i < progressionData.availableChests.Count; i++)
        {
            treasureList[i].gameObject.SetActive(progressionData.availableChests[i]);
        }
        CameraPosition();
        LoadInfo();
    }

    private void LoadInfo()
    {
        currentTurn = PlayerPrefs.GetInt("Turn", 1);
        OnTurnEnd?.Invoke();
        for (int i = 0; i < playerList.Count; i++)
        {
            playerList[i].GetComponent<CharacterStat>().LoadCharacterStat(i);
            playerList[i].GetComponent<CharacterObjectInventory>().LoadCharacterObjectInventory(i);
            playerList[i].GetComponent<CharacterWeaponInventory>().LoadWeaponData(i);
        }
    }

    private void LoadWeapon()
    {
        for (int i = 0; i < playerList.Count; i++)
        {
            playerList[i].GetComponent<CharacterWeaponInventory>().LoadWeaponData(i);
        }
    }

    private void ChangeWeapon()
    {
        for (int i = 0; i < playerList.Count; i++)
        {
            playerList[i].GetComponent<PlayerAnimatorManager>().ChangeWeaponAnimation();
        }
    }

    private void SpawnPlayers()
    {
        for (int i = 0; i < turnOrder.Count; i++)
        {
            turnOrder[i].transform.position = spawnPoints[i].transform.position;
        }
    }

    public int GetCurrentTurn()
    {
        return currentTurn;
    }

    public void SortTurnOrderBySpeed()
    {
        List<GameObject> tempList = new List<GameObject>();
        int charactersInTurnOrder = turnOrder.Count;

        for (int i = 0; i < charactersInTurnOrder; i++)
        {
            int highestSpeed = -9999;
            int highestSpeedIndex = -1;
            for (int j = 0; j < turnOrder.Count; j++)
            {
                int characterSpeed = turnOrder[j].CompareTag(Harmony.Tags.MainCharacter) ? turnOrder[j].GetComponent<CharacterStat>().Speed : turnOrder[j].GetComponent<EnemyInfo>().GetSpeed();
                if (characterSpeed > highestSpeed)
                {
                    highestSpeedIndex = j;
                    highestSpeed = characterSpeed;
                }
                
            }
            tempList.Add(turnOrder[highestSpeedIndex]);
            turnOrder.Remove(turnOrder[highestSpeedIndex]);
        }
        turnOrder = tempList;
        //recommencer au d�but du turn order
        currentPlayerTurnIndex = turnOrder.Count;
        actionPoints.PassTurn();
    }

    public void NextPlayerTurn()
    {
        uIController.DeactivatePlayerTargetEnemyCanvas();
        uIController.DeactivatePlayerTargetBarrelCanvas();

        currentPlayerTurnIndex = currentPlayerTurnIndex < turnOrder.Count - 1 ? currentPlayerTurnIndex+1 : 0;
        if (currentPlayerTurnIndex == 0 && turnCompleted)
        {
            turnCompleted = false;
            currentTurn++;
            OnTurnEnd?.Invoke();
        }
        else if (currentPlayerTurnIndex == turnOrder.Count - 1)
        {
            turnCompleted = true;
        }
        currentPlayerTurn = turnOrder[currentPlayerTurnIndex];
        if (!currentPlayerTurn.CompareTag(Harmony.Tags.MainCharacter)) {
            uIController.DeactivatePlayerTurnCanvas();
            uIController.ActivateEnnemiTurnCanvas();
            cameraGridControl.SetCurrentCharacter(currentPlayerTurn);
            currentPlayerTurn.GetComponent<EnemyActions>().PlayTurn(actionPoints);
        }
        else
        {
            uIController.ActivatePlayerTurnCanvas();
            uIController.DeactivateEnnemiTurnCanvas();
            objectUiController.UpdateInventoryUI(currentPlayerTurn.GetComponent<CharacterObjectInventory>().GetObjects());
            weaponAndMunitionUiController.UpdateWeaponAndAmmoUI();
            cameraGridControl.SetCurrentCharacter(currentPlayerTurn);

            currentPlayerMove = currentPlayerTurn.GetComponent<Move>();
            currentPlayerTurn.GetComponent<CharacterStat>().HealByTurn();

            Vector3 playerPosition = currentPlayerTurn.transform.position;
            playerPosition.y = 0.1f;
            currentPlayerMove.canMove = true;
            cameraGridControl.SetValidityOfNextMove(false);
            actionBarManager.InitializeActions();
        }
        if(currentPlayerTurnIndex == 0)
        {
            SetNbTurnsLeftUntilDeathForAllUnconsciousPlayers();
        }
        characterPortraitManager.UpdatePortrait();
    }

    public void RemovePlayerOffTurnOrderIfUnconscious(GameObject unconsciousPlayer)
    {
        if (turnOrder.IndexOf(unconsciousPlayer) <= currentPlayerTurnIndex) currentPlayerTurnIndex--;
        if (unconsciousPlayer == currentPlayerTurn) NextPlayerTurn();
        turnOrder.Remove(unconsciousPlayer);
        UpdateNumberOfActivePlayers();
        if (numberOfActivePlayers == 0)
        {
            uIController.ActivateEndGameCanvas(false);
        }
        characterPortraitManager.UpdatePortrait();
        unconsciousPlayersList.Add(unconsciousPlayer);
        actionBarManager.InitializeActions();
    }

    private void UpdateNumberOfActivePlayers()
    {
        int numberOfPlayers = 0;
        foreach (var character in turnOrder)
        {
            if (character.CompareTag(Harmony.Tags.MainCharacter))
            {
                numberOfPlayers++;
            }
        }
        numberOfActivePlayers = numberOfPlayers;
    }

    public int GetNumberOfActivePlayers() { return numberOfActivePlayers; }

    private void SetNbTurnsLeftUntilDeathForAllUnconsciousPlayers()
    {
        for (int i = 0; i < unconsciousPlayersList.Count; i++)
        {
            unconsciousPlayersList[i].GetComponent<CharacterInactiveStatus>().SetNbTurnsLeftUntilDeath();
            if (unconsciousPlayersList[i].activeSelf == false) unconsciousPlayersList.Remove(unconsciousPlayersList[i]);
        }
    }
    public GameObject GetCurrentPlayerTurn()
    {
        return currentPlayerTurn;
    }

    public List<GameObject> GetNextCharacters()
    {
        // R�cup�rer l'index du prochain joueur
        int nextIndex = currentPlayerTurnIndex;
        if (nextIndex == turnOrder.Count - 1)
        {
            nextIndex = 0;
        }

        // Cr�er une liste temporaire pour stocker les prochains personnages
        List<GameObject> nextCharacters = new List<GameObject>();

        // Ajouter les prochains personnages dans la liste temporaire
        for (int i = 0; i < 6; i++) // Limiter � 6 personnages
        {
            if (nextIndex >= turnOrder.Count || nextIndex < 0) nextIndex = 0;
            nextCharacters.Add(turnOrder[nextIndex]);
            nextIndex++;
        }

        return nextCharacters;
    }

    public List<GameObject> GetPlayers()
    {
        return playerList;
    }

    public void LoadNextLevel()
    {
        // Rcuprer l'index de la scne actuelle
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (SceneManager.GetActiveScene().name == Harmony.Scenes.Level1)
        {
            if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.FIRST_LEVEL_COMPLETED))
            {     
                AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.FIRST_LEVEL_COMPLETED);   
            }
        }
   
        SaveInfo();
        if(currentSceneIndex == LAST_LEVEL_INDEX) {
            uIController.ActivateEndGameCanvas(true);
        }
        else
        {
            PlayerPrefs.SetString("load", "nextLevel");
            SceneManager.LoadScene(currentSceneIndex + 1);
        }

    }
}
