using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private Enemy enemyData; 
    private EnemyInfo enemyInfo; 

    private void Start()
    {
        enemyInfo = GetComponent<EnemyInfo>();

        if (enemyInfo != null && enemyData != null)
        {
            enemyInfo.InitializeEnemy(enemyData);
        }
        else
        {
            Debug.LogError("EnemyInfo or Enemy data is not assigned!");
        }
    }
}
