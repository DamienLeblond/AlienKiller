using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyCreator", menuName = "Enemy")]
public class Enemy : ScriptableObject
{
    public new string name;
    public int critChance;
    public int health;
    public int minDamage;
    public int maxDamage;
    public int range;
    public int numberOfActionToShoot;
    public int speed;
    public int armor;
    public int moveDistance;
    public GameObject prefab;
    public Sprite iconImage;
    public Sprite portraitImage;
}
