using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInfo : MonoBehaviour
{
    private string enemyName;
    private int critChance;
    private int chanceToBeHit;
    private int chanceToBeCrit;
    private int baseChanceToBeCrit = 10;
    private int bonusCritWhenFlanked = 20;
    private bool flanked = false;
    private float flankDistanceThreshold = 4f;
    private int maxHealth;
    private int minDamage;
    private int maxDamage;
    private int range;
    private int numberOfActionToShoot;
    private int highestChanceToBeHit;
    [SerializeField] private int speed;
    private int armor;
    private int moveDistance;
    private Sprite iconImage;
    private Sprite portraitImage;
    private GameObject prefab;

    private int baseMinDamage;
    private int baseMaxDamage;
    private int baseMaxHealth;

    public void InitializeEnemy(Enemy enemy)
    {
        enemyName = enemy.name;
        critChance = enemy.critChance;
        maxHealth = enemy.health;
        baseMaxHealth = enemy.health;
        armor = enemy.armor;
        minDamage = enemy.minDamage;
        baseMinDamage = enemy.minDamage;
        maxDamage = enemy.maxDamage;
        baseMaxDamage = enemy.maxDamage;
        range = enemy.range;
        numberOfActionToShoot = enemy.numberOfActionToShoot;
        speed = enemy.speed;
        moveDistance = enemy.moveDistance;
        prefab = enemy.prefab;
        iconImage = enemy.iconImage;
        portraitImage = enemy.portraitImage;
        highestChanceToBeHit = 0;
    }

    public string GetName()
    {
        return enemyName;
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public int GetArmor()
    {
        return armor;
    }

    public int GetMinDamage()
    {
        return minDamage;
    }

    public int GetMaxDamage()
    {
        return maxDamage;
    }

    public int GetRange()
    {
        return range;
    }

    public int GetNumberOfActionToShoot()
    {
        return numberOfActionToShoot;
    }

    public Sprite GetIconImage()
    {
        return iconImage;
    }

    public Sprite GetPortraitSprite()
    {
        return portraitImage;
    }

    public int GetSpeed()
    {
        return speed;
    }

    public int GetMoveDistance()
    {
        return moveDistance;
    }

    public GameObject GetPrefab()
    {
        return prefab;
    }

    public int GetCritChance()
    {
        return critChance;
    }

    public int GetChanceToBeHit()
    {
        return chanceToBeHit;
    }

    public void BoostAttack(float bonusPercentage)
    {
        minDamage += (int)(baseMinDamage * bonusPercentage);
        maxDamage += (int)(baseMaxDamage * bonusPercentage);
    }

    public void BoostMaxHealth(float bonusPercentage, EnemyHealth enemyHealth)
    {
        maxHealth += (int)(baseMaxHealth * bonusPercentage);
        enemyHealth.SetCurrentHealthAfterStatIncrease((int)(baseMaxHealth * bonusPercentage));
    }

    public void SetChanceToBeHit(int chance)
    {
        chanceToBeHit = chance;
        if(chanceToBeHit > highestChanceToBeHit)
        {
            SetHighestChanceToBeHit(chanceToBeHit);
        }
    }
    public int GetChanceToBeCrit()
    {
        return chanceToBeCrit;
    }

    public void SetChanceToBeCrit(int bonusChance)
    {
        chanceToBeCrit = baseChanceToBeCrit + bonusChance;
        if (flanked)
        {
            chanceToBeCrit += bonusCritWhenFlanked;
        }
    }

    public int GetHighestChanceToBeHit()
    {
        return highestChanceToBeHit;
    }

    public void SetHighestChanceToBeHit(int chance)
    {
        highestChanceToBeHit = chance;
    }

    public void IsEnemyFlanked(GameObject player)
    {
        Vector3 enemyPosition = gameObject.transform.position;
        Vector3 playerPosition = player.transform.position;

        // Recherche de tous les objets avec le tag "Obstacle" dans une sph�re autour de l'ennemi
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag(Harmony.Tags.Obstacle);

        bool isFlanked = false; // Bool�en pour indiquer si l'ennemi est flanqu�

        foreach (GameObject obstacle in obstacles)
        {
            // V�rifier si l'objet est � une distance de 3 en x ou en y
            float distanceX = Mathf.Abs(playerPosition.x - obstacle.transform.position.x);
            float distanceY = Mathf.Abs(playerPosition.y - obstacle.transform.position.y);
            if (distanceX <= 3 || distanceY <= 3)
            {
                // V�rifie si l'ennemi se trouve entre le joueur et l'obstacle
                Vector3 playerToEnemy = enemyPosition - playerPosition;
                Vector3 playerToObstacle = obstacle.transform.position - playerPosition;
                float distancePlayerToObstacle = Vector3.Distance(playerPosition, obstacle.transform.position);
                float distancePlayerToEnemy = Vector3.Distance(playerPosition, enemyPosition);
                float angle = Vector3.Angle(playerToEnemy, playerToObstacle);

                if (angle < 90f)
                {
                    // L'ennemi est entre le joueur et l'obstacle, v�rifiez la distance
                    float distanceToObstacle = Vector3.Distance(enemyPosition, obstacle.transform.position);
                    if (distanceToObstacle <= flankDistanceThreshold) // L'ennemi est suffisamment proche de l'obstacle pour �tre flanqu�
                    {
                        if (distancePlayerToObstacle > distancePlayerToEnemy)
                        {
                            SetFlanking(true);
                            isFlanked = true; // L'ennemi est flanqu�, sortir de la boucle
                            break;
                        }
                    }
                }
            }
        }

        if (!isFlanked)
        {
            SetFlanking(false);
        }
    }




    public bool GetFlanked()
    {
        return flanked;
    }

    public void SetFlanking(bool isFlanked)
    {
        flanked = isFlanked;
    }
}
