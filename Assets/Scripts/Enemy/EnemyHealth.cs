using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private int currentHealth;
    private EnemyInfo info;

    private void Start()
    {
        info = GetComponent<EnemyInfo>();
    }

    public bool GetHit(int damage)
    {
        currentHealth -= damage - info.GetArmor();
        if (currentHealth < 0)
        {
            return true; //pour les comp�tence qui a besoin de savoir si on a tu� quelqu'un
        }
        return false;

    }

    public void SetEnemyHealth()
    {
        currentHealth = info.GetMaxHealth();
    }

    public void SetCurrentHealthAfterStatIncrease(int bonusHealth)
    {
        currentHealth += bonusHealth;
    }

    public int GetHealth()
    {
        return currentHealth;
    }

    internal bool IsFullHealth()
    {
        if (currentHealth == info.GetMaxHealth())
        {
            return true;
        }
        return false;
    }

    internal float GetRemainingPercentage()
    {
        return (float)currentHealth / (float)info.GetMaxHealth();
    }

    internal float GetPotentialDamagePercentage(int damage)
    {
        // Calcul du pourcentage des d�g�ts potentiels par rapport � la sant� maximale
        float potentialDamagePercentage = (float)damage / (float)GetHealth();

        // Inversion de la valeur en soustrayant le pourcentage de 1
        float invertedPercentage = 1f - potentialDamagePercentage;

        float clampedPercentage = Mathf.Clamp(invertedPercentage, 0f, 1f);

        return clampedPercentage;
    }
}
