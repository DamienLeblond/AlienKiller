using System.Collections;
using UnityEngine;

public interface EnemyActions
{
    void PlayTurn(ActionPoints actionPoints);
    void ChooseAction();
    void Shoot();
    void Move();
}
