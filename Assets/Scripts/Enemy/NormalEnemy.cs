using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using static UnityEngine.InputSystem.DefaultInputActions;

public class NormalEnemy : MonoBehaviour, EnemyActions
{
    private ActionPoints actionPoints;
    private ShootPlayer shoot;
    private PathFinding pathFinding;
    private EnemyInfo enemy;
    [SerializeField] private float actionDelay;
    CustomGrid targetGrid;
    private Vector2Int nextEnemyPosition;
    private GameObject closestPlayer;
    private Move move;
    private int nbActions;

    void Start()
    {
        shoot = GetComponent<ShootPlayer>();
        pathFinding = FindFirstObjectByType<PathFinding>();
        targetGrid = FindFirstObjectByType<CustomGrid>();
    }

    public void PlayTurn(ActionPoints points)
    {
        actionPoints = points;
        nbActions = actionPoints.GetActionPoints();
        enemy = GetComponent<EnemyInfo>();
        move = GetComponent<Move>();
        move.canMove = true;
        FindClosestCharacter();
        Invoke("ChooseAction", actionDelay);
    }

    public void ChooseAction()
    {
        shoot.CalculateChanceToHitVisibleTargets();
        if (move.canMove && !shoot.IsChanceToHitAbove60Percent())
        {
            Move();
            nbActions--;
            if (nbActions <= 0)
            {
                actionPoints.UseActionPoints(1);
                return;
            }
        }
        else if (shoot.CanShootCharacter())
        {
            Shoot();
            nbActions -= enemy.GetNumberOfActionToShoot();
            if (nbActions <= 0)
            {
                actionPoints.UseActionPoints(enemy.GetNumberOfActionToShoot());
                return;
            }
        }
        else
        {
            nbActions--;
        }
        actionPoints.UseActionPoints(1);
        if(nbActions > 0)
        {
            Invoke("ChooseAction", actionDelay);
        }
    }


    public void Shoot()
    {
        shoot.ShootCharacter();
    }

    public void Move()
    {
        if (!move.IsMoving())
        {
            Vector3 currentEnemyWorldPosition = transform.position;
            Vector2Int enemyPositionGrid = targetGrid.GetGridPosition(currentEnemyWorldPosition);

            Vector3 currentCharacterWorldPosition = closestPlayer.transform.position;
            Vector2Int closestPlayerPosition = targetGrid.GetGridPosition(currentCharacterWorldPosition);

            List<PathNode>  path = pathFinding.FindPath(enemyPositionGrid.x, enemyPositionGrid.y, closestPlayerPosition.x, closestPlayerPosition.y);
            if (path != null)
            {
                move.canEnemyMove = true;
                if (path.Count > enemy.GetMoveDistance())
                {
                    path.RemoveRange(enemy.GetMoveDistance(), path.Count - enemy.GetMoveDistance());
                }

                nextEnemyPosition = new Vector2Int(path[path.Count - 1].pos_x, path[path.Count - 1].pos_y);
                move.SetEnemyPosition(nextEnemyPosition, path, enemy);
            }
            else
            {
                Debug.Log("No valid path found.");
            }
        }
    }
    private void FindClosestCharacter()
    {
        GameObject[] mainCharacters = GameObject.FindGameObjectsWithTag(Harmony.Tags.MainCharacter);

        if (mainCharacters.Length > 0)
        {
            float closestDistance = Mathf.Infinity;

            foreach (GameObject mainCharacter in mainCharacters)
            {
                float distanceToPlayer = Vector3.Distance(transform.position, mainCharacter.transform.position);

                if (distanceToPlayer < closestDistance)
                {
                    closestDistance = distanceToPlayer;
                    closestPlayer = mainCharacter;

                }
            }
        }
    }
}
