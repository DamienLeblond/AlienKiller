using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseEnemyDifficulty : MonoBehaviour
{
    private GameManager gameManager;
    private EnemyInfo enemyInfo;
    private EnemyHealth enemyHealth;
    [SerializeField] private int nbTurnsBeforeIncrease;
    private int nbTurnsBeforeNextIncrease;
    [SerializeField] private float bonus;

    void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();
        enemyInfo = GetComponent<EnemyInfo>();
        enemyHealth = GetComponent<EnemyHealth>();
        enemyHealth.SetEnemyHealth();
        ApplyPreviousStatsIncrease();
    }

    private void OnEnable()
    {
        GameManager.OnTurnEnd += HandleTurnEnd;
    }

    private void OnDisable()
    {
        GameManager.OnTurnEnd -= HandleTurnEnd;
    }

    private void HandleTurnEnd()
    {
        if (gameManager.GetCurrentTurn() == nbTurnsBeforeNextIncrease)
        {
            ApplyStatsIncrease();
            nbTurnsBeforeNextIncrease += nbTurnsBeforeIncrease;
        }
    }

    private void ApplyStatsIncrease()
    {
        enemyInfo.BoostAttack(bonus);
        enemyInfo.BoostMaxHealth(bonus, enemyHealth);
    }

    private void ApplyPreviousStatsIncrease()
    {
        int nbStatsIncrease = gameManager.GetCurrentTurn() / nbTurnsBeforeIncrease;
        nbTurnsBeforeNextIncrease = (nbTurnsBeforeIncrease * nbStatsIncrease) + nbTurnsBeforeIncrease;
        for (int i = 0; i < nbStatsIncrease; i++)
        {
            ApplyStatsIncrease();
        }
    }
}
