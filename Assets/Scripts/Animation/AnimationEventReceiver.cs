using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimatorEventReceiver : MonoBehaviour
{
    private CameraEventManager cameraEventManager;

    public void ReturnToAerialCamera(AnimationEvent animationEvent)
    {
        cameraEventManager = FindAnyObjectByType<CameraEventManager>();
        cameraEventManager.ReturnToAerialCamera();

    }
}
