using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public interface IEnnemiAnimatorManager
{
    void StartShootingAnimation();
    void OnHitAnimationFinished();
    void AfterDeadAnimation();
    void FireShoot();
    void GetHit(Vector3 bulletDirection);
    void PlayHurtSound();
    void GetMeleeHit();
}
