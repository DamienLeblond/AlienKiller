using Harmony;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalAnimatorManager : MonoBehaviour, IEnnemiAnimatorManager, IMovingAnimator
{
    private Animator ennemiAnimator;
    private EnemyHealth enemyHealth;
    private bool alreadyHit = false;
    private GameObject blood;
    private float distanceBehind = 0.5f;
    [SerializeField] private EnemyHealthBarUpdater healthBarUpdater;
    [SerializeField] private AudioClip hurtClip;
    private GameObject weapon;
    private GameManager gameManager;

    void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();
        ennemiAnimator = GetComponent<Animator>();
        enemyHealth = GetComponent<EnemyHealth>();
        blood = GameObject.Find("RobotBlood");
        weapon = this.gameObject.transform.GetChild(0).gameObject;
    }

    public void OnHitAnimationFinished()
    {
        alreadyHit = false;
    }

    public void StartShootingAnimation()
    {
        ennemiAnimator.Play("Fire");
    }

    public void AfterDeadAnimation()
    {
        this.gameObject.SetActive(false);
    }


    public void FireShoot()
    {
        IWeaponEffect weaponEffect = weapon.GetComponent<IWeaponEffect>();

        ShootPlayer shootPlayer = GetComponent<ShootPlayer>();

        shootPlayer.ShootEffect(weaponEffect);
    }

    public void GetHit(Vector3 bulletDirection)
    {
        // V�rifier si l'objet n'a pas d�j� �t� touch�
        if (!alreadyHit)
        {
            // Marquer l'objet comme touch�
            alreadyHit = true;

            // Jouer le son du personnage bless�
            PlayHurtSound();

            Vector3 bloodPosition = transform.position + bulletDirection.normalized * distanceBehind;

            // Placer le GameObject "Blood" � la position calcul�e
            GameObject bloodInstance = Instantiate(blood, bloodPosition, Quaternion.identity);

            // Appliquer la rotation aux syst�mes de particules enfants du GameObject "Blood"
            var bloodParticleSystem1 = bloodInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
            var bloodParticleSystem2 = bloodInstance.transform.GetChild(1).GetComponent<ParticleSystem>();
            if (bloodParticleSystem1 != null && bloodParticleSystem2 != null)
            {
                bloodParticleSystem1.Play();
                bloodParticleSystem2.Play();
            }

            healthBarUpdater.UpdateValue();


            if (enemyHealth.GetHealth() <= 0)
            {
                ennemiAnimator.SetBool("Dead", true);
                if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.FIRST_OIL))
                {
                    AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.FIRST_OIL);
                }

                gameManager.RemoveEnemyFromTurnOrder(this.gameObject);
            }

            ennemiAnimator.Play("Hit");
        }
    }

    public void GetMeleeHit()
    {
        ennemiAnimator.Play("Hit");

        Vector3 bloodOffset = new Vector3(0f, 1f, -1f);

        // Jouer le son du personnage bless�
        PlayHurtSound();

        // Convertir l'offset de local � global en tenant compte de la rotation du personnage
        Vector3 globalBloodOffset = this.gameObject.transform.TransformDirection(bloodOffset);

        // Ajouter l'offset � la position du personnage pour obtenir la position du sang
        Vector3 bloodPosition = this.gameObject.transform.position + globalBloodOffset;

        // Placer le GameObject "Blood" � la position calcul�e
        GameObject bloodInstance = Instantiate(blood, bloodPosition, Quaternion.identity);

        // Appliquer la rotation aux syst�mes de particules enfants du GameObject "Blood"
        var bloodParticleSystem1 = bloodInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
        var bloodParticleSystem2 = bloodInstance.transform.GetChild(1).GetComponent<ParticleSystem>();
        if (bloodParticleSystem1 != null && bloodParticleSystem2 != null)
        {

            bloodParticleSystem1.Play();
            bloodParticleSystem2.Play();
        }

        healthBarUpdater.UpdateValue();


        if (enemyHealth.GetHealth() <= 0)
        {
            ennemiAnimator.SetBool("Dead", true);
            if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.FIRST_OIL))
            {
                AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.FIRST_OIL);
            }

            gameManager.RemoveEnemyFromTurnOrder(this.gameObject);
        }
    }

    public void PlayHurtSound()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.clip = hurtClip;
        audioSource.volume = 0.8f;
        audioSource.pitch = 1;
        audioSource.Play();
    }

    public void StartMoving()
    {
        ennemiAnimator.SetBool("Sprint", true);
    }

    public void StopMoving()
    {
        ennemiAnimator.SetBool("Sprint", false);
    }

    public void HideGun()
    {
        weapon.SetActive(false);
    }

    public void ShowGun()
    {
        weapon.SetActive(true);
    }
}
