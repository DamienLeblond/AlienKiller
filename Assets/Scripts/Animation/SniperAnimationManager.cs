using Harmony;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperAnimationManager : MonoBehaviour, IEnnemiAnimatorManager, IMovingAnimator
{
    private Animator ennemiAnimator;
    private EnemyHealth enemyHealth;
    private bool alreadyHit = false;
    [SerializeField] GameObject weaponPrefab;
    private Transform weaponR;
    private GameObject blood;
    private float distanceBehind = 0.5f;
    [SerializeField] private EnemyHealthBarUpdater healthBarUpdater;
    [SerializeField] private AudioClip hurtClip;
    private GameManager gameManager;

    void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();
        ennemiAnimator = GetComponent<Animator>();
        enemyHealth = GetComponent<EnemyHealth>();
        blood = GameObject.Find("RobotBlood");
    }

    public void OnHitAnimationFinished()
    {
        alreadyHit=false;
    }

    public void StartShootingAnimation()
    {
        ennemiAnimator.Play("Fire");
    }

    public void AfterDeadAnimation()
    {
        this.gameObject.SetActive(false);
    }


    public void FireShoot()
    {
        GameObject weapon = weaponR.transform.GetChild(0).gameObject;

        IWeaponEffect weaponEffect = weapon.GetComponent<IWeaponEffect>();

        ShootPlayer shootPlayer = GetComponent<ShootPlayer>();

        shootPlayer.ShootEffect(weaponEffect);
    }

    public void TakeGun()
    {
        weaponR = transform.Find("root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/Weapon_R");

        // V�rifier si le nouveau mod�le d'arme existe
        if (weaponPrefab != null)
        {
            Vector3 prefabPosition = weaponPrefab.transform.localPosition;
            Quaternion prefabRotation = weaponPrefab.transform.localRotation;
            Vector3 prefabScale = weaponPrefab.transform.localScale;

            // Instancier le prefab du nouveau mod�le d'arme avec ses valeurs de position, de rotation et d'�chelle d'origine
            GameObject newWeapon = Instantiate(weaponPrefab, weaponR.position, weaponR.rotation);

            // D�finir le parent de la nouvelle arme sur "Weapon_R" pour la placer dans les mains du personnage
            newWeapon.transform.SetParent(weaponR, false);

            // R�tablir les valeurs de position, de rotation et d'�chelle du prefab sur la nouvelle arme
            newWeapon.transform.localPosition = prefabPosition;
            newWeapon.transform.localRotation = prefabRotation;
            newWeapon.transform.localScale = prefabScale;

            
            newWeapon.AddComponent<AudioSource>();
        }
    }

    public void GetHit(Vector3 bulletDirection)
    {
        // V�rifier si l'objet n'a pas d�j� �t� touch�
        if (!alreadyHit)
        {
            // Marquer l'objet comme touch�
            alreadyHit = true;

            // Jouer le son du personnage bless�
            PlayHurtSound();

            Vector3 bloodPosition = transform.position + bulletDirection.normalized * distanceBehind;

            // Placer le GameObject "Blood" � la position calcul�e
            GameObject bloodInstance = Instantiate(blood, bloodPosition, Quaternion.identity);

            // Appliquer la rotation aux syst�mes de particules enfants du GameObject "Blood"
            var bloodParticleSystem1 = bloodInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
            var bloodParticleSystem2 = bloodInstance.transform.GetChild(1).GetComponent<ParticleSystem>();
            if (bloodParticleSystem1 != null && bloodParticleSystem2 != null)
            {
                bloodParticleSystem1.Play();
                bloodParticleSystem2.Play();
            }

            healthBarUpdater.UpdateValue();


            if (enemyHealth.GetHealth() <= 0)
            {
                ennemiAnimator.SetBool("Dead", true);
                if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.FIRST_OIL))
                {
                    AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.FIRST_OIL);
                }

                gameManager.RemoveEnemyFromTurnOrder(this.gameObject);
            }
            // Jouer l'animation de se faire toucher
            ennemiAnimator.Play("Hit");
        }
    }

    public void GetMeleeHit()
    {
        ennemiAnimator.Play("Hit");

        Vector3 bloodOffset = new Vector3(0f, 1f, -1f);

        // Jouer le son du personnage bless�
        PlayHurtSound();

        // Convertir l'offset de local � global en tenant compte de la rotation du personnage
        Vector3 globalBloodOffset = this.gameObject.transform.TransformDirection(bloodOffset);

        // Ajouter l'offset � la position du personnage pour obtenir la position du sang
        Vector3 bloodPosition = this.gameObject.transform.position + globalBloodOffset;

        // Placer le GameObject "Blood" � la position calcul�e
        GameObject bloodInstance = Instantiate(blood, bloodPosition, Quaternion.identity);

        // Appliquer la rotation aux syst�mes de particules enfants du GameObject "Blood"
        var bloodParticleSystem1 = bloodInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
        var bloodParticleSystem2 = bloodInstance.transform.GetChild(1).GetComponent<ParticleSystem>();
        if (bloodParticleSystem1 != null && bloodParticleSystem2 != null)
        {

            bloodParticleSystem1.Play();
            bloodParticleSystem2.Play();
        }

        healthBarUpdater.UpdateValue();


        if (enemyHealth.GetHealth() <= 0)
        {
            ennemiAnimator.SetBool("Dead", true);
            if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.FIRST_OIL))
            {
                AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.FIRST_OIL);
            }

            gameManager.RemoveEnemyFromTurnOrder(this.gameObject);
        }
    }
    public void PlayHurtSound()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.clip = hurtClip;
        audioSource.volume = 0.6f;
        audioSource.pitch = 1.2f;
        audioSource.Play();
    }

    public void StartMoving()
    {
        ennemiAnimator.SetBool("Sprint", true);
    }

    public void StopMoving()
    {
        ennemiAnimator.SetBool("Sprint", false);
    }
}
