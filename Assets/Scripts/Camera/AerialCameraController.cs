using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Unity.VisualScripting;

public class AerialCameraController : MonoBehaviour
{
    private float baseMovementSpeed = 5f;
    private float shiftMovementSpeed = 10f;
    private float rotationSpeed = 90f;
    private float slerpSpeed = 1000000f;
    private float zoomSpeed = 10f;
    float tolerance = 0.5f;
    float overwriteMinRotation = 0.2f;
    private float minZoomFOV = 30f; // Champ de vision minimum
    private float maxZoomFOV = 60f;

    public Transform cameraHolder;

    private bool isRotating = false;

    private CinemachineFreeLook freeLook;

    public Quaternion targetRotation = Quaternion.identity;

    private void Start()
    {
        freeLook = GetComponent<CinemachineFreeLook>();
        freeLook.m_Lens.FieldOfView = 50f;
    }

    private void Update()
    {
        float rotation = Input.GetAxis("Rotate");
        if (rotation != 0 && !isRotating)
        {
            StartRotation(rotation * rotationSpeed);
        }

        if (isRotating)
        {
            RotateCamera();
        }

        // D�placement avec WASD
        float horizontalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontalMovement, 0f, verticalMovement).normalized;

        // V�rifier si la touche Shift est enfonc�e pour augmenter la vitesse
        float movementSpeed = Input.GetKey(KeyCode.LeftShift) ? shiftMovementSpeed : baseMovementSpeed;

        movement = Quaternion.Euler(0f, transform.eulerAngles.y, 0f) * movement * movementSpeed;
        MoveCameraHolder(movement);

        float zoomInput = Input.GetAxis("Mouse ScrollWheel");
        if (zoomInput != 0)
        {
            ZoomCamera(zoomInput);
        }
    }

    private void ZoomCamera(float zoomInput)
    {
        // Calcul du nouveau FOV
        float newFOV = freeLook.m_Lens.FieldOfView - zoomInput * zoomSpeed;
        newFOV = Mathf.Clamp(newFOV, minZoomFOV, maxZoomFOV);

        // Application du nouveau FOV
        freeLook.m_Lens.FieldOfView = newFOV;
    }

    private void StartRotation(float angle)
    {
        // Cr�er un quaternion de rotation autour de l'axe Y
        Quaternion rotation = Quaternion.Euler(0f, angle, 0f);

        // Appliquer la rotation au Transform du cameraHolder
        targetRotation = cameraHolder.rotation * rotation;

        isRotating = true;
    }

    private void RotateCamera()
    {
        // Calculer l'angle entre la rotation actuelle et la rotation cible
        float angle = Quaternion.Angle(cameraHolder.rotation, targetRotation);

        // Calculer le facteur d'interpolation en fonction de l'angle
        float t = Mathf.Min(slerpSpeed * Time.deltaTime / angle, overwriteMinRotation);

        // Utiliser Quaternion.RotateTowards pour effectuer une rotation progressive
        cameraHolder.rotation = Quaternion.RotateTowards(cameraHolder.rotation, targetRotation, angle * t);

        // V�rifier si la rotation est termin�e
        if (Quaternion.Angle(cameraHolder.rotation, targetRotation) <= tolerance)
        {
            isRotating = false;
            cameraHolder.rotation = targetRotation;
        }
    }

    public void MoveCameraHolder(Vector3 direction)
    {
        Vector3 movement = direction * baseMovementSpeed * Time.deltaTime;
        cameraHolder.transform.Translate(movement, Space.World);
    }

}