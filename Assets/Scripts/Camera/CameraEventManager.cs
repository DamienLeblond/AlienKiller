using Cinemachine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Mime;

public class CameraEventManager : MonoBehaviour
{
    [SerializeField] private OnAirCameraManager onAirCameraManager;
    private CinemachineFreeLook freeLookCamera;
    private GameObject player;
    private List<Renderer> rendererThatBlockCamera = new();
    private List<Material> materialsThatBlockCamera = new();
    [SerializeField] Shader transparentShader;
    private Material transparentMaterial;

    private void Start()
    {
        CreateNewTransparentMaterial();

        // Chercher automatiquement CinemachineFreeLook dans la sc�ne
        freeLookCamera = FindAnyObjectByType<CinemachineFreeLook>();

        // V�rifier si les r�f�rences ont �t� trouv�es
        if (onAirCameraManager == null)
        {
            Debug.LogError("Erreur : OnAirCameraManager non trouv� dans la sc�ne.");
        }

        if (freeLookCamera == null)
        {
            Debug.LogError("Erreur : CinemachineFreeLook non trouv� dans la sc�ne.");
        }

        
    }
    private void CreateNewTransparentMaterial()
    {
        transparentMaterial = new Material(transparentShader);
        transparentMaterial.SetFloat("_Mode", 3); // Set rendering mode to Transparent
        transparentMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        transparentMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        transparentMaterial.SetInt("_ZWrite", 1);
        transparentMaterial.DisableKeyword("_ALPHATEST_ON");
        transparentMaterial.EnableKeyword("_ALPHABLEND_ON");
        transparentMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        transparentMaterial.renderQueue = 1;
    }

    public void SetPlayer(GameObject player)
    {
        this.player = player;
    }
    public void AimCameraPlayer(GameObject targetObject)
    {
        // Trouver la cam�ra Cinemachine attach�e � l'objet du joueur
        CinemachineVirtualCamera playerCamera = player.GetComponentInChildren<CinemachineVirtualCamera>();

        if (playerCamera != null)
        {
            SetActiveObjectsThatBlockedAimView();
            playerCamera.LookAt = targetObject.transform;
            playerCamera.Follow = player.transform;
            onAirCameraManager.SwitchToLiveCamera(playerCamera);
            DetectObjectsThatAreBlockingAimView(playerCamera.transform, targetObject);
        }
        else
        {
            Debug.LogWarning("Aucune cam�ra Cinemachine trouv�e sur l'objet du joueur.");
        }
    }

    private void DetectObjectsThatAreBlockingAimView(Transform cameraTransform, GameObject targetObject)
    {    
        List<RaycastHit> hits = new();
        foreach (var hit in GetRecastHits(cameraTransform.position, targetObject.transform.position)) hits.Add(hit);
        foreach (var hit in GetRecastHits(cameraTransform.position + (cameraTransform.right * 2), targetObject.transform.position)) hits.Add(hit);
        foreach (var hit in GetRecastHits(cameraTransform.position - (cameraTransform.right * 2), targetObject.transform.position)) hits.Add(hit);

        foreach (RaycastHit hit in hits) SetTransparentShader(hit, targetObject);
    }

    private RaycastHit[] GetRecastHits(Vector3 startingPosition, Vector3 targetPosition) {
        Vector3 heading = targetPosition - startingPosition;
        float distance = heading.magnitude;
        Vector3 direction = heading / distance;

        return Physics.RaycastAll(startingPosition, direction, distance);
    }

    private void SetTransparentShader(RaycastHit hit, GameObject targetObjectToAvoidDetect)
    {
        if (!hit.collider.gameObject.CompareTag(Harmony.Tags.Room) &&
             hit.collider.gameObject.layer != Harmony.Layers.Terrain.Index &&
             hit.collider.gameObject != targetObjectToAvoidDetect)
        {
            if (hit.collider.gameObject.TryGetComponent<Renderer>(out var renderer))
            {
                if (!rendererThatBlockCamera.Contains(renderer))
                {
                    rendererThatBlockCamera.Add(renderer);
                    materialsThatBlockCamera.Add(renderer.material);
                    renderer.material = transparentMaterial;
                }
            }
        }
    }

    public void SetActiveObjectsThatBlockedAimView()
    {
        int objectToGiveBack = rendererThatBlockCamera.Count;
        for (int i = 0; i < objectToGiveBack; i++)
        {
            rendererThatBlockCamera[i].material = materialsThatBlockCamera[i];
        }
        rendererThatBlockCamera.Clear();
        materialsThatBlockCamera.Clear();
    }

    public void AimCameraEnemy(GameObject targetObject)
    {
        // Trouver la cam�ra Cinemachine attach�e � l'objet du joueur
        CinemachineVirtualCamera enemyCamera = targetObject.GetComponentInChildren<CinemachineVirtualCamera>();

        if (enemyCamera != null)
        {
            enemyCamera.LookAt = player.transform;
            enemyCamera.Follow = targetObject.transform;
            onAirCameraManager.SwitchToLiveCamera(enemyCamera);
        }
        else
        {
            Debug.LogWarning("Aucune cam�ra Cinemachine trouv�e sur l'objet du joueur.");
        }
    }

    public void RevealCamera(GameObject targetObject)
    {
        // Trouver la cam�ra Cinemachine attach�e � l'objet du joueur
        CinemachineClearShot camera = targetObject.GetComponentInChildren<CinemachineClearShot>();
        CinemachineVirtualCamera virtualCamera = camera.GetComponentInChildren<CinemachineVirtualCamera>();

        if (camera != null)
        {
            onAirCameraManager.SwitchToLiveCamera(camera);
            DetectObjectsThatAreBlockingAimView(virtualCamera.transform, targetObject);
        }
        else
        {
            Debug.LogWarning("Aucune cam�ra Cinemachine trouv�e sur l'objet du joueur.");
        }
    }
    public void ReturnToAerialCamera()
    {
        SetActiveObjectsThatBlockedAimView();
        onAirCameraManager.SwitchToLiveCamera(freeLookCamera);
    }
}
