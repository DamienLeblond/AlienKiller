using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToArerialCameraAfterAnimation : MonoBehaviour
{
    private CameraEventManager cameraEventManager;
    private void Start()
    {
        // Chercher automatiquement OnAirCameraManager dans la sc�ne
        cameraEventManager = FindAnyObjectByType<CameraEventManager>();
    }

    public void ReturnToAerialCamera()
    {
        cameraEventManager.ReturnToAerialCamera();
    }

}
