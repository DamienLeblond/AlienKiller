using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAirCameraManager : MonoBehaviour
{
    [SerializeField] private CinemachineBrain mainCameraBrain;
    private AerialCameraController controller;

    private void Start()
    {
        // Cherche la cam�ra principale au d�marrage
        FindController();

        if (mainCameraBrain == null)
        {
            Debug.LogError("La cam�ra principale n'a pas de composant CinemachineBrain.");
        }
    }
    public void SwitchToLiveCamera(CinemachineVirtualCameraBase liveCamera)
    {
        if (liveCamera != null && mainCameraBrain != null)
        {
            FindController();
            //Si la camera que l'on veut activer est la camera libre - activer le d�placement, sinon le d�sactiver.
            if (liveCamera.GetComponent<AerialCameraController>() != null) controller.enabled = true;
            else controller.enabled = false;
            // D�sactivez toutes les cam�ras Cinemachine dans la sc�ne
            CinemachineVirtualCameraBase[] allCameras = FindObjectsByType<CinemachineVirtualCameraBase>(FindObjectsSortMode.None);
            foreach (CinemachineVirtualCameraBase camera in allCameras)
            {
                camera.Priority = 0;
            }
            liveCamera.Priority = 1;
        }
        else
        {
            Debug.LogWarning("La cam�ra en direct fournie est nulle.");
        }
    }

    private void FindController()
    {
        if (controller == null)
        {
            controller = FindAnyObjectByType<AerialCameraController>();
        }
            
    }
}
