using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuUI;
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject confirmPanel;

    private GameManager gameManager;

    void Start()
    {
        // Assurez-vous que le menu de pause est d�sactiv� au d�but
        if (pauseMenuUI != null)
        {
            pauseMenuUI.SetActive(false);
        }

        // Assurez-vous que le panel de confirmation est d�sactiv� au d�but
        if (confirmPanel != null)
        {
            confirmPanel.SetActive(false);
        }
        gameManager = FindAnyObjectByType<GameManager>();
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }
    }

    void TogglePauseMenu()
    {
        // Active/d�sactive le menu de pause
        if (pauseMenuUI != null)
        {
            pauseMenuUI.SetActive(!pauseMenuUI.activeSelf);
            mainPanel.SetActive(!mainPanel.activeSelf);

            // Mettre en pause ou reprendre le jeu en fonction de l'�tat du menu de pause
            Time.timeScale = pauseMenuUI.activeSelf ? 0f : 1f;
        }
    }

    public void ResumeGame()
    {
        // Reprendre le jeu en d�sactivant le menu de pause
        TogglePauseMenu();
    }

    public void ReturnToMainMenu()
    {
        // Afficher le panel de confirmation
        ConfirmQuit();
    }

    public void SaveAndReturnToMainMenu()
    {
        gameManager.SaveCurrentProgression();
        Time.timeScale = 1f;
        SceneManager.LoadScene(Harmony.Scenes.MainMenu);
    }

    public void SaveAndQuitGame()
    {
        gameManager.SaveCurrentProgression();
        Application.Quit();
    }

    public void ConfirmQuit()
    {
        // Afficher le panel de confirmation
        if (confirmPanel != null)
        {
            confirmPanel.SetActive(true);
        }
    }

    public void ConfirmQuitYes()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(Harmony.Scenes.MainMenu);
    }

    public void ConfirmQuitNo()
    {
        // Quand l'utilisateur annule l'abandon, d�sactivez le panel de confirmation
        if (confirmPanel != null)
        {
            confirmPanel.SetActive(false);
        }
    }
}
