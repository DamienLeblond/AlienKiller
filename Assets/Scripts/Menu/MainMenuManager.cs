using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    private const int TUTORIAL_INDEX_SCENE = 6;
    [SerializeField] private GameObject loadSaveButton;
    [SerializeField] private GameObject loadOut;
    private SaveProgression progression;
    private SaveProgressionData progressionData;
    void Start()
    {
        // V�rifie si une sauvegarde existe
        bool saveExists = CheckSaveExists();
        // D�sactive le bouton UI s'il n'y a pas de sauvegarde
        if (!saveExists)
        {
            loadSaveButton.SetActive(false);
        }
        progression = new SaveProgression();
    }

    bool CheckSaveExists()
    {
        string saveFileName = "progression.json";
        string saveFilePath = Path.Combine(Application.persistentDataPath, saveFileName);

        return File.Exists(saveFilePath);
    }

    public void LoadSave()
    {
        //loader la sauvegarde
        PlayerPrefs.SetString("load", "loadGame");
        progressionData = progression.LoadProgressionData();
        SceneManager.LoadScene(progressionData.scene);
    }

    public void CloseGame()
    {
        // Ferme l'application
        Application.Quit();
    }

    public void OpenStats()
    {
        SceneManager.LoadScene(Harmony.Scenes.MenuStatsAchievements);
    }

    public void Play()
    {
        // ouvrir loadout
        loadOut.SetActive(true);
        loadOut.GetComponent<LoadOutMenu>().OpenLoadOut();
        this.gameObject.SetActive(false);
    }

    public void Tutorial()
    {
        SceneManager.LoadScene(TUTORIAL_INDEX_SCENE);
    }
}
