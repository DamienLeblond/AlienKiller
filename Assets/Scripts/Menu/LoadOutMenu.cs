using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.TextCore.Text;
using UnityEngine.UI;

public class LoadOutMenu : MonoBehaviour
{
    
    [SerializeField] List<GameObject> playerlist = new List<GameObject>();

    [SerializeField] private Weapon firstBaseWeapon;
    [SerializeField] private Weapon secondBaseWeapon;
    [SerializeField] private AllWeapon weapons;
    [SerializeField] private GameObject mainMenu;

    public static string WEAPON_IMAGE_TAG = Harmony.Tags.WeaponHud;
    public static string WEAPON_BUTTON_TAG = Harmony.Tags.WeaponButtonTag;

    private bool buttonsActive = false;

    private GameObject panel;

    private void OnEnable()
    {
        for (int i = 0; i < playerlist.Count; i++)
        {
            GameObject character = playerlist[i];

            CharacterWeaponInventory weaponInventory = character.GetComponent<CharacterWeaponInventory>();

            weaponInventory.ReplaceWeapon(1, firstBaseWeapon);
            weaponInventory.ReplaceWeapon(2, secondBaseWeapon);


        }
    }
    public void OpenLoadOut()
    {

        for (int i = 0; i < playerlist.Count; i++)
        {
            GameObject character = playerlist[i];

            string panelName = "CharacterPanel" + (i + 1);

            GameObject panel = GameObject.Find(panelName);
            CharacterWeaponInventory weaponInventory = character.GetComponent<CharacterWeaponInventory>();

            EquipCharacterWeapons(panel, character);

        }
    }

    private void EquipCharacterWeapons(GameObject panel, GameObject character)
    {

        foreach(Transform child in panel.transform)
        {
            if (child.CompareTag(WEAPON_IMAGE_TAG))
            {
                Destroy(child.gameObject);
            }
        }

        CharacterWeaponInventory characterWeaponInventory = character.GetComponent<CharacterWeaponInventory>();

        Sprite weapon1Sprite = characterWeaponInventory.GetWeaponIcon(1);

        GameObject firstWeaponGO = new GameObject("FirstWeapon", typeof(RectTransform), typeof(Image));

       

        firstWeaponGO.tag = WEAPON_IMAGE_TAG;

        firstWeaponGO.transform.SetParent(panel.transform, false);

        // Assigner le sprite � l'image UI
        Image imageWeapon1Component = firstWeaponGO.GetComponent<Image>();
        imageWeapon1Component.sprite = weapon1Sprite;

        RectTransform weapon1RectTransform = imageWeapon1Component.GetComponent<RectTransform>();

        weapon1RectTransform.anchorMin = new Vector2(0.29f, 0.75f);
        weapon1RectTransform.anchorMax = new Vector2(0.71f, 0.99f);
        weapon1RectTransform.pivot = new Vector2(0.5f, 1f);
        weapon1RectTransform.anchoredPosition = new Vector2(0f, 0f);
        weapon1RectTransform.sizeDelta = new Vector2(150, 0);

        UnityEngine.UI.Button weaponButton = firstWeaponGO.AddComponent<UnityEngine.UI.Button>();
        weaponButton.onClick.AddListener(() => OnWeaponImageClicked(imageWeapon1Component, character, 1));


        // deuxi�me arme
        Sprite weapon2Sprite = characterWeaponInventory.GetWeaponIcon(2);

        GameObject secondWeaponGO = new GameObject("SecondWeapon", typeof(RectTransform), typeof(Image));

        secondWeaponGO.tag= WEAPON_IMAGE_TAG;

        secondWeaponGO.transform.SetParent(panel.transform, false);

        // Assigner le sprite � l'image UI
        Image imageWeapon2Component = secondWeaponGO.GetComponent<Image>();
        imageWeapon2Component.sprite = weapon2Sprite;

        RectTransform weapon2RectTransform = imageWeapon2Component.GetComponent<RectTransform>();

        weapon2RectTransform.anchorMin = new Vector2(0.29f, 0.75f);
        weapon2RectTransform.anchorMax = new Vector2(0.71f, 0.99f);
        weapon2RectTransform.pivot = new Vector2(0.5f, 1f);
        weapon2RectTransform.anchoredPosition = new Vector2(0f, -100f);
        weapon2RectTransform.sizeDelta = new Vector2(150, 0);

        UnityEngine.UI.Button secondWeaponButton = secondWeaponGO.AddComponent<UnityEngine.UI.Button>();
        secondWeaponButton.onClick.AddListener(() => OnWeaponImageClicked(imageWeapon2Component, character, 2));

    }

    public void StartGame()
    {
        for (int i = 0; i < playerlist.Count; i++)
        {
            playerlist[i].GetComponent<CharacterWeaponInventory>().SaveWeaponData(i);
        }
        PlayerPrefs.SetString("load", "newGame");
        SceneManager.LoadScene(1);
    }

    public void ReturnHomeScreen()
    {
        if(panel != null)
        {
            DeactivateButtons();
        }
        mainMenu.SetActive(true);
        this.gameObject.SetActive(false);
    }

    public void OnWeaponImageClicked(Image weaponImage, GameObject character, int weaponSlot)
    {
        buttonsActive = true;
        GameObject panelGO = new GameObject("Panel", typeof(RectTransform), typeof(UnityEngine.UI.Image));
        panelGO.GetComponent<Image>().color = Color.gray; // Couleur du panel noir
        panel = panelGO;
        RectTransform panelRectTransform = panelGO.GetComponent<RectTransform>();

        // D�finir le parent du panel sur le m�me parent que l'image d'arme
        panelRectTransform.SetParent(weaponImage.transform.parent);

        // Positionner le panel pour qu'il soit align� sous l'image d'arme
        panelRectTransform.anchorMin = Vector2.right / 2;
        panelRectTransform.anchorMax = Vector2.right / 2;
        panelRectTransform.pivot = Vector2.right / 2;
        panelRectTransform.anchoredPosition = new Vector2(0f, weaponImage.transform.position.y - 1100); // Ajustez la position verticale selon vos besoins
        panelRectTransform.sizeDelta = new Vector2(300f, weapons.GetBasicList().Count * 110f); // Ajustez la taille selon le nombre de boutons

        // Cr�er les trois boutons
        for (int i = 0; i < weapons.GetBasicList().Count; i++)
        {
            int weaponIndex = i;
            // Cr�er un nouveau GameObject pour le bouton
            GameObject buttonGO = new GameObject("WeaponButton" + i, typeof(RectTransform), typeof(UnityEngine.UI.Button), typeof(Image));

            buttonGO.tag = WEAPON_IMAGE_TAG;

            // R�cup�rer le RectTransform du bouton
            RectTransform buttonRectTransform = buttonGO.GetComponent<RectTransform>();

            // D�finir le parent du bouton sur le m�me parent que l'image d'arme
            buttonRectTransform.SetParent(panelRectTransform);

            // Positionner le bouton sous l'image d'arme
            buttonRectTransform.anchorMin = Vector2.right / 2;
            buttonRectTransform.anchorMax = Vector2.right / 2;
            buttonRectTransform.pivot = Vector2.right / 2;
            buttonRectTransform.anchoredPosition = new Vector2(0f, weaponImage.transform.position.y -715  - (i * 100f)); // Espacement vertical entre les boutons

            // Taille du bouton
            buttonRectTransform.sizeDelta = new Vector2(300f, 81f);

            // Ajouter un bouton UI au GameObject
            UnityEngine.UI.Button buttonComponent = buttonGO.GetComponent<Button>();

            // Ajouter un �couteur d'�v�nement pour d�tecter le clic sur le bouton
            buttonComponent.onClick.AddListener(() => OnWeaponSelected(character, weaponSlot, weapons.GetBasicList()[weaponIndex]));

            // R�cup�rer l'arme correspondante depuis la liste des armes
            Weapon weapon = weapons.GetBasicList()[i];

            // R�cup�rer l'image de l'arme pour le bouton
            Image weaponButtonImage = buttonGO.GetComponent<Image>();
            weaponButtonImage.sprite = weapon.sprite;

            // Ajouter une couleur d'arri�re-plan au bouton
            weaponButtonImage.color = Color.white; // Vous pouvez ajuster la couleur selon vos besoins
        }
    }

    // M�thode appel�e lorsqu'une arme est s�lectionn�e dans le dropdown
    public void OnWeaponSelected(GameObject character, int weaponSlot, Weapon weapon)
    {

        // Obtenez l'arme s�lectionn�e dans la liste des armes

        character.GetComponent<CharacterWeaponInventory>().ReplaceWeapon(weaponSlot, weapon);
        // Mettez � jour l'image d'arme avec l'arme s�lectionn�e
        OpenLoadOut();
        DeactivateButtons();

    }

    private void Update()
    {
        // V�rifie si les boutons sont actifs et s'ils ont �t� d�sactiv�s
        if (buttonsActive && Input.GetMouseButtonDown(0) && !IsPointerOverUIObject())
        {
            DeactivateButtons();
        }
    }

    private bool IsPointerOverUIObject()
    {
        // V�rifie si le pointeur de la souris est sur un objet d'UI
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        GraphicRaycaster uiRaycaster = GetComponent<GraphicRaycaster>();
        var results = new List<RaycastResult>();
        uiRaycaster.Raycast(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    private void DeactivateButtons()
    {
        Destroy(panel);
        buttonsActive = false;
    }
}

