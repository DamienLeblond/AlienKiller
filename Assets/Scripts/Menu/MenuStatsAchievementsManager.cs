using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuStatsAchievementsManager : MonoBehaviour
{
    private GlobalStats globalStats;

    private void Start()
    {
        // Mettez � jour les �l�ments de l'interface utilisateur avec les donn�es actuelles
        globalStats = new GlobalStats();

        UpdateStatsUI();
    }

    // Mettre � jour les �l�ments de l'interface utilisateur pour afficher les statistiques globales
    private void UpdateStatsUI()
    {
        for (int i = 0; i < 4; i++)
        {
            string panelName = "CharacterPanel" + (i + 1);
            GameObject panel = GameObject.Find(panelName);

            // V�rifier si le panneau du personnage existe
            if (panel != null)
            {
                // Charger les statistiques globales du personnage
                GlobalStatsData characterStats = globalStats.LoadGlobalStats(i);

                // V�rifier si les statistiques globales ont �t� charg�es avec succ�s
                if (characterStats != null)
                {
                    // Mettre � jour les textes des statistiques dans le panneau
                    Text characterStatsText = panel.transform.Find("CharacterStatsText").GetComponent<Text>();
                    characterStatsText.text = "Character " + (i + 1) + " Stats:\n" +
                        "Shots Fired: " + characterStats.shotsFired + "\n" +
                        "Bullets Hit: " + characterStats.bulletsHit + "\n" +
                        "Bullets Missed: " + characterStats.bulletsMissed + "\n" +
                        "Crit: " + characterStats.crit + "\n" +
                        "Enemies Defeated: " + characterStats.enemyDefeated + "\n" +
                        "HP Recovered: " + characterStats.hpRecovered + "\n" +
                        "Total Damage Dealt: " + characterStats.totalDamageDealt + "\n" +
                        "Highest Damage Dealt: " + characterStats.highestDamageDealt + "\n" +
                        "Total Damage Received: " + characterStats.totalDamageReceived;
                }
                else
                {
                    Debug.LogWarning("Global stats data not found for Character " + (i + 1));
                }
            }
            else
            {
                Debug.LogWarning("Character panel not found: " + panelName);
            }
        }
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene(Harmony.Scenes.MainMenu);
    }
}
