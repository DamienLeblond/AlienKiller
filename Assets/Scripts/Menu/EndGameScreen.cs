using Harmony;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TextCore.Text;
using UnityEngine.UI;
using static UnityEngine.Rendering.DebugUI;

public class EndGameScreen : MonoBehaviour
{
    [SerializeField] List<GameObject> playerList = new List<GameObject>();

    private Vector2 itemSpacing = new Vector2(24f, 0f); // Espace entre chaque objet dans le canvas
    private Vector2 itemSize = new Vector2(60f, 60f); // Taille de chaque objet dans le canvas
    private int maxItemsPerRow = 12; // Nombre maximal d'objets par ligne
    [SerializeField] Font font;
    private GameObject characterPanel;
    public static string OBJECT_IMAGE_TAG = Harmony.Tags.ObjectImage;
    private SaveProgression progression;
    private TextMeshProUGUI nbTir, precision, nbCrit, nbKill, nbPv, damageDealt, highestDamage, damageReceived;
    private TextMeshProUGUI charNumber, charNbTir, charPrecision, charNbCrit, charKill, charPv, charDamageDealt, charHighestDamage, charDamageReceived;

    
        

    private void OnEnable()
    {
        progression = new SaveProgression();
        GameManager gameManager = GameObject.FindAnyObjectByType<GameManager>();
        playerList = gameManager.GetPlayers();
        characterPanel = GameObject.Find("CharacterPanel");

        FindAllTextComponents();
    }

    private void FindAllTextComponents()
    {
        //stats
        nbTir = GameObject.Find("NbTir").GetComponent<TextMeshProUGUI>();
        precision = GameObject.Find("Pr�cision").GetComponent<TextMeshProUGUI>();
        nbCrit = GameObject.Find("NbCrit").GetComponent<TextMeshProUGUI>();
        nbKill = GameObject.Find("NbKill").GetComponent<TextMeshProUGUI>();
        nbPv = GameObject.Find("NbPv").GetComponent<TextMeshProUGUI>();
        damageDealt = GameObject.Find("DamageDealt").GetComponent<TextMeshProUGUI>();
        highestDamage = GameObject.Find("HigestDamage").GetComponent<TextMeshProUGUI>();
        damageReceived = GameObject.Find("DomageReceived").GetComponent<TextMeshProUGUI>();

        //charStats
        charNumber = GameObject.Find("Character_Number").GetComponent<TextMeshProUGUI>();
        charNbTir = GameObject.Find("Character_NbTir").GetComponent<TextMeshProUGUI>();
        charPrecision = GameObject.Find("Character_Pr�cision").GetComponent<TextMeshProUGUI>();      
        charNbCrit = GameObject.Find("Character_NbCrit").GetComponent<TextMeshProUGUI>();
        charKill = GameObject.Find("Character_NbKill").GetComponent<TextMeshProUGUI>();
        charPv = GameObject.Find("Character_NbPv").GetComponent<TextMeshProUGUI>();
        charDamageDealt = GameObject.Find("Character_DamageDealt").GetComponent<TextMeshProUGUI>();
        charHighestDamage = GameObject.Find("Character_HigestDamage").GetComponent<TextMeshProUGUI>();
        charDamageReceived = GameObject.Find("Character_DomageReceived").GetComponent<TextMeshProUGUI>();
    }

    public void OpenEndMenu(bool victoire)
    {
        if(victoire)
        {
            GameObject.Find("EndText").GetComponent<TextMeshProUGUI>().text = "Victoire";
            if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.VICTORY))
            {
                AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.VICTORY);
            }
            if (!AchievementSaveManager.Instance.IsAchievementUnlocked(AchievementSaveManager.NO_LIFE_LOST))
            {
                if (!AchievementSaveManager.lostLife)
                {
                    AchievementSaveManager.Instance.UnlockAchievement(AchievementSaveManager.NO_LIFE_LOST);
                }
            }
        }
        else
        {
            GameObject.Find("EndText").GetComponent<TextMeshProUGUI>().text = "d�faite";
        }
        progression.DeleteProgressionFile();
        SetStatsText();
        CreateCharacterPortrait();
        OnPortraitClicked(playerList[0]);
    }

    private void SetStatsText()
    {
        int totalShotsFired = 0;
        int totalShotsHit = 0;
        int totalCriticalHits = 0;
        int totalEnemiesKilled = 0;
        int totalDamageDealt = 0;
        int maxHighestDamage = 0;
        int totalDamageTaken = 0;
        int healthRegain = 0;

        foreach (GameObject player in playerList)
        {
            // Obtenir les donn�es de statistiques globales du joueur
            GlobalStatsData stat = player.GetComponent<CharacterStat>().globalStatsData;

            // Mettre � jour les totaux avec les statistiques du joueur actuel
            totalShotsFired += stat.shotsFired;
            totalShotsHit += stat.bulletsHit;
            totalCriticalHits += stat.crit;
            totalEnemiesKilled += stat.enemyDefeated;
            totalDamageDealt += stat.totalDamageDealt;
            totalDamageTaken += stat.totalDamageReceived;
            healthRegain += stat.hpRecovered;

            // Mettre � jour le dommage le plus �lev� si n�cessaire
            if (stat.highestDamageDealt > maxHighestDamage)
            {
                maxHighestDamage = stat.highestDamageDealt;
            }

        }

        // Calculer la pr�cision
        float accuracy = totalShotsHit / (float)totalShotsFired * 100f;


        nbTir.text += totalShotsFired.ToString();
        precision.text += accuracy.ToString();
        nbCrit.text += totalCriticalHits.ToString();
        nbKill.text += totalEnemiesKilled.ToString();
        nbPv.text += healthRegain.ToString();
        damageDealt.text += totalDamageDealt.ToString();
        highestDamage.text += maxHighestDamage.ToString();
        damageReceived.text += totalDamageTaken.ToString();
    
    }

    private void SetCharacterStatsText(GameObject character)
    {


        GlobalStatsData stat = character.GetComponent<CharacterStat>().globalStatsData;

        // Mettre � jour les totaux avec les statistiques du joueur actuel
        int totalShotsFired = stat.shotsFired;
        int totalShotsHit = stat.bulletsHit;
        int totalCriticalHits = stat.crit;
        int totalEnemiesKilled = stat.enemyDefeated;
        int totalDamageDealt = stat.totalDamageDealt;
        int totalDamageTaken = stat.totalDamageReceived;
        int healthRegain = stat.hpRecovered;

        int maxHighestDamage = stat.highestDamageDealt;


        // Calculer la pr�cision
        float accuracy = totalShotsHit / (float)totalShotsFired * 100f;

        charNumber.text = "Player : " + (playerList.FindIndex(ch => ch == character) + 1);
        charNbTir.text = "NB tir: " + totalShotsFired.ToString();
        charPrecision.text = "Pr�cision: " + accuracy.ToString();
        charNbCrit.text = "NB crit: " + totalCriticalHits.ToString();
        charKill.text = "ennemis tuer: " + totalEnemiesKilled.ToString();
        charPv.text = "point de vie r�cup�rer: " + healthRegain.ToString();
        charDamageDealt.text = "Domage inflig�: " + totalDamageDealt.ToString();
        charHighestDamage.text = "Domage inflig� le plus haut : " + maxHighestDamage.ToString();
        charDamageReceived.text = "Domage re�u: " + totalDamageTaken.ToString();

    }


    public void CreateCharacterPortrait()
    {
        float portraitSpacing = 20f; // Espacement horizontal entre les portraits
        float portraitWidth = 150f; // Largeur du portrait

        // Boucler � travers la liste des personnages
        for (int i = 0; i < playerList.Count; i++)
        {
            GameObject character = playerList[i];

            // Cr�er un nouveau GameObject pour le portrait du personnage
            GameObject portraitGO = new("CharacterPortrait", typeof(RectTransform), typeof(Image));
            portraitGO.transform.SetParent(characterPanel.transform, false);

            // R�cup�rer le sprite du personnage
            Sprite characterSprite = character.GetComponent<Image>().sprite;

            // Obtenir le composant Image du portrait et assigner le sprite
            Image imageComponentPortrait = portraitGO.GetComponent<Image>();
            imageComponentPortrait.sprite = characterSprite;

            // Obtenir le composant RectTransform du portrait
            RectTransform imageRectTransform = imageComponentPortrait.GetComponent<RectTransform>();

            // Calculer la position horizontale du portrait en fonction de l'index de la boucle
            float portraitX = (portraitWidth + portraitSpacing) * i;

            // Ajuster la position et la taille du portrait
            imageRectTransform.anchorMin = new Vector2(0.25f, 1f);
            imageRectTransform.anchorMax = new Vector2(0.25f, 1f);
            imageRectTransform.pivot = new Vector2(0.5f, 1f);
            imageRectTransform.anchoredPosition = new Vector2(portraitX, 200f); // Ajuster la position en fonction de l'index
            imageRectTransform.sizeDelta = new Vector2(portraitWidth, 200f);


            UnityEngine.UI.Button button = portraitGO.AddComponent<UnityEngine.UI.Button>();

            int index = i; // Capturer la valeur de l'index dans une variable locale pour �viter les probl�mes de cl�ture
            button.onClick.AddListener(() => OnPortraitClicked(playerList[index]));
        }
    }

    private void OnPortraitClicked(GameObject character)
    {
        DisplayCharacterObjects(character);
        SetCharacterStatsText(character);
    }

    public void DisplayCharacterObjects(GameObject character)
    {
        CharacterObjectInventory characterObjectInventory = character.GetComponent<CharacterObjectInventory>();
        List<TrinketObject> objectList = characterObjectInventory.GetObjects();
        Dictionary<TrinketObject, int> itemCounts = new();

        foreach (Transform child in characterPanel.transform)
        {
            if (child.CompareTag(OBJECT_IMAGE_TAG))
            {
                Destroy(child.gameObject);
            }
        }

        int itemIndex = 0;
        int rowIndex = 0;
        int colIndex = 0;
        float startX = -540f; // Gardons la m�me valeur de d�part pour startX
        float startY = 320f;

        // Parcourez la liste des objets et cr�ez des images pour chaque objet
        foreach (TrinketObject obj in objectList)
        {
            if (!itemCounts.ContainsKey(obj))
            {
                itemCounts[obj] = 0;
            }

            // Incr�mentez le compteur d'occurrences de l'objet
            itemCounts[obj]++;

            // Cr�ez l'image de l'objet si c'est sa premi�re occurrence dans l'inventaire
            if (itemCounts[obj] == 1)
            {
                GameObject itemGameObject = new GameObject(obj.name);
                itemGameObject.tag = OBJECT_IMAGE_TAG;

                itemGameObject.AddComponent<RectTransform>();
                itemGameObject.AddComponent<Image>();

                // Configurez la position de l'image dans le canvas
                RectTransform itemRectTransform = itemGameObject.GetComponent<RectTransform>();
                itemRectTransform.sizeDelta = itemSize;
                // Calcul de la position
                float xPos = startX + (colIndex * (itemSize.x + itemSpacing.x));
                float yPos = startY - (rowIndex * (itemSize.y + itemSpacing.y));

                itemRectTransform.anchoredPosition = new Vector2(xPos, yPos);

                // Configurez le sprite de l'image
                Image imageComponent = itemGameObject.GetComponent<Image>();
                imageComponent.sprite = obj.sprite;

                // V�rifiez s'il y a plusieurs occurrences du m�me objet dans l'inventaire
                int itemCount = CountItems(objectList, obj);
                if (itemCount > 1)
                {
                    // Cr�ez un texte pour afficher le nombre d'occurrences
                    GameObject itemCountTextObject = new GameObject("ItemCountText", typeof(RectTransform));
                    RectTransform textRectTransform = itemCountTextObject.GetComponent<RectTransform>();
                    textRectTransform.SetParent(itemRectTransform);
                    textRectTransform.anchorMin = new Vector2(1f, 0f);
                    textRectTransform.anchorMax = new Vector2(1f, 0f);
                    textRectTransform.pivot = new Vector2(1f, 0f);
                    textRectTransform.anchoredPosition = new Vector2(2f, 0f);
                    Text itemCountText = itemCountTextObject.AddComponent<Text>();
                    itemCountText.text = "x" + itemCount.ToString();
                    itemCountText.alignment = TextAnchor.LowerRight;
                    itemCountText.color = Color.white;
                    itemCountText.font = font;
                }
                itemRectTransform.SetParent(characterPanel.transform, false);

                // Mettez � jour les indices de ligne et de colonne
                itemIndex++;
                colIndex++;
                if (colIndex >= maxItemsPerRow)
                {
                    colIndex = 0;
                    rowIndex++;
                }
                if (rowIndex == 5)
                {
                    break;
                }
            }
        }
    }

    public int CountItems(List<TrinketObject> objects, TrinketObject targetObject)
    {
        int count = 0;
        foreach (TrinketObject obj in objects)
        {
            if (obj == targetObject)
            {
                count++;
            }
        }
        return count;
    }


    public void ReturnToMenu()
    {
        SceneManager.LoadScene(Harmony.Scenes.MainMenu);
    }
}
