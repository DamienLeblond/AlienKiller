using UnityEngine;

public class LevelInformation : MonoBehaviour
{
    [SerializeField] private int level;
    public float CommonObjectPercentChance { get; private set; }
    public float UncommonObjectPercentChance { get; private set; }
    public float RareObjectPercentChance { get; private set; }
    public float LegendaryObjectPercentChance { get; private set; }
    public float NormalRobotPercent { get; private set; }
    public float SniperRobotPercent { get; private set; }

    public void InitializeValues()
    {
        switch (level)
        {
            case 1:
                CommonObjectPercentChance = 75f;
                UncommonObjectPercentChance = 20f;
                RareObjectPercentChance = 4.5f;
                LegendaryObjectPercentChance = 0.5f;
                NormalRobotPercent = 100f;
                SniperRobotPercent = 0f; 
                break;
            case 2:
                CommonObjectPercentChance = 49f;
                UncommonObjectPercentChance = 35f;
                RareObjectPercentChance = 14f;
                LegendaryObjectPercentChance = 2f;
                NormalRobotPercent = 60f;
                SniperRobotPercent = 40f;
                break;
            default:
                break;
        }
    }
}
