using DungeonArchitect;
using DungeonArchitect.Builders.GridFlow;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GenerateLevel : MonoBehaviour
{
    private int seed;
    private Dungeon level;
    private WorkingSeedsList workingSeedsList;
    private ChangeWallColor wallColor;

    
    void Start()
    {
        wallColor = GetComponent<ChangeWallColor>();
        level = GetComponent<Dungeon>();
        workingSeedsList = GetComponent<WorkingSeedsList>();

        if (PlayerPrefs.GetString("load") == "loadGame")
        {
            seed = PlayerPrefs.GetInt("loadedGameSeed");
            level.SetSeed(seed);
        }
        else
        {
            level.RandomizeSeed();
            seed = (int)level.Config.Seed;
        }
        try
        {
            level.Build();
            wallColor.ChangeColor();
        }
        catch
        {
            seed = workingSeedsList.GetRandomWorkingSeed(); 
            level.SetSeed(seed);
            level.Build();
            wallColor.ChangeColor();
        }
    }
}
