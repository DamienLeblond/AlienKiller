using UnityEngine;
using System.Collections;
using DungeonArchitect;
using DungeonArchitect.Utils;
using System;


public class OneTileObjectRule : TransformationRule
{
        public float plageDeplacement = 1f;

    public override void GetTransform(PropSocket socket, DungeonModel model, Matrix4x4 propTransform, System.Random random, out Vector3 outPosition, out Quaternion outRotation, out Vector3 outScale)
    {
        base.GetTransform(socket, model, propTransform, random, out outPosition, out outRotation, out outScale);

        // Déplacement aléatoire
        float deplacementX = (UnityEngine.Random.Range(0, 2) == 0) ? plageDeplacement : -plageDeplacement;
        float deplacementZ = (UnityEngine.Random.Range(0, 2) == 0) ? plageDeplacement : -plageDeplacement;

        outPosition.x = deplacementX;
        outPosition.z = deplacementZ;
    }
}