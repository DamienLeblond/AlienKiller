using UnityEngine;
using System.Collections;
using DungeonArchitect;
using DungeonArchitect.Utils;
using System;

public class BarrierPlacementRule : TransformationRule
{
    public float plageDeplacement = 1.9f;
    public float distanceThreshold = 4f;

    public override void GetTransform(PropSocket socket, DungeonModel model, Matrix4x4 propTransform, System.Random random, out Vector3 outPosition, out Quaternion outRotation, out Vector3 outScale)
    {
        base.GetTransform(socket, model, propTransform, random, out outPosition, out outRotation, out outScale);

        // Rotation al�atoire en Y (0, 90, 180 ou -90 degr�s)
        int randomRotation = random.Next(4) * 90;
        outRotation = Quaternion.Euler(0, randomRotation, 0);

        // D�placement en fonction de la rotation
        float deplacementX = 0f;
        float deplacementZ = 0f;

        if (randomRotation == 90 || randomRotation == -90)
        {
            // Si la rotation est de 90 ou -90 degr�s, ajuster le d�placement en X
            deplacementX = 2f * (random.Next(2) * 2 - 1); // -2, 2
        }
        else if (randomRotation == 0 || randomRotation == 180)
        {
            // Si la rotation est de 0 ou 180 degr�s, ajuster le d�placement en Z
            deplacementZ = 2f * (random.Next(2) * 2 - 1); // -2, 2
        }

        // Appliquer le d�placement
        outPosition.x = deplacementX;
        outPosition.z = deplacementZ;
    }
}
