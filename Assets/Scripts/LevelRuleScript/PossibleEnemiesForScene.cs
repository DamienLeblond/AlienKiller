using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PossibleEnemiesForScene : MonoBehaviour
{
    [SerializeField] private GameObject normalEnemy;
    [SerializeField] private GameObject sniperEnemy;
    private LevelInformation levelInformation;
    private int remainingNbNormalEnemies;
    private int remainingNbSniperEnemies;

    private void Awake()
    {
        levelInformation = FindAnyObjectByType<LevelInformation>();
        levelInformation.InitializeValues();
    }

    public void SetNbEnemiesInScene(int nbEnemies)
    {
        remainingNbNormalEnemies = Mathf.RoundToInt(levelInformation.NormalRobotPercent / 100f * nbEnemies);
        remainingNbSniperEnemies = Mathf.RoundToInt(levelInformation.SniperRobotPercent / 100f * nbEnemies);

        // Au cas o� si l'addition du nombre d'ennemi normal et celui d'ennemi sniper n'�quivaut pas � le nombre total d'ennemi attendu
        while ((remainingNbNormalEnemies + remainingNbSniperEnemies) < nbEnemies)
        {
            if (levelInformation.NormalRobotPercent > levelInformation.SniperRobotPercent)
            {
                remainingNbNormalEnemies++;
            }
            else
            {
                remainingNbSniperEnemies++;
            }
        }
    }

    public GameObject GetNextEnemy()
    {
        int totalEnemies = remainingNbNormalEnemies + remainingNbSniperEnemies;

        if (totalEnemies > 0)
        {
            float randomValue = Random.value;

            if (randomValue < (levelInformation.NormalRobotPercent / 100f))
            {
                remainingNbNormalEnemies--;
                return Instantiate(normalEnemy);
            }
            else if (randomValue < (levelInformation.NormalRobotPercent / 100f) + (levelInformation.SniperRobotPercent / 100f))
            {
                remainingNbSniperEnemies--;
                return Instantiate(sniperEnemy);
            }
        }

        return Instantiate(normalEnemy);
    }


}
