using DungeonArchitect;
using UnityEngine;

public class AltPathRule : ScriptableObject, ISGFLayoutNodeCategoryConstraint
{
    public string[] GetModuleCategoriesAtNode(int currentPathPosition, int pathLength)
    {
   
        // Use a large room for every alternate node
        if (currentPathPosition % 2 == 0)
        {
            return new string[] { "Corridor" };
        }
        else
        {
            return new string[] { "Room" };
        }
    }
}
