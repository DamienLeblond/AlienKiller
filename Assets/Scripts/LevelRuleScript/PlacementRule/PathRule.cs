using DungeonArchitect;
using UnityEngine;

public class PathRule : ScriptableObject, ISGFLayoutNodeCategoryConstraint
{
    public string[] GetModuleCategoriesAtNode(int currentPathPosition, int pathLength)
{
    // Place a boss room in the last node
    if (currentPathPosition == pathLength - 1)
    {
        return new string[] { "Exit" };
    }

    if (currentPathPosition == 0)
    {
        return new string[] { "Start" };
    }

        // Use a shop or health room before the boss room
    if (currentPathPosition == pathLength - 2)
    {
        return new string[] { "Boss" };
    }

    if (currentPathPosition == pathLength - 3)
    {
        return new string[] { "Treasure" };
    }

    // Use a large room for every alternate node
    if (currentPathPosition % 2 == 0)
    {
        return new string[] { "Corridor" };
    }

    // use a normal room
    return new string[] { "Room" };
}
}
