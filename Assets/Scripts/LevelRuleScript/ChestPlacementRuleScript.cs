using UnityEngine;
using System.Collections;
using DungeonArchitect;
using DungeonArchitect.Utils;
using System;

public class RandomRotYTransformRule : TransformationRule
{
    public float plageDeplacement = 1.9f;
    public float distanceThreshold = 4f;

    public override void GetTransform(PropSocket socket, DungeonModel model, Matrix4x4 propTransform, System.Random random, out Vector3 outPosition, out Quaternion outRotation, out Vector3 outScale)
    {
        base.GetTransform(socket, model, propTransform, random, out outPosition, out outRotation, out outScale);

        // D�placement al�atoire
        float deplacementX = (UnityEngine.Random.Range(0, 2) == 0) ? plageDeplacement : -plageDeplacement;
        float deplacementZ = (UnityEngine.Random.Range(0, 2) == 0) ? plageDeplacement : -plageDeplacement;

        outPosition.x = deplacementX;
        outPosition.z = deplacementZ;


        //Mettre le coffre en diagonal
        Collider[] colliders = Physics.OverlapSphere(outPosition, distanceThreshold);

        foreach (var collider in colliders)
        {
            // V�rifier si l'objet a le nom "decorative_wall_3"
            if (collider.gameObject.name == "decorative_wall_3")
            {
                // Si l'objet est trouv�, ajuster l'orientation en Y seulement
                Vector3 directionToWall = collider.transform.position - outPosition;
                float angle = Mathf.Atan2(directionToWall.x, directionToWall.z) * Mathf.Rad2Deg;
                outRotation = Quaternion.Euler(0, angle, 0);
                return;
            }
        }
    }
}
