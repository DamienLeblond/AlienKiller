using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ChangeWallColor : MonoBehaviour
{
    [SerializeField] private Material newMaterial;

    void Start()
    {
        ChangeColor();
    }

    public void ChangeColor()
    {
        GameObject dungeonItem = GameObject.Find("DungeonItems");
        if (dungeonItem != null && newMaterial != null)
        {
            List<MeshFilter> detectWalls = dungeonItem.GetComponentsInChildren<MeshFilter>()
                .Where(x => x.gameObject.name == "P_Wall_01")
                .ToList();

            foreach (MeshFilter wall in detectWalls)
            {
                Renderer wallRenderer = wall.GetComponent<Renderer>();
                if (wallRenderer != null)
                {
                    wallRenderer.sharedMaterial = newMaterial;
                }
                else
                {
                    Debug.LogError("Composant Renderer non trouv� sur le mur: " + wall.gameObject.name);
                }
            }

            ForceUIRefresh();
        }
    }

    private void ForceUIRefresh()
    {
        if (RenderSettings.skybox != null)
        {
            RenderSettings.skybox.SetFloat("_RandomSeed", Random.value);
        }
    }
}
