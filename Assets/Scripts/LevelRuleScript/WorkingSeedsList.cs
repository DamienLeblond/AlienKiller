using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkingSeedsList : MonoBehaviour
{
    //list de seeds test�s � la main qui fonctionnent, qui servent pour si les seeds al�atoires plantent.
    private static readonly List<int> seeds = new() { 827, 1923798272, 985, 1086, 1257, 1827, 2265, 2416, 2564, 3225, 4805, 5548, 6428, 7315, 8723 };

    public int GetRandomWorkingSeed()
    {
        return seeds[Random.Range(0, seeds.Count)];
    }
}
