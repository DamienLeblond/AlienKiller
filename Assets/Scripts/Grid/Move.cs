using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public bool canEnemyMove = false;
    private const int COST_TO_MOVE = 1;
    private int maxTiles = 8;
    private bool isMoving = false;
    private List<PathNode> currentPath;
    private int currentPathIndex;
    private Vector3 currentNode;
    private float speed = 5;
    private Vector3 positionToReach;
    private ActionPoints actionPoints;
    private CharacterStat characterStat;
    private AudioSource audioSource;
    private GridObject gridObject;
    public bool canMove = true;
    private IMovingAnimator animatorManager;

    [SerializeField] private AudioClip walkClip;


    public Vector3 GetPositionToReach()
    {
        return positionToReach;
    }

    public void SetPositionToReach(Vector3 position)
    {
        positionToReach = position;
    }

    private void Start()
    {
        gridObject = GetComponent<GridObject>();
        audioSource = GetComponent<AudioSource>();
        characterStat = GetComponent<CharacterStat>();
        actionPoints = FindAnyObjectByType<ActionPoints>();
        animatorManager = GetComponent<IMovingAnimator>();
        if (characterStat != null) maxTiles = characterStat.Movement;

    }

    void Update()
    {
        if (canMove && isMoving)
        {
            Vector3 directionToTheCurrentNodeFromCharacter = (currentNode - transform.position).normalized;
            transform.SetPositionAndRotation(Vector3.MoveTowards(transform.position, currentNode, speed * Time.deltaTime), Quaternion.LookRotation(directionToTheCurrentNodeFromCharacter));

            if (positionToReach == transform.position)
            {
                InitializeValuesAfterFinalDestination();
            }
            else if (transform.position == currentNode)
            {
                SetNextCurrentNode();
            }
        }

    }

    private void InitializeValuesAfterFinalDestination()
    {
        gridObject.Init();
        isMoving = false;
        canMove = false;
        animatorManager.StopMoving();
        if (!canEnemyMove) actionPoints.UseActionPoints(COST_TO_MOVE);
        canEnemyMove = false;
        audioSource.Stop();
    }

    private void SetNextCurrentNode()
    {
        if (!audioSource.isPlaying) audioSource.Play();
        currentPathIndex++;
        currentNode = ConvertGridPositionToWorldPosition(currentPath[currentPathIndex].pos_x, currentPath[currentPathIndex].pos_y);
    }


    public bool IsMoving()
    {
        return isMoving;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }


    public void SetPlayerPosition(Vector2Int position, List<PathNode> path)
    {
        if (path.Count <= characterStat.Movement && path.Count > 0 && !isMoving)
        {
            SetAudioWalk(1, 1);
            SetPosition(position.x, position.y, path);
        }
    }

    public void SetEnemyPosition(Vector2Int position, List<PathNode> path, EnemyInfo enemy)
    {
        int mouvementSpeed = enemy.GetMoveDistance();

        if (path.Count <= mouvementSpeed && path.Count > 0 && !isMoving)
        {
            SetAudioWalk(0.4f, 1.5f);
            SetPosition(position.x, position.y, path);
        }
    }
    private void SetPosition(int x, int y, List<PathNode> path)
    {
        positionToReach = ConvertGridPositionToWorldPosition(x, y);
        isMoving = true;
        animatorManager.StartMoving();
        currentPath = path;
        currentPathIndex = 0;
        currentNode = ConvertGridPositionToWorldPosition(path[currentPathIndex].pos_x, path[currentPathIndex].pos_y);
    }

    private void SetAudioWalk(float volume, float pitch)
    {
        audioSource.clip = walkClip;
        audioSource.pitch = pitch;
        audioSource.volume = volume;
        audioSource.Play();
    }
    private Vector3 ConvertGridPositionToWorldPosition(int x, int y)
    {
        return new Vector3(x * 2, 0, y * 2);
    }

    public int GetMaxTileCount() { return maxTiles;}

}
