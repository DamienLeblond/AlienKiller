using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class PathNode
{
    public int pos_x;
    public int pos_y;

    public float gValue;
    public float hValue;
    public PathNode parentNode;

    public float fValue
    {
        get { return gValue + hValue; }
    }

    public PathNode(int xPos, int yPos)
    {
        pos_x = xPos;
        pos_y = yPos;
    }
}

[RequireComponent(typeof(CustomGrid))]
public class PathFinding : MonoBehaviour
{
    private const int G_COST = 14;
    private const int H_COST = 10;
    CustomGrid gridMap;

    PathNode[,] pathNodes;

    private void Awake()
    {
        if (gridMap == null) { gridMap = GetComponent<CustomGrid>(); }
    }
    private void Start()
    {
        Init();
    }

    private void Init()
    {
        
        pathNodes = new PathNode[gridMap.lenght, gridMap.width];

        for (int x = 0; x < gridMap.lenght; x++)
        {
            for (int y = 0; y < gridMap.width; y++)
            {
                pathNodes[x, y] = new PathNode(x, y);
            }
        }
    }

    public GridObject GetGameObjectByTagNearSelectedPosition(Vector2Int position, String tag)
    {
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                Vector2Int currentNodePosition = new Vector2Int(position.x + x, position.y + y);
                if (currentNodePosition.x > 0 && currentNodePosition.y > 0)
                {
                    GridObject gridObject = gridMap.GetPlacedObject(currentNodePosition);
                    if (gridObject != null && gridObject.gameObject.CompareTag(tag)) return gridObject;
                }
            }
        }

        return null; 
    }

    public List<PathNode> FindPath(int startX, int startY, int endX, int endY)
    {
        Vector2Int endPosition = gridMap.FindValidPosition(new Vector2Int(endX, endY));
        if (endX < 0 || endY < 0) return null;
        PathNode startNode = pathNodes[startX, startY];
        PathNode endNode = pathNodes[endPosition.x, endPosition.y];

        List<PathNode> openList = new List<PathNode>();
        List<PathNode> closedList = new List<PathNode>();
        openList.Add(startNode);

        List<PathNode> lastValidPath = null;

        while (openList.Count > 0)
        {
            PathNode currentNode = SetCurrentNode(openList);
            openList.Remove(currentNode);
            closedList.Add(currentNode);

            if (currentNode == endNode) return RetracePath(startNode, endNode);

            List<PathNode> neighborNodes = GetNodesNearTheCurrentNode(currentNode);
            for (int i = 0; i < neighborNodes.Count; i++)
            {
                if (IsNeighborNodeTaken(neighborNodes[i], closedList)) continue;
                if (CanAddNeighborNodeToOpenList(neighborNodes[i], currentNode, openList, endNode)) openList.Add(neighborNodes[i]);
            }

            if (currentNode != endNode && openList.Count > 0)
            {
                lastValidPath = RetracePath(startNode, currentNode);
            }
        }

      return lastValidPath;
    }


    private bool CanAddNeighborNodeToOpenList(PathNode neighborNode, PathNode currentNode, List<PathNode> openList, PathNode endNode)
    {
        float movementCost = currentNode.gValue + CalculateDistance(currentNode, neighborNode);

        if (openList.Contains(neighborNode) == false
            || movementCost < neighborNode.gValue)
        {
            neighborNode.gValue = movementCost;
            neighborNode.hValue = CalculateDistance(neighborNode, endNode);
            neighborNode.parentNode = currentNode;

            if (openList.Contains(neighborNode) == false)
            {
                return true;
            }
        }

        return false;
    }

    private bool IsNeighborNodeTaken(PathNode neighborNode, List<PathNode> closedList)
    {
        if (closedList.Contains(neighborNode) || !gridMap.NodeIsAvailable(neighborNode.pos_x, neighborNode.pos_y)) { return true; }
        return false;
    }
    private PathNode SetCurrentNode(List<PathNode> openList)
    {
        PathNode currentNode = openList[0];
        for (int i = 0; i < openList.Count; i++)
        {
            if (currentNode.fValue > openList[i].fValue)
            {
                currentNode = openList[i];
            }

            if (currentNode.fValue == openList[i].fValue
                && currentNode.hValue > openList[i].hValue)
            {
                currentNode = openList[i];
            }
        }

        return currentNode;
    }

    private List<PathNode> GetNodesNearTheCurrentNode(PathNode currentNode)
    {
        List<PathNode> neighborNodes = new List<PathNode>();
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                if (x == 0 && y == 0) { continue; } //check si la case s�lectionn� n'est pas celle qu'on se trouve dessus
                if (gridMap.CheckBoundary(currentNode.pos_x + x, currentNode.pos_y + y) == false) { continue; } //check si les cases sont dans l'int�rieur de la map.
                if (x != 0 && y !=0) { continue; } //check si les cases sont en diagonales.
                if (x == 0 && y == 1 && gridMap.NodeHasWallNorth(currentNode.pos_x,currentNode.pos_y)) { continue; } //check si on peut se d�placer vers le haut s'il n'y a pas de murs
                if (x == 0 && y == -1 && gridMap.NodeHasWallSouth(currentNode.pos_x,currentNode.pos_y)) { continue; } //check si on peut se d�placer vers le haut s'il n'y a pas de murs
                if (x == 1 && y == 0 && gridMap.NodeHasWallEast(currentNode.pos_x,currentNode.pos_y)) { continue; } //check si on peut se d�placer vers le haut s'il n'y a pas de murs
                if (x == -1 && y == 0 && gridMap.NodeHasWallWest(currentNode.pos_x,currentNode.pos_y)) { continue; } //check si on peut se d�placer vers le haut s'il n'y a pas de murs
                neighborNodes.Add(pathNodes[currentNode.pos_x + x, currentNode.pos_y + y]);
            }
        }

        return neighborNodes;
    }


    public List<GameObject> GetGameObjectsNearSelectedPosition(Vector2Int position)
    {
        List<GameObject> affectedGameObjects = new List<GameObject>();
        for (int x = -2; x < 3; x++)
        {
            for (int y = -2; y < 3; y++)
            {
                if (gridMap.GetPlacedObject(new Vector2Int(position.x + x, position.y + y)) != null)
                {
                    GameObject gridObject = gridMap.GetPlacedObject(new Vector2Int(position.x + x, position.y + y)).gameObject;
                    int layerIndex = gridObject.layer;
                    if (layerIndex == Harmony.Layers.Player.Index ||
                        layerIndex == Harmony.Layers.Enemy.Index)
                    {
                        affectedGameObjects.Add(gridObject);
                    }
                }
            }
        }
        return affectedGameObjects;
    }

    private int CalculateDistance(PathNode currentNode, PathNode target)
    {
        int distX = Mathf.Abs(currentNode.pos_x - target.pos_x);
        int distY = Mathf.Abs(currentNode.pos_y - target.pos_y);

        if (distX > distY)
        {
            return G_COST * distY + H_COST * (distX - distY);
        }
        return G_COST * distX + H_COST * (distY - distX);
        
    }

    private List<PathNode> RetracePath(PathNode startNode, PathNode endNode)
    {
        List<PathNode> path = new List<PathNode>();
        
        PathNode currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parentNode;
        }
      
        path.Reverse();
        return path;
    }
}
