using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public bool available;
    public bool isSpawnable;
    public GridObject gridObject;
    public float elevation;
    public bool hasWallNorth;
    public bool hasWallSouth;
    public bool hasWallEast;
    public bool hasWallWest;

    public void InitializeVariablesToFalse()
    {
        available = false;
        hasWallNorth = false;
        hasWallSouth = false;
        hasWallEast = false;
        hasWallWest = false;
    }
}