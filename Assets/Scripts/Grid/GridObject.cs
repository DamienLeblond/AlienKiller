using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObject : MonoBehaviour
{
    public Vector2Int CurrentPositionOnGrid { get; private set; }
    private CustomGrid targetGrid;

    private void Start()
    {
        targetGrid = FindAnyObjectByType<CustomGrid>();
        if (targetGrid != null) Init();
    }

    public void Init()
    {
        if (CurrentPositionOnGrid != null) targetGrid.ResetGridObject(CurrentPositionOnGrid);
        CurrentPositionOnGrid = targetGrid.GetGridPosition(transform.position);
        targetGrid.PlaceObject(CurrentPositionOnGrid, this);
    }
}
