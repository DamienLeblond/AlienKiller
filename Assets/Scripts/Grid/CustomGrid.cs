using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CustomGrid : MonoBehaviour
{
    Node[,] grid;
    public int width = 25;
    public int lenght = 25;
    [SerializeField] float cellSize = 2f;
    private void Awake()
    {
        GenerateGrid();
    }

    private void Update()
    {
        CheckPassableTerrain();
    }

    private void GenerateGrid()
    {
        grid = new Node[lenght, width];
        for (int y = 0; y < width; y++)
        {
            for (int x = 0; x < lenght; x++)
            {
                grid[x, y] = new Node();
            }
        }
        CalculateElevation();
    }

    private void CalculateElevation()
    {
        for (int y = 0; y < width; y++)
        {
            for (int x = 0; x < lenght; x++)
            {
                Ray ray = new Ray(GetWorldPosition(x, y) + Vector3.up * 100f, Vector3.down);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, float.MaxValue, Harmony.Layers.Terrain.Mask))
                {
                    grid[x, y].elevation = hit.point.y;
                }
            }
        }
    }

    public void CheckPassableTerrain()
    {
        if (grid == null) { return; }
        for (int y = 0; y < width; y++)
        {
            for (int x = 0; x < lenght; x++)
            {
                Vector3 worldPosition = GetWorldPosition(x, y, true);

                bool hasObstacle = Physics.CheckBox(worldPosition, Vector3.one / 2 * cellSize, Quaternion.identity, Harmony.Layers.Obstacle.Mask);
                bool hasPlayer = Physics.CheckBox(worldPosition, Vector3.one/2 * cellSize, Quaternion.identity, Harmony.Layers.Player.Mask);
                bool hasEnemy = Physics.CheckBox(worldPosition, Vector3.one / 2 * cellSize, Quaternion.identity, Harmony.Layers.Enemy.Mask);
                bool hasExplosive = Physics.CheckBox(worldPosition, Vector3.one / 2 * cellSize, Quaternion.identity, Harmony.Layers.Explosive.Mask);
                bool hasTerrain = Physics.CheckBox(worldPosition, Vector3.one / 2 * cellSize, Quaternion.identity, Harmony.Layers.Terrain.Mask);
                bool hasInteraction = Physics.CheckBox(worldPosition, Vector3.one / 2 * cellSize, Quaternion.identity, Harmony.Layers.Interaction.Mask);
                bool hasWall = Physics.CheckBox(worldPosition, (Vector3.one / 2) * cellSize, Quaternion.identity, Harmony.Layers.Walls.Mask);
                bool hasSpawn = Physics.CheckBox(worldPosition, (Vector3.one / 2) * cellSize, Quaternion.identity, Harmony.Layers.Spawn.Mask);

                grid[x,y].InitializeVariablesToFalse();

                if (!hasObstacle && !hasPlayer && !hasEnemy && hasTerrain && !hasExplosive && !hasInteraction) grid[x, y].available = true;

                if (hasSpawn) grid[x, y].isSpawnable = true;

                if (hasWall)
                {
                    if (Physics.CheckBox(worldPosition + Vector3.right, (Vector3.right / 2) * cellSize, Quaternion.identity, Harmony.Layers.Walls.Mask)) grid[x, y].hasWallEast = true;
                    if (Physics.CheckBox(worldPosition - Vector3.right, (Vector3.right / 2) * cellSize, Quaternion.identity, Harmony.Layers.Walls.Mask)) grid[x, y].hasWallWest = true;
                    if (Physics.CheckBox(worldPosition + Vector3.forward, (Vector3.forward / 2) * cellSize, Quaternion.identity, Harmony.Layers.Walls.Mask)) grid[x, y].hasWallNorth = true;
                    if (Physics.CheckBox(worldPosition - Vector3.forward, (Vector3.forward / 2) * cellSize, Quaternion.identity, Harmony.Layers.Walls.Mask)) grid[x, y].hasWallSouth = true;
                }

            }
        }
    }

    public bool NodeIsAvailable(int x, int y)
    {
        return grid[x, y].available;
    }

    public bool NodeHasWallNorth(int x, int y)
    {
        return grid[x,y].hasWallNorth;
    }
    public bool NodeHasWallSouth(int x, int y)
    {
        return grid[x, y].hasWallSouth;
    }
    public bool NodeHasWallEast(int x, int y)
    {
        return grid[x, y].hasWallEast;
    }
    public bool NodeHasWallWest(int x, int y)
    {
        return grid[x, y].hasWallWest;
    }

    public bool NodeIsSpawnable(int x, int y)
    {
        return grid[x, y].isSpawnable;
    }

    private void OnDrawGizmos()
    {

        if (grid == null)
        {
            for (int y = 0; y < width; y++)
            {


                for (int x = 0; x < lenght; x++)
                {
                    Vector3 pos = GetWorldPosition(x, y);
                    Gizmos.DrawCube(pos, Vector3.one / 4);
                }
            }
        }
        else
        {
            for (int y = 0; y < width; y++)
            {
                for (int x = 0; x < lenght; x++)
                {
                    Vector3 pos = GetWorldPosition(x, y, true);
                    Gizmos.color = grid[x, y].available ? Color.white : Color.red;
                    Gizmos.DrawWireCube(pos, new Vector3(2,0.1f,2));
                }
            }
        }

    }

    public Vector3 GetWorldPosition(int x, int y, bool elevation = false)
    {
        return new Vector3(x * cellSize, elevation == true ? grid[x, y].elevation : 0f, y * cellSize);
    }

    public Vector2Int GetGridPosition(Vector3 worldPosition)
    {
        Vector2Int positionOnGrid = new Vector2Int((int)((worldPosition.x+1) / cellSize ), (int)((worldPosition.z+1) / cellSize));
        return positionOnGrid;
    }

    public Vector2Int GetGridVisualPosition(Vector3 worldPosition)
    {
        Vector2Int positionOnGrid = new Vector2Int((int)Math.Round((worldPosition.x) / cellSize), (int)Math.Round((worldPosition.z) / cellSize));
        return positionOnGrid;
    }


    public void PlaceObject(Vector2Int positionOnGrid, GridObject gridObject)
    {
        if (CheckBoundary(positionOnGrid.x, positionOnGrid.y) == true)
        {
            grid[positionOnGrid.x, positionOnGrid.y].gridObject = gridObject;
        }
        else
        {
            Debug.Log("You trying to put something outside the boundaries!");
        }

    }

    public void ResetGridObject(Vector2Int positionOnGrid)
    {
        grid[positionOnGrid.x, positionOnGrid.y].gridObject = null;
    }

    public GridObject GetPlacedObject(Vector2Int gridPosition)
    {
        if (CheckBoundary(gridPosition.x, gridPosition.y) == true)
        {
            GridObject gridObject = grid[gridPosition.x, gridPosition.y].gridObject;
            return gridObject;
        }
        return null;

    }
    
    
    public bool CheckBoundary(int posX, int posY)
    {
        if (posX < 0 || posX >= lenght)
        {
            return false;
        }
        if (posY < 0 || posY >= width)
        {
            return false;
        }
        return true;
    }

    public Vector2Int FindValidPosition(Vector2Int gridPosition)
    {
        // V�rifier si la position de d�part est valide
        if (NodeIsAvailable(gridPosition.x, gridPosition.y))
        {
            // Si la position de d�part est valide, retourner la position
            return gridPosition;
        }

        // Sinon, chercher une position valide � partir de la position de d�part
        for (int x = gridPosition.x - 1; x <= gridPosition.x + 1; x++)
        {
            for (int y = gridPosition.y - 1; y <= gridPosition.y + 1; y++)
            {
                // V�rifier si la position est � l'int�rieur des limites de la grille
                if (CheckBoundary(x, y))
                {
                    // V�rifier si la position est valide
                    if (NodeIsAvailable(x, y))
                    {
                        // Retourner la position valide
                        return new Vector2Int(x, y);
                    }
                }
            }
        }

        return gridPosition;
    }

    public Vector2Int FindValidAllySpawnPosition(Vector2Int gridPosition)
    {
        //cherche une position valide � partir de la position de d�part
        for (int x = gridPosition.x - 1; x <= gridPosition.x + 1; x++)
        {
            for (int y = gridPosition.y - 1; y <= gridPosition.y + 1; y++)
            {
                // V�rifier si la position est � l'int�rieur des limites de la grille
                if (CheckBoundary(x, y))
                {
                    // V�rifier si la position est valide et spawnable
                    if (NodeIsAvailable(x, y) && NodeIsSpawnable(x, y))
                    {
                        //mettre la position valide � invalide
                        grid[x, y].available = false;
                        // Retourner la position valide
                        return new Vector2Int(x, y);
                    }
                }
            }
        }

        return gridPosition;
    }
}
