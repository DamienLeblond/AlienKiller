﻿public interface IMovingAnimator
{
    public void StartMoving();
    public void StopMoving();
}