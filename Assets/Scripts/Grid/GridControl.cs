using Harmony;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.TextCore.Text;
using UnityEngine.UIElements;

public class GridControl : MonoBehaviour
{
    private CustomGrid targetGrid;
    private ActionBarManager actionBarManager;
    [SerializeField] GameObject currentCharacter;
    [SerializeField] GameObject cursorIndicator;
    private Vector2Int nextCharacterPosition;
    private bool nextMoveIsValid;
    PathFinding pathFinding;
    List<PathNode> path;

    private void Start()
    {
        actionBarManager = FindAnyObjectByType<ActionBarManager>();
        targetGrid = FindAnyObjectByType<CustomGrid>();
        pathFinding = targetGrid.GetComponent<PathFinding>();
    }

    private void Update()
    {
        if (Input.GetAxis("Move") != 0 && actionBarManager.GetCurrentAction() == ActionBarManager.RUN_ACTION_NAME && !EventSystem.current.IsPointerOverGameObject())
        {
            Vector2Int cursorGridPosition = targetGrid.GetGridPosition(cursorIndicator.transform.position);
            MovingCurrentCharacter(cursorGridPosition);
        }
    }
    
    private void MovingCurrentCharacter(Vector2Int clickedGridPosition) 
    {
        Move move = currentCharacter.GetComponent<Move>();
        if (!move.IsMoving())
        {
            Vector2Int characterPositionGrid = targetGrid.GetGridPosition(move.GetPosition());
            path = pathFinding.FindPath(characterPositionGrid.x, characterPositionGrid.y, clickedGridPosition.x, clickedGridPosition.y);
            if (path != null && clickedGridPosition != characterPositionGrid)
            {
                nextCharacterPosition = new Vector2Int(path[path.Count - 1].pos_x, path[path.Count - 1].pos_y);
                move.SetPlayerPosition(nextCharacterPosition, path);
                nextMoveIsValid = true;
            }
        }
    }

    public bool IsNextMoveValid()
    {
        return nextMoveIsValid;
    }

    public void SetValidityOfNextMove(bool isValid)
    {
        this.nextMoveIsValid = isValid;
    }

    private void OnDrawGizmos()
    {
        if (path == null) { return; }
        if (path.Count == 0) { return; }

        for (int i = 0; i < path.Count - 1; i++)
        {
            Gizmos.DrawLine(targetGrid.GetWorldPosition(path[i].pos_x, path[i].pos_y, true),
                targetGrid.GetWorldPosition(path[i ].pos_x, path[i ].pos_y, true));
        }
    }

    public void SetCurrentCharacter(GameObject character) {
        currentCharacter = character;
    }
}
