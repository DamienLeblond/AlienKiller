using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float maxDistance = 25f; // Distance maximale que la balle peut parcourir avant de se d�sactiver
    private Vector3 initialPosition; // Position initiale de la balle

    private void Start()
    {
        // Enregistrer la position initiale de la balle
        initialPosition = transform.position;
    }

    private void Update()
    {
        // Calculer la distance parcourue par la balle depuis sa position initiale
        float distanceTravelled = Vector3.Distance(transform.position, initialPosition);

        // V�rifier si la distance parcourue d�passe la distance maximale
        if (distanceTravelled >= maxDistance)
        {
            // D�sactiver la balle si elle a parcouru la distance maximale
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == Harmony.Layers.Projectile.Mask)
        {
            // Ne pas d�truire la balle si elle entre en collision avec un objet du layer "Projectile"
            return;
        }

        if(collision.gameObject.CompareTag(Harmony.Tags.MainCharacter))
        {
            Vector3 bulletDirection = gameObject.GetComponent<Rigidbody>().velocity.normalized;
            collision.gameObject.GetComponent<PlayerAnimatorManager>().GetHit(bulletDirection);
        }

        if (collision.gameObject.CompareTag(Harmony.Tags.Enemy))
        {
            Vector3 bulletDirection = gameObject.GetComponent<Rigidbody>().velocity.normalized;
            collision.gameObject.GetComponent<IEnnemiAnimatorManager>().GetHit(bulletDirection);
        }

        // D�sactiver la balle lorsqu'elle entre en collision avec un autre objet
        Destroy(gameObject);
    }
}

