using System.Collections.Generic;
using UnityEngine;

public enum BonusType
{
    Accuracy,
    CritDamage,
    CritChance,
    DamageIncrease,
    DamageReduction,
    MeleeWeaponDamage,
    FirstHitBonusDamage,
    DamageIntoHealth,
    BaseHealth,
    ViewDistance,
    HealingBonus,
    HealthRegenPerTurn,
    HealthGainOnKill,
    HealthGainOnCritKill,
    HealthGainOnCrit,
    BonusDamageAfterReload,
    BonusMouvement
}

// Structure pour reprsenter un bonus
[System.Serializable]
public struct Bonus
{
    public BonusType type;
    public float value;
    public bool isPercentage; // Indique si la valeur est un pourcentage
}

[CreateAssetMenu(fileName = "TrinketCreator", menuName = "Trinket")]
public class TrinketObject : ScriptableObject
{
    public new string name;
    public Bonus bonus;
    public Rarity rarity;
    public string effectDescription;
    public Sprite sprite;
    public string condition;
}

[System.Serializable]
public class ObjectNameData
{
    public List<string> objectNames;

    public ObjectNameData()
    {
        objectNames = new List<string>();
    }
}

