using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.XR;


public class OpenTreasure : MonoBehaviour
{
    private List<TrinketObject> commonObjects, uncommonObjects, rareObjects, legendaryObjects;
    private LevelInformation levelInformation;
    private CollectableObjects collectableObjects;
    private UIController uiController;
    private void Awake()
    {
        levelInformation = FindAnyObjectByType<LevelInformation>();
        collectableObjects = FindAnyObjectByType<CollectableObjects>();
        uiController = FindAnyObjectByType<UIController>();
        commonObjects = collectableObjects.GetAllCommonObjects();
        uncommonObjects = collectableObjects.GetAllUncommonObjects();
        rareObjects = collectableObjects.GetAllRareObjects();
        legendaryObjects = collectableObjects.GetAllRareObjects();
    }

    public void Interact()
    {
        uiController.ActivateGetObject(GetRandomObject());
        Destroy(gameObject);
    }
    private TrinketObject GetRandomObject()
    {
        // R�f�rence: https://www.reddit.com/r/Unity3D/comments/ln052o/i_need_help_with_percent_chances/

        float result = Random.Range(0f, 100.0f);

        bool gainUncommonObject = result <= levelInformation.UncommonObjectPercentChance;
        bool gainRareObject = result <= levelInformation.RareObjectPercentChance;
        bool gainLegendaryObject = result <= levelInformation.LegendaryObjectPercentChance;

        List<TrinketObject> chosenTypeObjectList = commonObjects; // Par d�faut
        if (gainLegendaryObject)
        {
            chosenTypeObjectList = legendaryObjects;
        }
        else if (gainRareObject)
        {
            chosenTypeObjectList = rareObjects;
        }
        else if (gainUncommonObject)
        {
            chosenTypeObjectList = uncommonObjects;
        }

        int randomIndex = (int)Random.Range(0f, chosenTypeObjectList.Count);
        return chosenTypeObjectList[randomIndex];
    }



}
