using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CollectableObjects : MonoBehaviour
{
    public List<TrinketObject> objects = new List<TrinketObject>();

    public TrinketObject GetObjectByName(string name)
    {
        foreach (TrinketObject obj in objects)
        {
            if (obj.name == name)
            {
                return obj;
            }  
        }
        return null;
    }

    
    public List<TrinketObject> GetAllRareObjects()
    {
        return objects.Where(obj => obj.rarity == Rarity.Rare).ToList();
    }

    public List<TrinketObject> GetAllCommonObjects()
    {
        return objects.Where(obj => obj.rarity == Rarity.Common).ToList();
    }

    public List<TrinketObject> GetAllUncommonObjects()
    {
        return objects.Where(obj => obj.rarity == Rarity.Uncommon).ToList();
    }

    public List<TrinketObject> GetAllLegendaryObjects()
    {
        return objects.Where(obj => obj.rarity == Rarity.Legendary).ToList();
    }
}
