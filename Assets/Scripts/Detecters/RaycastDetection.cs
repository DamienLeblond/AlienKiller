using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.HID;
using UnityEngineInternal;

public class RaycastDetection : MonoBehaviour
{
    public bool DetectObstacleWithRaycast(Vector3 startPoint, Vector3 endPoint)
    {
        // Cr�ation du rayon entre les deux points
        Ray ray = new Ray(startPoint, endPoint - startPoint);

        // D�finir une distance maximale pour le raycast
        float maxRaycastDistance = Vector3.Distance(startPoint, endPoint);

        // Masque de layer pour les obstacles
        int obstacleLayerMask = 1 << LayerMask.NameToLayer("Obstacle");

        // Effectuer le raycast en utilisant le masque de layer
        if (Physics.Raycast(ray, out RaycastHit hit, maxRaycastDistance, obstacleLayerMask))
        {
            // V�rifier si l'objet touch� est un obstacle
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
            {
                return true;
            }
        }

        return false;
    }
}