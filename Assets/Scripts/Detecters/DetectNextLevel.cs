using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectNextLevel : MonoBehaviour
{
    private GameManager gameManager;
    private List<GameObject> characterInFinalRoom = new();

    private void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Harmony.Tags.MainCharacter))
        {
            if (!characterInFinalRoom.Contains(other.gameObject))
            {
                characterInFinalRoom.Add(other.gameObject);
                IsEveryoneInFinalRoom();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(Harmony.Tags.MainCharacter))
        {
            if (characterInFinalRoom.Contains(other.gameObject))
            {
                characterInFinalRoom.Remove(other.gameObject);
                IsEveryoneInFinalRoom();
            }
        }
    }

    private void IsEveryoneInFinalRoom()
    {
        if (characterInFinalRoom.Count == CountAlivePlayers())
        {
            gameManager.LoadNextLevel();
        }
    }

    private int CountAlivePlayers()
    {
        int count = 0;
        foreach (GameObject player in gameManager.GetPlayers())
        {
            if (player.GetComponent<CharacterStat>().IsAlive)
            {
                count++;
            }
        }
        return count;
    }
}
