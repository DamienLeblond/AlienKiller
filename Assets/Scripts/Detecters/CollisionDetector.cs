using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log(name + ": J'entre dans une collision avec " + collision.gameObject.name);
    }

    void OnCollisionStay(Collision collision)
    {
        Debug.Log(name + ": Je reste dans une collision avec " + collision.gameObject.name);
    }

    void OnCollisionExit(Collision collision)
    {
        Debug.Log(name + ": Je sors d'une collision avec " + collision.gameObject.name);
    }

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log(name + ": J'entre dans un trigger port� par " + collider.gameObject.name);

    }

    void OnTriggerStay(Collider collider)
    {
        Debug.Log(name + ": Je reste dans un trigger port� par " + collider.gameObject.name);
    }

    void OnTriggerExit(Collider collider)
    {
        Debug.Log(name + ": Je sors d'un trigger port� par " + collider.gameObject.name);
    }

}
