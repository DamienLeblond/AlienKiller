using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Random = UnityEngine.Random;
using FischlWorks_FogWar;
using static UnityEngine.EventSystems.EventTrigger;

public class DetectSpawnEnemies : MonoBehaviour
{
    [SerializeField] private List<GameObject> enemySpawnPoints;
    private bool enemiesSpawned = false;
    private PossibleEnemiesForScene possibleEnemiesForScene;
    private GameManager gameManager;
    private GameObject[] doors;
    private List<Animator> doorAnimators = new();
    private int numberEnemy = 0;
    private Move currentPlayerMove;
    private GameObject currentPlayer, fogPlane;
    private CustomGrid customGrid;

    private float timeForEnemySpawnAnimation = 3.5f;

    public bool roomCleared = false;
    public bool roomEntered = false;

    private void OnEnable()
    {
        GameManager.OnDeleteEnemy += OnEnemyKilled;
    }

    private void Start()
    {
        InstantiateGameManager();
        InstantiatePossibleEnemiesForScene();
        InstantiateCustomGrid();
        doors = GetComponentsInChildren<Transform>().Where(x => x.CompareTag(Harmony.Tags.Door)).Select(x => x.gameObject).ToArray();
        foreach (GameObject door in doors)
        {
            doorAnimators.Add(door.GetComponent<Animator>());
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!roomCleared)
        {
            if (other.CompareTag(Harmony.Tags.MainCharacter))
            {
                if (!enemiesSpawned && !currentPlayerMove.IsMoving())
                {
                    InstantiateFogPlane();
                    fogPlane.SetActive(false);
                    SpawnEnemies();
                    foreach (Animator animator in doorAnimators)
                    {
                        animator.SetBool("in_room_combat", true);
                    }
                    enemiesSpawned = true;
                }
                else if (!enemiesSpawned && currentPlayerMove.IsMoving())
                {
                    //si le joueur est en mouvement et qu'il est d�tect� comme �tant dans la salle, mettre sa position � atteindre
                    //le milieu de la case dans laquelle il se trouve, puis l'arr�ter.
                    Vector2Int gridPosition = customGrid.GetGridPosition(currentPlayer.transform.position);
                    Vector3 newPositionToReach = customGrid.GetWorldPosition(gridPosition.x, gridPosition.y);
                    currentPlayerMove.SetPositionToReach(newPositionToReach);
                }
            }
        }
        else
        {
            foreach (Animator animator in doorAnimators)
            {
                animator.SetBool("in_room_combat", false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Harmony.Tags.MainCharacter))
        {
            InstantiateGameManager();
            if (currentPlayer == null) 
            { 
                currentPlayer = gameManager.GetCurrentPlayerTurn();
                currentPlayerMove = currentPlayer.GetComponent<Move>(); 
            }
            possibleEnemiesForScene.SetNbEnemiesInScene(enemySpawnPoints.Count);
        }
    }

    private void OnEnemyKilled()
    {
        if (enemiesSpawned)
        {
            numberEnemy--;
            if (numberEnemy <= 0)
            {
                foreach (Animator animator in doorAnimators)
                {
                    if (animator != null)
                    {
                        animator.SetBool("in_room_combat", false);
                    }
                }
                roomCleared = true;
            }
        }
    }

    private void SpawnEnemies()
    {
        InstantiatePossibleEnemiesForScene();
        int numberOfSpawners = enemySpawnPoints.Count;
        csFogVisibilityAgent csFogVisibilityAgent = null;
        GameObject firstEnemy = null;
        for (int i = 0; i < numberOfSpawners/2; i++)
        {
            int randomIndex = Random.Range(0, enemySpawnPoints.Count);
            GameObject enemy = possibleEnemiesForScene.GetNextEnemy();
            enemy.transform.position = enemySpawnPoints[randomIndex].transform.position;
            enemySpawnPoints.RemoveAt(randomIndex);
            gameManager.AddEnemyToTurnOrder(enemy);
            enemy.GetComponent<Animator>().SetTrigger("Uncover");
            if (i == 0) {
                firstEnemy = enemy;
            }
            numberEnemy++;
        }
        FindFirstObjectByType<CameraEventManager>().RevealCamera(firstEnemy);
        csFogVisibilityAgent = firstEnemy.GetComponent<csFogVisibilityAgent>();
        csFogVisibilityAgent.enabled = false;
        StartCoroutine(WaitForEnemyToSpawn(csFogVisibilityAgent));

        if (!roomEntered)
        {
            TeleportAllies();
            roomEntered = true;
        }
    }

    private void TeleportAllies()
    {
        InstantiateCustomGrid();
        List<GameObject> players = gameManager.GetPlayers();
        Vector3 playerTurnPosition = currentPlayer.transform.position;
        foreach (var player in players)
        {
            Vector2Int validPosition = customGrid.FindValidAllySpawnPosition(customGrid.GetGridPosition(playerTurnPosition));
            player.transform.position = customGrid.GetWorldPosition(validPosition.x, validPosition.y);
            playerTurnPosition = player.transform.position;
        }
    }

    private IEnumerator WaitForEnemyToSpawn(csFogVisibilityAgent visibilityAgent)
    {
        yield return new WaitForSeconds(timeForEnemySpawnAnimation);
        fogPlane.SetActive(true);
        visibilityAgent.enabled = true;
        gameManager.SortTurnOrderBySpeed();
    }

    private void InstantiateGameManager()
    {
        if (gameManager == null)
        {
            gameManager = FindAnyObjectByType<GameManager>();
        }
    }

    private void InstantiatePossibleEnemiesForScene()
    {
        if (possibleEnemiesForScene == null)
        {
            possibleEnemiesForScene = FindAnyObjectByType<PossibleEnemiesForScene>();
        }
    }

    private void InstantiateCustomGrid()
    {
        if (customGrid == null)
        {
            customGrid = FindAnyObjectByType<CustomGrid>();
        }
    }

    private void InstantiateFogPlane()
    {
        if (fogPlane == null)
        {
            fogPlane = GameObject.FindWithTag(Harmony.Tags.FogPlane);
        }
    }
}
