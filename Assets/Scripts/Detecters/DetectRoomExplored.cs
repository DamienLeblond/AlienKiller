using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DetectRoomExplored : MonoBehaviour
{
    public bool roomExplored = false;

    private bool doorsOpened;
    private GameObject[] doors;
    private List<Animator> animators = new();
    private List<GameObject> playersInRoom = new();
    private GameManager gameManager;

    void Start()
    {
        doorsOpened = false;
        gameManager = FindAnyObjectByType<GameManager>();
        doors = GetComponentsInChildren<Transform>().Where(x => x.CompareTag(Harmony.Tags.Door)).Select(x => x.gameObject).ToArray();
        foreach (GameObject door in doors)
        {
            animators.Add(door.GetComponent<Animator>());
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (!roomExplored)
        {
            if (other.CompareTag(Harmony.Tags.MainCharacter))
            {
                if (!playersInRoom.Contains(other.gameObject))
                {
                    playersInRoom.Add(other.gameObject);
                }
                if(gameManager != null)
                {
                    if (playersInRoom.Count >= gameManager.GetNumberOfActivePlayers())
                    {
                        if (animators.Count > 0)
                        {
                            foreach (Animator animator in animators)
                            {
                                animator.SetBool("in_room_combat", false);
                            }
                            roomExplored = true;
                            doorsOpened = true;
                        }
                    }
                }
            }
        }
        else if(!doorsOpened)
        {
            foreach (Animator animator in animators)
            {
                animator.SetBool("in_room_combat", false);
            }
            doorsOpened = true;
        }
    }
}
