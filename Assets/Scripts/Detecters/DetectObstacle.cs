using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectObstacle : MonoBehaviour
{
    public bool IsObstacleBetween(Vector3 startPoint, Vector3 endPoint)
    {
        return Physics.Linecast(startPoint, endPoint, Harmony.Layers.Obstacle.Mask);
    }
}

