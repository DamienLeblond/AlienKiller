using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPositionIndicator : MonoBehaviour
{
    private GameManager gameManager;
    private GameObject currentPlayerTurn;
    private Renderer previewRenderer;
    private Color playerColor = new(0f, 18f/255f, 144f/255f);
    private Color enemyColor = new(255f/255f,96f/255f, 0f);
    void Start()
    {
        gameManager = FindAnyObjectByType<GameManager>();
        previewRenderer = GetComponentInChildren<Renderer>();
    }

    void Update()
    {
        if (currentPlayerTurn != gameManager.GetCurrentPlayerTurn())
        {
            currentPlayerTurn = gameManager.GetCurrentPlayerTurn();
            if (currentPlayerTurn.CompareTag("MainCharacter"))
            {
                previewRenderer.material.color = playerColor;
            }
            else
            {
                previewRenderer.material.color=enemyColor;
            }
        }
        
        transform.position = currentPlayerTurn.transform.position;
    }
}
