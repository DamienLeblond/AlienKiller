using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

public class CharacterInactiveStatus : MonoBehaviour
{
    private const int MAX_TURNS_LEFT_UNTIL_DEATH = 3;
    public bool IsUnconscious { get; private set; }
    private int nbTurnsLeftUntilDeath;
    private GameManager gameManager;
    public bool IsDead { get; private set; }

    private void Awake()
    {
        nbTurnsLeftUntilDeath = MAX_TURNS_LEFT_UNTIL_DEATH;
        gameManager = FindAnyObjectByType<GameManager>();
    }

    public void SetNbTurnsLeftUntilDeath()
    {
        nbTurnsLeftUntilDeath--;
        if (nbTurnsLeftUntilDeath == 0)
        {
            IsUnconscious = false;
            IsDead = true;
            gameObject.SetActive(false);

        }
    }

    public void SetUnconscious()
    {
        IsUnconscious = true;
        gameManager.RemovePlayerOffTurnOrderIfUnconscious(gameObject);
    }

}
