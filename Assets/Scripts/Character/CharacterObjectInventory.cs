using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CharacterObjectInventory : MonoBehaviour
{
    [SerializeField] private List<TrinketObject> objects;
    [SerializeField] private CollectableObjects allObjects;

    private CharacterStat characterStat;

    private const string saveFileNamePrefix = "character_object_inventory_";
    private const string saveFileExtension = ".json";


    private void Start()
    {
        characterStat = GetComponent<CharacterStat>();
    }

    public void SaveCharacterObjectInventory(int characterIndex)
    {
        string saveFilePath = Application.persistentDataPath + "/" + saveFileNamePrefix + characterIndex.ToString() + saveFileExtension;

        ObjectNameData data = new ObjectNameData();
        foreach (TrinketObject obj in objects)
        {
            data.objectNames.Add(obj.name);
        }

        string json = JsonUtility.ToJson(data);

        File.WriteAllText(saveFilePath, json);
    }



    public void LoadCharacterObjectInventory(int characterIndex)
    {
        string saveFilePath = Application.persistentDataPath + "/" + saveFileNamePrefix + characterIndex.ToString() + saveFileExtension;
        if (File.Exists(saveFilePath))
        {
            string json = File.ReadAllText(saveFilePath);
            ObjectNameData data = JsonUtility.FromJson<ObjectNameData>(json);

            objects.Clear();
            string objectName = "";
            foreach (string name in data.objectNames)
            {
                if(objectName == name)
                {
                    AddObject(allObjects.GetObjectByName(objectName));
                }
                else
                {
                    objectName = name;
                    AddObject(allObjects.GetObjectByName(objectName));
                }   
            }
        }
    }

    public List<TrinketObject> GetObjects()
    {
        return objects;
    }

    public void Recalculate()
    {
        characterStat.Reset();
        foreach (TrinketObject obj in objects)
        {
            ApplyObjectBonuses(obj);
        }
    }

    public void AddObject(TrinketObject obj)
    {
        if(obj != null)
        {
            ApplyObjectBonuses(obj);
            AddObjectToList(obj);
        }
   }

    private void AddObjectToList(TrinketObject obj)
    {
        objects.Add(obj);
    }
    private void ApplyObjectBonuses(TrinketObject obj)
    {
        switch (obj.bonus.type)
        {
            case BonusType.Accuracy:
                characterStat.AddAccuracyBonus((int)obj.bonus.value);
                break;
            case BonusType.CritDamage:
                characterStat.AddCritDamageBonus((int)obj.bonus.value);
                break;
            case BonusType.DamageIncrease:
                characterStat.AddDamageIncreaseBonus((int)obj.bonus.value);
                break;
            case BonusType.DamageReduction:
                characterStat.AddDamageReductionBonus((int)obj.bonus.value);
                break;
            case BonusType.MeleeWeaponDamage:
                characterStat.AddMeleeWeaponDamageBonus((int)obj.bonus.value);
                break;
            case BonusType.FirstHitBonusDamage:
                characterStat.AddFirstHitBonusDamage((int)obj.bonus.value);
                break;
            case BonusType.DamageIntoHealth:
                characterStat.AddDamageIntoHealthBonus((int)obj.bonus.value);
                break;
            case BonusType.BaseHealth:
                characterStat.AddBaseHealthBonus((int)obj.bonus.value);
                break;
            case BonusType.ViewDistance:
                characterStat.AddBonusViewDistance((int)obj.bonus.value);
                break;
            case BonusType.HealingBonus:
                characterStat.AddHealingBonus((int)obj.bonus.value);
                break;
            case BonusType.HealthRegenPerTurn:
                characterStat.AddHealthRegenPerTurn((int)obj.bonus.value);
                break;
            case BonusType.HealthGainOnKill:
                characterStat.AddHealthGainOnKill((int)obj.bonus.value);
                break;
            case BonusType.HealthGainOnCritKill:
                characterStat.AddHealthGainOnCritKill((int)obj.bonus.value);
                break;
            case BonusType.HealthGainOnCrit:
                characterStat.AddHealthGainOnCrit((int)obj.bonus.value);
                break;
            case BonusType.BonusDamageAfterReload:
                characterStat.AddBonusDamageAfterReload((int)obj.bonus.value);
                break;
            case BonusType.BonusMouvement:
                characterStat.AddBonusMovement((int)obj.bonus.value);
                break;
            default:
                Debug.LogWarning("Bonus type not handled: " + obj.bonus.type);
                break;
        }
    }
}
