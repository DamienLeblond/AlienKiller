using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Burst.Intrinsics;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public class CharacterWeaponInventory : MonoBehaviour
{
    [SerializeField] private AllWeapon allWeapons;
    [SerializeField] private Weapon firstWeapon;
    [SerializeField] private Weapon secondWeapon;
    private int currentWeaponIndex; // L'arme actuellement quipe
    private int munitionFirstWeapon;
    private int munitionSecondWeapon;
    private bool justReloaded;
    [SerializeField] private WeaponList weaponList;
    private static readonly int CRIT_MULTIPLICATOR = 200; //200% alors le double

    private const string saveFileNamePrefix = "weapon_data_";
    private const string saveFileExtension = ".json";
    private CharacterStat characterStat;


    void Start()
    {
        characterStat = GetComponent<CharacterStat>();
        // Initialisez l'arme actuellement quipe  firstWeapon
        currentWeaponIndex = 1;

        munitionFirstWeapon = firstWeapon.maxMunition;
        munitionSecondWeapon = secondWeapon.maxMunition;
    }

    public void SaveWeaponData(int characterIndex)
    {
        string saveFilePath = Application.persistentDataPath + "/" + saveFileNamePrefix + characterIndex.ToString() + saveFileExtension;

        WeaponNameData data = new WeaponNameData();

        data.weaponNames.Add(firstWeapon.name);
        data.weaponNames.Add(secondWeapon.name);

        string json = JsonUtility.ToJson(data);

        File.WriteAllText(saveFilePath, json);
    }


    public void LoadWeaponData(int characterIndex)
    {
        string saveFilePath = Application.persistentDataPath + "/" + saveFileNamePrefix + characterIndex.ToString() + saveFileExtension;
        if (File.Exists(saveFilePath))
        {
            string json = File.ReadAllText(saveFilePath);
            WeaponNameData data = JsonUtility.FromJson<WeaponNameData>(json);

            int index = 0;
            foreach (string name in data.weaponNames)
            {
                if (index == 0)
                {
                    index++;
                    firstWeapon = allWeapons.GetWeaponByName(name);
                    munitionFirstWeapon = firstWeapon.maxMunition;
                }
                else if (index == 1)
                {
                    secondWeapon = allWeapons.GetWeaponByName(name);
                    munitionSecondWeapon = secondWeapon.maxMunition;
                }
            }
        }
    }


    public void ReplaceWeapon(int slot, Weapon newWeapon)
    {
        switch (slot)
        {
            case 1:
                firstWeapon = newWeapon;
                munitionFirstWeapon = firstWeapon.maxMunition;
                break;
            case 2:
                secondWeapon = newWeapon;
                munitionSecondWeapon = secondWeapon.maxMunition;
                break;
            default:
                Debug.LogError("Slot d'arme invalide : " + slot);
                break;
        }
    }


    public void SwitchWeapon()
    {
        if (currentWeaponIndex == 1)
        {
            currentWeaponIndex = 2;
        }
        else if ( currentWeaponIndex == 2)
        {
            currentWeaponIndex = 1;
        }
    }


    public void SetCurrentWeapon(int weaponIndex)
    {
        if (weaponIndex == 1)
        {
            currentWeaponIndex = 1;
        }
        else if (weaponIndex == 2)
        {
            currentWeaponIndex = 2;
        }
        else
        {
            Debug.LogError("Index d'arme invalide : " + weaponIndex);
        }
    }

    public Weapon GetCurrentWeapon()
    {
        // Vrifiez d'abord si l'arme actuellement quipe est nulle, si oui, configurez-la sur Fist
        if (currentWeaponIndex == 1)
        {
            if (firstWeapon == null)
            {
                firstWeapon = GetFistWeapon();
            }
        }
        else
        {
            if (secondWeapon == null)
            {
                secondWeapon = GetFistWeapon();
            }
        }

        // Retourne l'arme actuellement quipe
        return currentWeaponIndex == 1 ? firstWeapon : secondWeapon;
    }

    // Mthode pour obtenir l'arme "Fist"  partir de votre liste d'armes
    private Weapon GetFistWeapon()
    {

        return weaponList.GetFistWeapon();
    }

    public bool IsWeaponFist()
    {
        if (currentWeaponIndex == 1)
        {
            if (firstWeapon.name == "Fist")
            {
                return true;
            }
            return false;
        }
        if (currentWeaponIndex == 2)
        {
            if (secondWeapon.name == "Fist")
            {
                return true;
            }
            return false;
        }
        return false;
    }

    public void UseAmmo()
    {
        if (currentWeaponIndex == 1)
        {
            munitionFirstWeapon--;
        }
        else
        {
            munitionSecondWeapon--;
        }
    }

    // Fonction pour vrifier s'il reste des munitions dans l'arme equipe
    public int MunitionRemaining(int index) //index 0 pour munition arme equip, sinon index de l'arme qu'on veut
    {
        if (IsWeaponFist())
        {
            return 1;
        }
        if (index == 0)
        {
            index = currentWeaponIndex;
        }
        if (index == 1)
        {
            return munitionFirstWeapon;
        }
        if (index == 2)
        {
            return munitionSecondWeapon;
        }
        return -1;
    }

    public int MaxMunition(int index) //index 0 pour munition arme equip, sinon index de l'arme qu'on veut
    {

        if (index == 0)
        {
            index = currentWeaponIndex;
        }
        if (index == 1)
        {
            return firstWeapon.maxMunition;
        }
        if (index == 2)
        {
            return secondWeapon.maxMunition;
        }
        return -1;
    }

    public bool IsWeaponAtMaxMunition()
    {
        if (currentWeaponIndex == 1)
        {
            if (munitionFirstWeapon < firstWeapon.maxMunition)
            {
                return false;
            }
            return true;

        }
        else
        {
            if (munitionSecondWeapon < secondWeapon.maxMunition)
            {
                return false;
            }
            return true;
        }
    }


    // Fonction pour remplir les munitions de l'arme equipe
    public void RefillAmmo()
    {
        if (currentWeaponIndex == 1)
        {
            munitionFirstWeapon = firstWeapon.maxMunition;
        }
        else
        {
            munitionSecondWeapon = secondWeapon.maxMunition;
        }
        justReloaded = true;
    }

    public int ActionCostToFire()
    {
        if (currentWeaponIndex == 1)
        {
            return firstWeapon.numberOfActionToShoot;
        }
        else
        {
            return secondWeapon.numberOfActionToShoot;
        }
    }

    public int GetMinDamage()
    {
        if (currentWeaponIndex == 1)
        {
            return firstWeapon.minDamage;
        }
        else
        {
            return secondWeapon.minDamage;
        }
    }

    public int GetMaxDamage(GameObject enemy)
    {

        int bonusDamage = characterStat.DamageIncrease;
        if (justReloaded)
        {
            bonusDamage += characterStat.BonusDamageAfterReload;
        }
        if (enemy.GetComponent<EnemyHealth>().IsFullHealth() == true)
        {
            bonusDamage += characterStat.FirstHitBonusDamage;
        }
        
        

        //Rajout� les calculs de d�g�t bonus
        if (currentWeaponIndex == 1)
        {
            if (firstWeapon.isMelee)
            {
                bonusDamage += characterStat.MeleeWeaponDamage;
            }
            return Mathf.FloorToInt(firstWeapon.maxDamage + (firstWeapon.maxDamage * (bonusDamage / 100)));
        }
        else
        {
            if(secondWeapon.isMelee)
            {
                bonusDamage += characterStat.MeleeWeaponDamage;
            }
            return Mathf.FloorToInt(secondWeapon.maxDamage + (secondWeapon.maxDamage * (bonusDamage / 100)));
        }
    }

    public int GetDamage(bool isCrit, bool ennemyFullLife)
    {
        int minDamage;
        int maxDamage;
        int bonusDamage = 0;

        // Dterminez les valeurs min et max de dgts en fonction de l'arme actuellement quipe
        if (currentWeaponIndex == 1)
        {
            minDamage = firstWeapon.minDamage;
            maxDamage = firstWeapon.maxDamage;
            if (firstWeapon.isMelee)
            {
                bonusDamage += characterStat.MeleeWeaponDamage;
            }
        }
        else
        {
            if (secondWeapon.isMelee)
            {
                bonusDamage += characterStat.MeleeWeaponDamage;
            }
            minDamage = secondWeapon.minDamage;
            maxDamage = secondWeapon.maxDamage;
        }

        if (justReloaded)
        {
            bonusDamage += characterStat.BonusDamageAfterReload;
        }
        if (ennemyFullLife == true)
        {
            bonusDamage += characterStat.FirstHitBonusDamage;
        }
        bonusDamage += characterStat.DamageIncrease;
        int damage = Mathf.FloorToInt(UnityEngine.Random.Range(minDamage, maxDamage + 1));

        // Retournez une valeur alatoire entre minDamage et maxDamage inclusivement
        if (isCrit)
        {
            return Mathf.FloorToInt((damage + (damage * (bonusDamage / 100))) * ((CRIT_MULTIPLICATOR + characterStat.CritDamage) / 100));
        }
        return Mathf.FloorToInt(damage + (damage * (bonusDamage / 100)));
    }

    public int GetCurrentWeaponRange()
    {
        if (currentWeaponIndex == 1)
        {
            return firstWeapon.range;
        }
        else
        {
            return secondWeapon.range;
        }
    }

    public Sprite GetWeaponIcon(int index)
    {
        if (index == 0)
        {
            index = currentWeaponIndex;
        }
        if (index == 1)
        {
            return firstWeapon.sprite;
        }
        if (index == 2)
        {
            return secondWeapon.sprite;
        }
        return null;
    }

    public GameObject GetWeaponModel(int index)
    {
        if (index == 0)
        {
            index = currentWeaponIndex;
        }
        if (index == 1)
        {
            return firstWeapon.prefab;
        }
        if (index == 2)
        {
            return secondWeapon.prefab;
        }
        return null;
    }
}

[System.Serializable]
public class WeaponNameData
{
    public List<string> weaponNames;

    public WeaponNameData()
    {
        weaponNames = new List<string>();
    }
}