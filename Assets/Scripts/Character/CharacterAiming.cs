using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAiming : MonoBehaviour
{
    public void RotateTowardsTarget(Transform target)
    {
        if (target != null)
        {
            // Calculer la direction du target par rapport � la position actuelle
            Vector3 direction = target.position - transform.position;

            // Calculer la rotation en utilisant la direction
            Quaternion rotation = Quaternion.LookRotation(direction);

            // Extraire l'angle Y de la rotation calcul�e
            float angleY = rotation.eulerAngles.y;

            // Cr�er une nouvelle rotation en utilisant uniquement l'angle Y
            Quaternion newRotation = Quaternion.Euler(0f, angleY, 0f);

            // Appliquer la rotation au GameObject
            transform.rotation = newRotation;
        }
        else
        {
            Debug.LogWarning("Le target n'est pas d�fini.");
        }
    }
}
