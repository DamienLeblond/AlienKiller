using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CharacterStat : MonoBehaviour
{
    private int baseHealth;
    private int baseMouvement;
    private int baseViewDistance;
    [SerializeField] private int baseSpeed;
    [SerializeField] private int currentHealth;
    private int accuracy;
    private int critDamage;
    private int critChance;
    private int damageIncrease;
    private int damageReduction;
    private int meleeWeaponDamage;
    private int firstHitBonusDamage;
    private int damageIntoHealth;
    private int bonusHealth;
    private int bonusViewDistance;
    public int healingBonus;
    public int healthRegenPerTurn;
    private int healthGainOnKill;
    private int healthGainOnCritKill;
    private int healthGainOnCrit;
    private int bonusDamageAfterReload;
    private int bonusMovement;
    [SerializeField] private CharacterHealthBar healthBar;

    private const string saveFileNamePrefix = "character_stat_";
    private const string saveFileExtension = ".json";

    private GlobalStats globalStats;
    [SerializeField] public GlobalStatsData globalStatsData;

    private void Start()
    {
        globalStats = new GlobalStats();
        baseHealth = 1000;
        baseMouvement = 8;
        baseViewDistance = 10;
        baseSpeed = 40;
        currentHealth = 1000;

    }

    public void SaveCharacterStat(int characterIndex)
    {
        string saveFilePath = Application.persistentDataPath + "/" + saveFileNamePrefix + characterIndex.ToString() + saveFileExtension;

        CharacterStatData data = new CharacterStatData(
            baseHealth,
            currentHealth,
            baseMouvement,
            baseViewDistance,
            baseSpeed,
            accuracy,
            critDamage,
            critChance,
            damageIncrease,
            damageReduction,
            meleeWeaponDamage,
            firstHitBonusDamage,
            damageIntoHealth,
            bonusHealth,
            bonusViewDistance,
            healingBonus,
            healthRegenPerTurn,
            healthGainOnKill,
            healthGainOnCritKill,
            healthGainOnCrit,
            bonusDamageAfterReload,
            bonusMovement
        );

        string json = JsonUtility.ToJson(data);
        File.WriteAllText(saveFilePath, json);

        globalStats.SaveGlobalStats(globalStatsData, characterIndex);
    }


    public void LoadCharacterStat(int characterIndex)
    {
        string saveFilePath = Application.persistentDataPath + "/" + saveFileNamePrefix + characterIndex.ToString() + saveFileExtension;
        if (File.Exists(saveFilePath))
        {
            string json = File.ReadAllText(saveFilePath);
            CharacterStatData data = JsonUtility.FromJson<CharacterStatData>(json);
            baseHealth = data.baseHealth;
            currentHealth = data.currentHealth;
            baseMouvement = data.baseMovement;
            baseViewDistance = data.baseViewDistance;
            baseSpeed = data.baseSpeed;
            accuracy = data.accuracy;
            critDamage = data.critDamage;
            critChance = data.critChance;
            damageIncrease = data.damageIncrease;
            damageReduction = data.damageReduction;
            meleeWeaponDamage = data.meleeWeaponDamage;
            firstHitBonusDamage = data.firstHitBonusDamage;
            damageIntoHealth = data.damageIntoHealth;
            bonusHealth = data.bonusHealth;
            bonusViewDistance = data.bonusViewDistance;
            healingBonus = data.healingBonus;
            healthRegenPerTurn = data.healthRegenPerTurn;
            healthGainOnKill = data.healthGainOnKill;
            healthGainOnCritKill = data.healthGainOnCritKill;
            healthGainOnCrit = data.healthGainOnCrit;
            bonusDamageAfterReload = data.bonusDamageAfterReload;
            bonusMovement = data.bonusMovement;
        }
        globalStatsData = globalStats.LoadGlobalStats(characterIndex);
    }

    public void Reset()
    {
        baseHealth = 1000;
        baseMouvement = 8;
        baseViewDistance = 10;
        baseSpeed = 40;

        damageIncrease = 0;
        damageReduction = 0;
        meleeWeaponDamage = 0;
        firstHitBonusDamage = 0;
        damageIntoHealth = 0;
        bonusHealth = 0;
        bonusViewDistance = 0;
        healingBonus = 0;
        healthRegenPerTurn = 0;
        healthGainOnKill = 0;
        healthGainOnCritKill = 0;
        healthGainOnCrit = 0;
        bonusDamageAfterReload = 0;
        bonusMovement = 0;
    }

    public int MaxHealth
    {
        get { return baseHealth + bonusHealth; }
    }

    public int Movement
    {
        get { return baseMouvement + bonusMovement; }
    }

    public int ViewDistance
    {
        get { return baseViewDistance + bonusViewDistance; }
    }

    public int Speed
    {
        get { return baseSpeed; }
    }

    public int CurrentHealth
    {
        get { return currentHealth; }
    }

    public int Accuracy
    {
        get { return accuracy; }
    }

    public int CritDamage
    {
        get { return critDamage; }
    }

    public int CritChance
    {
        get { return critChance; }
    }

    public int DamageIncrease
    {
        get { return damageIncrease; }
    }

    public int DamageReduction
    {
        get { return damageReduction; }
    }

    public int MeleeWeaponDamage
    {
        get { return meleeWeaponDamage; }
    }

    public int FirstHitBonusDamage
    {
        get { return firstHitBonusDamage; }
    }

    public int DamageIntoHealth
    {
        get { return damageIntoHealth; }
    }


    public int BonusViewDistance
    {
        get { return bonusViewDistance; }
    }

    public int HealingBonus
    {
        get { return healingBonus; }
    }

    public int HealthRegenPerTurn
    {
        get { return healthRegenPerTurn; }
    }

    public int HealthGainOnKill
    {
        get { return healthGainOnKill; }
    }

    public int HealthGainOnCritKill
    {
        get { return healthGainOnCritKill; }
    }

    public int HealthGainOnCrit
    {
        get { return healthGainOnCrit; }
    }

    public int BonusDamageAfterReload
    {
        get { return bonusDamageAfterReload; }
    }

    public bool IsAlive
    {
        get { return currentHealth >0; }
    }

    public void AddBaseHealthBonus(int bonus)
    {
        bonusHealth += bonus;
    }

    public void AddAccuracyBonus(int bonus)
    {
        accuracy += bonus;
    }

    public void AddCritDamageBonus(int bonus)
    {
        critDamage += bonus;
    }

    public void AddCritChanceBonus(int bonus)
    {
        critChance += bonus;
    }


    public void AddDamageIncreaseBonus(int bonus)
    {
        damageIncrease += bonus;
    }

    public void AddDamageReductionBonus(int bonus)
    {
        damageReduction += bonus;
    }

    public void AddMeleeWeaponDamageBonus(int bonus)
    {
        meleeWeaponDamage += bonus;
    }

    public void AddFirstHitBonusDamage(int bonus)
    {
        firstHitBonusDamage += bonus;
    }

    public void AddDamageIntoHealthBonus(int bonus)
    {
        damageIntoHealth += bonus;
    }

    public void AddBonusHealth(int bonus)
    {
        bonusHealth += bonus;
    }

    public void AddBonusViewDistance(int bonus)
    {
        bonusViewDistance += bonus;
    }

    public void AddHealingBonus(int bonus)
    {
        healingBonus += bonus;
    }

    public void AddHealthRegenPerTurn(int bonus)
    {
        healthRegenPerTurn += bonus;
    }

    public void AddHealthGainOnKill(int bonus)
    {
        healthGainOnKill += bonus;
    }

    public void AddHealthGainOnCritKill(int bonus)
    {
        healthGainOnCritKill += bonus;
    }

    public void AddHealthGainOnCrit(int bonus)
    {
        healthGainOnCrit += bonus;
    }

    public void AddBonusDamageAfterReload(int bonus)
    {
        bonusDamageAfterReload += bonus;
    }

    public void AddBonusMovement(int bonus)
    {
        bonusMovement += bonus;
    }

    public void Heal(int amount)
    {
        currentHealth += Mathf.RoundToInt(amount + (amount * healingBonus));
        globalStatsData.hpRecovered += Mathf.RoundToInt(amount + (amount * healingBonus));
        healthBar.UpdateValue();
    }

    public void AttackHeal(int damage, bool crit, bool kill)
    {
        int healAmount = 0;
        healAmount += damage * (damageIntoHealth / 100);
        if (crit)
        {
            healAmount += healthGainOnCrit;
        }
        if (kill)
        {
            healAmount += healthGainOnKill;
        }
        if (crit && kill)
        {
            healAmount += healthGainOnCritKill;
        }
        Heal(healAmount);
    }

    public void GetHit(bool isCrit, int damage)
    {
        if (isCrit)
        {
            damage *= 2;
        }
        currentHealth -= (damage - damageReduction);
        globalStatsData.totalDamageReceived += (damage - damageReduction);
        AchievementSaveManager.lostLife = true;
    }

    internal float GetPercentageHealthRemaining()
    {
        if(MaxHealth == 0)
        {
            return (float)currentHealth / (float)1000f;
        }
        return (float)currentHealth / (float)MaxHealth;
    }

    internal void HealByTurn()
    {
        int healingAmount = MaxHealth * healthRegenPerTurn / 100;
        Heal(healingAmount);
    }
}

[System.Serializable]
public class CharacterStatData
{
    public int baseHealth;
    public int currentHealth;
    public int baseMovement;
    public int baseViewDistance;
    public int baseSpeed;
    public int accuracy;
    public int critDamage;
    public int critChance;
    public int damageIncrease;
    public int damageReduction;
    public int meleeWeaponDamage;
    public int firstHitBonusDamage;
    public int damageIntoHealth;
    public int bonusHealth;
    public int bonusViewDistance;
    public int healingBonus;
    public int healthRegenPerTurn;
    public int healthGainOnKill;
    public int healthGainOnCritKill;
    public int healthGainOnCrit;
    public int bonusDamageAfterReload;
    public int bonusMovement;

    public CharacterStatData(
        int baseHealth,
        int currentHealth,
        int baseMovement,
        int baseViewDistance,
        int baseSpeed,
        int accuracy,
        int critDamage,
        int critChance,
        int damageIncrease,
        int damageReduction,
        int meleeWeaponDamage,
        int firstHitBonusDamage,
        int damageIntoHealth,
        int bonusHealth,
        int bonusViewDistance,
        int healingBonus,
        int healthRegenPerTurn,
        int healthGainOnKill,
        int healthGainOnCritKill,
        int healthGainOnCrit,
        int bonusDamageAfterReload,
        int bonusMovement)
    {
        this.baseHealth = baseHealth;
        this.currentHealth = currentHealth;
        this.baseMovement = baseMovement;
        this.baseViewDistance = baseViewDistance;
        this.baseSpeed = baseSpeed;
        this.accuracy = accuracy;
        this.critDamage = critDamage;
        this.critChance = critChance;
        this.damageIncrease = damageIncrease;
        this.damageReduction = damageReduction;
        this.meleeWeaponDamage = meleeWeaponDamage;
        this.firstHitBonusDamage = firstHitBonusDamage;
        this.damageIntoHealth = damageIntoHealth;
        this.bonusHealth = bonusHealth;
        this.bonusViewDistance = bonusViewDistance;
        this.healingBonus = healingBonus;
        this.healthRegenPerTurn = healthRegenPerTurn;
        this.healthGainOnKill = healthGainOnKill;
        this.healthGainOnCritKill = healthGainOnCritKill;
        this.healthGainOnCrit = healthGainOnCrit;
        this.bonusDamageAfterReload = bonusDamageAfterReload;
        this.bonusMovement = bonusMovement;
    }
}

