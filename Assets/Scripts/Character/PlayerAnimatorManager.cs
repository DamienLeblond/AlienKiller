using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerAnimatorManager : MonoBehaviour, IMovingAnimator
{
    private Animator playerAnimator;
    private CharacterWeaponInventory weaponInventory;
    [SerializeField] private List<AnimationClip> meleeAttackAnimations;
    [SerializeField] private CharacterHealthBar healthBar;
    private CharacterStat characterStat;
    private bool alreadyHit = false;
    private CharacterInactiveStatus characterInactiveStatus;
    private GameObject blood;
    private float distanceBehind = 0.5f;
    [SerializeField] private AudioClip hurtClip;
    [SerializeField] private AudioClip meleeHitClip;
    private AudioSource audioSource;

    void Start()
    {
        // Obtenez la r�f�rence de l'Animator attach� au GameObject du joueur
        playerAnimator = GetComponent<Animator>();

        // V�rifiez si l'Animator a �t� correctement attach�
        if (playerAnimator == null)
        {
            Debug.LogError("Animator non trouv� sur le GameObject du joueur.");
        }

        characterInactiveStatus = GetComponent<CharacterInactiveStatus>();
        weaponInventory = GetComponent<CharacterWeaponInventory>();
        characterStat = GetComponent<CharacterStat>();
        blood = GameObject.Find("Blood");
        audioSource = GetComponent<AudioSource>();
    }

    public void PutAwayGun()
    {

        Transform weaponR = transform.Find("Weapon_R");

        if (weaponR.childCount > 0)
        {
            // R�cup�rer le premier enfant de Weapon_R
            Transform child = weaponR.GetChild(0);

            // V�rifier si l'enfant existe avant de le d�truire
            if (child != null)
            {
                Destroy(child.gameObject);
            }
        }

        if (weaponInventory.IsWeaponFist() == false)
        {
            playerAnimator.SetBool("IsWeaponFist", false);
        }
        else
        {
            playerAnimator.SetBool("IsWeaponFist", true);
        }
    }

    public void TakeNewGun()
    {
        Transform weaponR = transform.Find("Weapon_R");

        GameObject newWeaponPrefab = weaponInventory.GetWeaponModel(0);

        // V�rifier si le nouveau mod�le d'arme existe
        if (newWeaponPrefab != null)
        {
            Vector3 prefabPosition = newWeaponPrefab.transform.localPosition;
            Quaternion prefabRotation = newWeaponPrefab.transform.localRotation;
            Vector3 prefabScale = newWeaponPrefab.transform.localScale;

            // Instancier le prefab du nouveau mod�le d'arme avec ses valeurs de position, de rotation et d'�chelle d'origine
            GameObject newWeapon = Instantiate(newWeaponPrefab, weaponR.position, weaponR.rotation);

            // D�finir le parent de la nouvelle arme sur "Weapon_R" pour la placer dans les mains du personnage
            newWeapon.transform.SetParent(weaponR, false);

            // R�tablir les valeurs de position, de rotation et d'�chelle du prefab sur la nouvelle arme
            newWeapon.transform.localPosition = prefabPosition;
            newWeapon.transform.localRotation = prefabRotation;
            newWeapon.transform.localScale = prefabScale;
        }


    }

    public void OnHitAnimationFinished()
    {
        // R�initialiser le marqueur pour permettre � l'objet d'�tre touch� � nouveau
        alreadyHit = false;
    }

    public void StartRandomMeleeAttack()
    {
        // V�rifiez si la liste des animations est vide
        if (meleeAttackAnimations.Count == 0)
        {
            Debug.LogWarning("Liste d'animations d'attaque au corps � corps vide.");
            return;
        }

        // Choisir al�atoirement une animation d'attaque au corps � corps
        AnimationClip randomAnimation = meleeAttackAnimations[Random.Range(0, meleeAttackAnimations.Count)];

        // Jouer l'animation s�lectionn�e
        PlaySound(meleeHitClip, 1, 1);
        playerAnimator.Play(randomAnimation.name, LayerMask.NameToLayer("LegsLayer"));
        playerAnimator.Play(randomAnimation.name, playerAnimator.GetLayerIndex("Hand"));

    }

    public void StartShootingAnimation()
    {
        playerAnimator.Play("Shoot_Rifle", playerAnimator.GetLayerIndex("Hand"));
    }


    // M�thode pour d�marrer l'animation de changement d'arme
    public void ChangeWeaponAnimation()
    {
        // D�marrer l'animation de changement d'arme
        playerAnimator.SetTrigger("ChangeGun");
    }

    public void GetHit(Vector3 bulletDirection)
    {
        // V�rifier si l'objet n'a pas d�j� �t� touch�
        if (!alreadyHit)
        {
            // Marquer l'objet comme touch�
            alreadyHit = true;

            playerAnimator.SetTrigger("Hit");
            // Jouer le son du personnage bless�
            PlaySound(hurtClip, 1, 0.8f);

            Vector3 bloodPosition = transform.position + bulletDirection.normalized * distanceBehind;

            // Placer le GameObject "Blood" � la position calcul�e
            GameObject bloodInstance = Instantiate(blood, bloodPosition, Quaternion.identity);

            // Calculer la rotation pour orienter l'�mission du syst�me de particules dans la direction oppos�e � la balle
            Quaternion rotation = Quaternion.LookRotation(-bulletDirection.normalized) * Quaternion.Euler(0, 180, 0);

            // Appliquer la rotation � l'�mission du syst�me de particules
            var particleSystem = bloodInstance.GetComponent<ParticleSystem>();
            if (particleSystem != null)
            {
                var shape = particleSystem.shape;
                shape.rotation = rotation.eulerAngles;
                particleSystem.Play();
            }

            healthBar.UpdateValue();

            if (characterStat.CurrentHealth <= 0)
            {
                playerAnimator.SetBool("Dead", true);
            }
            // Jouer l'animation de se faire toucher
            
        }
    }

    public void AfterDeadAnimation()
    {
        characterInactiveStatus.SetUnconscious();
    }

    public void PlaySound(AudioClip audioClip, float volume, float pitch)
    {
        audioSource.clip = audioClip;
        audioSource.volume = volume;
        audioSource.pitch = pitch;
        audioSource.Play();
    }

    public void StartMoving()
    {
        playerAnimator.SetBool("Sprint", true );
    }

    public void StopMoving()
    {
        playerAnimator.SetBool("Sprint", false);
    }
}
