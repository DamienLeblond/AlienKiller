using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInfo : MonoBehaviour
{
    private int critChance;
    private int chanceToBeHit;
    private int chanceToBeCrit;
    private int baseChanceToBeCrit = 10;
    private int bonusCritWhenFlanked = 20;
    private bool flanked = false;
    private float flankDistanceThreshold = 4f;

    public int GetCritChance()
    {
        return critChance;
    }

    public int GetChanceToBeHit()
    {
        return chanceToBeHit;
    }

    public void SetChanceToBeHit(int chance)
    {
        chanceToBeHit = chance;
    }
    public int GetChanceToBeCrit()
    {
        return chanceToBeCrit;
    }

    public void SetChanceToBeCrit(int bonusChance)
    {
        chanceToBeCrit = baseChanceToBeCrit + bonusChance;
        if (flanked)
        {
            chanceToBeCrit += bonusCritWhenFlanked;
        }
    }

    public void IsCharacterFlanked(GameObject enemy)
    {
        Vector3 playerPosition = gameObject.transform.position;
        Vector3 enemyPosition = enemy.transform.position;

        GameObject[] obstacles = GameObject.FindGameObjectsWithTag(Harmony.Tags.Obstacle);

        bool isFlanked = false;

        foreach (GameObject obstacle in obstacles)
        {
            float distanceX = Mathf.Abs(playerPosition.x - obstacle.transform.position.x);
            float distanceY = Mathf.Abs(playerPosition.y - obstacle.transform.position.y);
            if (distanceX <= 3 || distanceY <= 3)
            {
                Vector3 playerToEnemy = enemyPosition - playerPosition;
                Vector3 playerToObstacle = obstacle.transform.position - playerPosition;
                float distancePlayerToObstacle = Vector3.Distance(playerPosition, obstacle.transform.position);
                float distancePlayerToEnemy = Vector3.Distance(playerPosition, enemyPosition);
                float angle = Vector3.Angle(playerToEnemy, playerToObstacle);

                if (angle < 90f)
                {
                    float distanceToObstacle = Vector3.Distance(enemyPosition, obstacle.transform.position);
                    if (distanceToObstacle <= flankDistanceThreshold) 
                    {
                        if (distancePlayerToObstacle > distancePlayerToEnemy)
                        {
                            SetFlanking(true);
                            isFlanked = true;
                            break;
                        }
                    }
                }
            }
        }

        if (!isFlanked)
        {
            SetFlanking(false);
        }
    }

    public bool GetFlanked()
    {
        return flanked;
    }

    public void SetFlanking(bool isFlanked)
    {
        flanked = isFlanked;
    }
}
