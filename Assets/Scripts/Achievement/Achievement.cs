using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AchievementCreator", menuName = "Achievement")]
public class Achievement : ScriptableObject
{
    public new string name;
    public string description;
    public Sprite image;
    public bool unlocked;
}

[System.Serializable]
public class AchievementUnlocked
{
    public List<AchievementUnlockedData> achievementUnlocked = new();
    public int itemCollected;
    public int rareItemCollected;
}

[System.Serializable]
public class AchievementUnlockedData
{
    public string name;
    public bool unlocked;

    public AchievementUnlockedData(string name, bool unlocked)
    {
        this.name = name;
        this.unlocked = unlocked;
    }
}
