using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AchievementSaveManager : MonoBehaviour
{
    private static AchievementSaveManager instance;
    private string achievementSaveFileName = "achievements.json";
    [SerializeField] public List<Achievement> achievementList;
    private AchievementUnlocked unlockedAchievements;

    public const string FIRST_COLLECT = "First Collect";
    public const string FIRST_OIL = "First Oil";
    public const string HOARDER = "Hoarder";
    public const int ITEM_TO_COLLECT = 5;
    public const string FIRST_LEVEL_COMPLETED = "First level completed";
    public const string NO_LIFE_LOST = "No life lost";
    public const string RARE_ITEM_COLLECTOR = "Rare item collector";
    public const int RARE_ITEM_TO_COLLECT = 10;
    public const string VICTORY = "Victory";

    public static int itemCollected = 0;
    public static int rareItemCollected = 0;
    public static bool lostLife = false;


    public static AchievementSaveManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindAnyObjectByType<AchievementSaveManager>();
                if (instance == null)
                {
                    GameObject obj = new GameObject("AchievementManager");
                    instance = obj.AddComponent<AchievementSaveManager>();
                }
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject); 
        }
        else
        {
            Destroy(gameObject); 
        }
        unlockedAchievements = new AchievementUnlocked();
        LoadUnlockedAchievements();
    }

    public void UnlockAchievement(string achievementName)
    {
        if (FindAnyObjectByType<TutorialManager>() == null)
        {
            AchievementUnlockedData achievement = unlockedAchievements.achievementUnlocked.Find(a => a.name == achievementName);
            if (achievement == null)
            {
                AchievementUI uI = FindAnyObjectByType<AchievementUI>();
                uI.ShowAchievement(achievementName);
                achievement = new AchievementUnlockedData(achievementName, true);
                unlockedAchievements.achievementUnlocked.Add(achievement);
                GetAchievementByName(achievementName).unlocked = true;
                SaveUnlockedAchievements();
            }
        }
    }

    public void SaveUnlockedAchievements()
    {
        string saveFilePath = Path.Combine(Application.persistentDataPath, achievementSaveFileName);
        unlockedAchievements.itemCollected = itemCollected;
        unlockedAchievements.rareItemCollected = rareItemCollected;
        string json = JsonUtility.ToJson(unlockedAchievements);
        File.WriteAllText(saveFilePath, json);
    }

    public void LoadUnlockedAchievements()
    {
        string saveFilePath = Path.Combine(Application.persistentDataPath, achievementSaveFileName);
        if (File.Exists(saveFilePath))
        {
            string json = File.ReadAllText(saveFilePath);
            unlockedAchievements = JsonUtility.FromJson<AchievementUnlocked>(json);
            itemCollected = unlockedAchievements.itemCollected;
            rareItemCollected = unlockedAchievements.rareItemCollected;
            foreach(AchievementUnlockedData data in unlockedAchievements.achievementUnlocked)
            {
                GetAchievementByName(data.name).unlocked = data.unlocked;
            }
        }
        else
        {
            Debug.LogWarning("Achievement save file not found: " + saveFilePath);
        }
    }

    public bool IsAchievementUnlocked(string achievementName)
    {
        foreach (AchievementUnlockedData achievement in unlockedAchievements.achievementUnlocked)
        {
            if (achievement.name == achievementName && achievement.unlocked)
            {
                return true;
            }
        }
        return false;
    }

    public Achievement GetAchievementByName(string name)
    {
        foreach (Achievement achievement in achievementList)
        {
            if (achievement.name == name)
            {
                return achievement;
            }
        }
        return null;
    }
}
