using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShowAchievementInfo : MonoBehaviour
{
    [SerializeField] private Achievement achievement;
    [SerializeField] private TextMeshProUGUI nameTxt;
    [SerializeField] private TextMeshProUGUI descTxt;
    [SerializeField] private GameObject image;

    void Start()
    {
        nameTxt.SetText(achievement.name);
        descTxt.SetText(achievement.description);
        image.GetComponent<Image>().sprite = achievement.image;
        if (!achievement.unlocked)
        {
            GetComponent<Image>().color = Color.red;
        }
        else
        {
            GetComponent<Image>().color = Color.green;
        }
    }
}
