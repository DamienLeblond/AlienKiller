// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-03-05 11:15

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Scenes
    {
        public const string DemoScene = "DemoScene";
        // "Défaite" is invalid.
        public const string Feature_HitChance = "Feature_HitChance";
        public const string Level1 = "Level1";
        // "Level1 - Dominic" is invalid.
        public const string Level3 = "Level3";
        public const string MainMenu = "MainMenu";
        public const string MenuStatsAchievements = "MenuStatsAchievements";
        public const string PrototypeCamera = "PrototypeCamera";
        // "SceneDetection des murs" is invalid.
        public const string Scene_BSP = "Scene_BSP";
        public const string Scene_CircularCity = "Scene_CircularCity";
        public const string Scene_FloorPlan = "Scene_FloorPlan";
        public const string Scene_Grid = "Scene_Grid";
        public const string Scene_GridFlow = "Scene_GridFlow";
        public const string Scene_InfinityCaves = "Scene_InfinityCaves";
        public const string Scene_Isaac = "Scene_Isaac";
        public const string Scene_Level3 = "Scene_Level3";
        public const string Scene_Mario = "Scene_Mario";
        public const string Scene_Maze = "Scene_Maze";
        public const string Scene_SimpleCity = "Scene_SimpleCity";
        public const string Scene_Snap = "Scene_Snap";
        public const string Scene_SnapGridFlow = "Scene_SnapGridFlow";
        public const string ShootingPrototype = "ShootingPrototype";
        public const string TestAnim = "TestAnim";
        public const string Tutorial = "Tutorial";
        public const string Victoire = "Victoire";
        public const string lvl1DamienUI = "lvl1DamienUI";
        // "outpost on desert" is invalid.
        // "outpost with snow" is invalid.
        
    }
}