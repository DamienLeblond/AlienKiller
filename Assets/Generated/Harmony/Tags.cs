// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-03-05 11:15

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Tags
    {
        public const string ActionBarImage = "ActionBarImage";
        public const string Barrel = "Barrel";
        public const string Door = "Door";
        public const string EditorOnly = "EditorOnly";
        public const string Enemy = "Enemy";
        public const string Finish = "Finish";
        public const string FogPlane = "FogPlane";
        public const string GameController = "GameController";
        public const string GameManager = "GameManager";
        public const string HitChanceText = "HitChanceText";
        public const string MainCamera = "MainCamera";
        public const string MainCharacter = "MainCharacter";
        public const string MainController = "MainController";
        public const string Normal = "Normal";
        public const string ObjectImage = "ObjectImage";
        public const string Obstacle = "Obstacle";
        public const string Player = "Player";
        public const string Respawn = "Respawn";
        public const string Room = "Room";
        public const string TargetIcon = "TargetIcon";
        public const string Treasure = "Treasure";
        public const string Untagged = "Untagged";
        public const string Weapon = "Weapon";
        public const string WeaponButtonTag = "WeaponButtonTag";
        public const string WeaponHud = "WeaponHud";
        
    }
}