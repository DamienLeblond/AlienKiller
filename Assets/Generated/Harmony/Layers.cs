// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-03-05 11:15

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Layers
    {
        public static readonly Layer Default = new Layer(LayerMask.NameToLayer("Default"));
        public static readonly Layer Enemy = new Layer(LayerMask.NameToLayer("Enemy"));
        public static readonly Layer Explosive = new Layer(LayerMask.NameToLayer("Explosive"));
        // "Ignore Raycast" is invalid.
        public static readonly Layer Interaction = new Layer(LayerMask.NameToLayer("Interaction"));
        public static readonly Layer Obstacle = new Layer(LayerMask.NameToLayer("Obstacle"));
        public static readonly Layer Player = new Layer(LayerMask.NameToLayer("Player"));
        public static readonly Layer Projectile = new Layer(LayerMask.NameToLayer("Projectile"));
        public static readonly Layer Spawn = new Layer(LayerMask.NameToLayer("Spawn"));
        public static readonly Layer Terrain = new Layer(LayerMask.NameToLayer("Terrain"));
        public static readonly Layer TransparentFX = new Layer(LayerMask.NameToLayer("TransparentFX"));
        public static readonly Layer UI = new Layer(LayerMask.NameToLayer("UI"));
        public static readonly Layer Walls = new Layer(LayerMask.NameToLayer("Walls"));
        public static readonly Layer Water = new Layer(LayerMask.NameToLayer("Water"));
        
        public struct Layer
        {
            public int Index;
            public int Mask;

            public Layer(int index)
            {
                this.Index = index;
                this.Mask = 1 << index;
            }
        }

    }
}