// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-03-05 11:15

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class InputActions
    {
        public const string Click = "Click";
        public const string Exit = "Exit";
        public const string Move = "Move";
        public const string Navigate = "Navigate";
        public const string Point = "Point";
        public const string Rotate = "Rotate";
        public const string Run = "Run";
        public const string SpeedDown = "SpeedDown";
        public const string SpeedUp = "SpeedUp";
        public const string Submit = "Submit";
        public const string Zoom = "Zoom";
        
    }
}