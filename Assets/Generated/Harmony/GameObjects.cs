// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-03-05 11:15

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class GameObjects
    {
        // "2wall" is invalid.
        // "2wall (1)" is invalid.
        // "2wall (2)" is invalid.
        // "2wall (3)" is invalid.
        // "2wall (4)" is invalid.
        // "2wall (5)" is invalid.
        // "2wall (6)" is invalid.
        // "2wall (7)" is invalid.
        // "2wall (8)" is invalid.
        // "2wall (9)" is invalid.
        // "4Floor" is invalid.
        // "4Floor (1)" is invalid.
        // "4Floor (10)" is invalid.
        // "4Floor (11)" is invalid.
        // "4Floor (12)" is invalid.
        // "4Floor (13)" is invalid.
        // "4Floor (14)" is invalid.
        // "4Floor (15)" is invalid.
        // "4Floor (16)" is invalid.
        // "4Floor (17)" is invalid.
        // "4Floor (18)" is invalid.
        // "4Floor (19)" is invalid.
        // "4Floor (2)" is invalid.
        // "4Floor (20)" is invalid.
        // "4Floor (21)" is invalid.
        // "4Floor (22)" is invalid.
        // "4Floor (23)" is invalid.
        // "4Floor (24)" is invalid.
        // "4Floor (3)" is invalid.
        // "4Floor (4)" is invalid.
        // "4Floor (5)" is invalid.
        // "4Floor (6)" is invalid.
        // "4Floor (7)" is invalid.
        // "4Floor (8)" is invalid.
        // "4Floor (9)" is invalid.
        public const string Abandonner = "Abandonner";
        public const string Achievement1 = "Achievement1";
        public const string Achievement2 = "Achievement2";
        public const string Achievement3 = "Achievement3";
        public const string Achievement4 = "Achievement4";
        public const string Achievement5 = "Achievement5";
        public const string Achievement6 = "Achievement6";
        public const string Achievement7 = "Achievement7";
        public const string AchievementCanvas = "AchievementCanvas";
        public const string AchievementImage = "AchievementImage";
        public const string AchievementManager = "AchievementManager";
        public const string AchievementTxt = "AchievementTxt";
        public const string AchievementUI = "AchievementUI";
        public const string AchievementsPanel = "AchievementsPanel";
        public const string ActionBarManager = "ActionBarManager";
        public const string ActionMenu = "ActionMenu";
        public const string ActionsPanel = "ActionsPanel";
        public const string AerialCameraHolder = "AerialCameraHolder";
        public const string AffectedGameObjectsIndicator = "AffectedGameObjectsIndicator";
        public const string AimMode = "AimMode";
        public const string AllySpawnBigRoom = "AllySpawnBigRoom";
        public const string AllySpawnMediumRoom = "AllySpawnMediumRoom";
        public const string AllySpawnSmallRoom = "AllySpawnSmallRoom";
        public const string Arm1 = "Arm1";
        public const string ArmPlacement_Left = "ArmPlacement_Left";
        public const string ArmPlacement_Right = "ArmPlacement_Right";
        public const string ArmPlacement_Upper = "ArmPlacement_Upper";
        public const string ArmPosition_Left = "ArmPosition_Left";
        public const string ArmPosition_Right = "ArmPosition_Right";
        public const string AssaultRiffleBulletPrefabs = "AssaultRiffleBulletPrefabs";
        public const string AssaultRifle = "AssaultRifle";
        public const string Assault_Rifle_Prefab = "Assault_Rifle_Prefab";
        public const string AttackEnemy = "AttackEnemy";
        public const string AttackManager = "AttackManager";
        public const string B_Sniper_Rifle_Body = "B_Sniper_Rifle_Body";
        public const string B_Sniper_Rifle_Detail_01 = "B_Sniper_Rifle_Detail_01";
        public const string B_Sniper_Rifle_Magazine = "B_Sniper_Rifle_Magazine";
        public const string B_Sniper_Rifle_Trigger = "B_Sniper_Rifle_Trigger";
        public const string BackPack = "BackPack";
        public const string Background = "Background";
        // "Background Decorations" is invalid.
        public const string Backpack1 = "Backpack1";
        // "Barrières" is invalid.
        public const string BaseCamera = "BaseCamera";
        public const string Battery = "Battery";
        // "Battery (1)" is invalid.
        // "Battery (2)" is invalid.
        // "Battery (3)" is invalid.
        // "Battery 1" is invalid.
        public const string Battery_big = "Battery_big";
        public const string Battery_medium = "Battery_medium";
        // "Battery_medium (1)" is invalid.
        public const string Bip001 = "Bip001";
        // "Bip001 Head" is invalid.
        // "Bip001 L Calf" is invalid.
        // "Bip001 L Clavicle" is invalid.
        // "Bip001 L Finger0" is invalid.
        // "Bip001 L Finger01" is invalid.
        // "Bip001 L Finger02" is invalid.
        // "Bip001 L Finger1" is invalid.
        // "Bip001 L Finger11" is invalid.
        // "Bip001 L Finger12" is invalid.
        // "Bip001 L Finger2" is invalid.
        // "Bip001 L Finger21" is invalid.
        // "Bip001 L Finger22" is invalid.
        // "Bip001 L Foot" is invalid.
        // "Bip001 L Forearm" is invalid.
        // "Bip001 L Hand" is invalid.
        // "Bip001 L Thigh" is invalid.
        // "Bip001 L Toe0" is invalid.
        // "Bip001 L UpperArm" is invalid.
        // "Bip001 Neck" is invalid.
        // "Bip001 Pelvis" is invalid.
        // "Bip001 R Calf" is invalid.
        // "Bip001 R Clavicle" is invalid.
        // "Bip001 R Finger0" is invalid.
        // "Bip001 R Finger01" is invalid.
        // "Bip001 R Finger02" is invalid.
        // "Bip001 R Finger1" is invalid.
        // "Bip001 R Finger11" is invalid.
        // "Bip001 R Finger12" is invalid.
        // "Bip001 R Finger2" is invalid.
        // "Bip001 R Finger21" is invalid.
        // "Bip001 R Finger22" is invalid.
        // "Bip001 R Foot" is invalid.
        // "Bip001 R Forearm" is invalid.
        // "Bip001 R Hand" is invalid.
        // "Bip001 R Thigh" is invalid.
        // "Bip001 R Toe0" is invalid.
        // "Bip001 R UpperArm" is invalid.
        // "Bip001 Spine" is invalid.
        // "Bip001 Spine1" is invalid.
        // "Bip001 Spine2" is invalid.
        public const string Blood = "Blood";
        public const string Body1 = "Body1";
        public const string BottomLeftBorder = "BottomLeftBorder";
        public const string BottomRig = "BottomRig";
        // "Bouton menu principal" is invalid.
        // "Building System" is invalid.
        public const string Bullet = "Bullet";
        public const string Button = "Button";
        // "Button (1)" is invalid.
        // "Button (2)" is invalid.
        // "Button (3)" is invalid.
        public const string ButtonMenu = "ButtonMenu";
        public const string Camera = "Camera";
        // "Camera Controllers" is invalid.
        public const string CameraEventManager = "CameraEventManager";
        public const string CameraPanel = "CameraPanel";
        public const string CameraRef = "CameraRef";
        public const string CameraTutorialCanvas = "CameraTutorialCanvas";
        public const string Canvas = "Canvas";
        public const string CanvasTestCamera = "CanvasTestCamera";
        public const string Capacitor = "Capacitor";
        // "Capacitor 1" is invalid.
        public const string Capsule = "Capsule";
        // "Capsule (1)" is invalid.
        // "Capsule (2)" is invalid.
        // "Capsule (3)" is invalid.
        // "Capsule (4)" is invalid.
        // "Capsule (5)" is invalid.
        // "Capsule (6)" is invalid.
        // "Capsule (7)" is invalid.
        // "Capsule (8)" is invalid.
        // "Capsule (9)" is invalid.
        // "Chance toucher" is invalid.
        public const string Character = "Character";
        // "Character (1)" is invalid.
        public const string CharacterHealtBar = "CharacterHealtBar";
        public const string CharacterPanel = "CharacterPanel";
        public const string CharacterPanel1 = "CharacterPanel1";
        public const string CharacterPanel2 = "CharacterPanel2";
        public const string CharacterPanel3 = "CharacterPanel3";
        public const string CharacterPanel4 = "CharacterPanel4";
        public const string CharacterPortraitManager = "CharacterPortraitManager";
        public const string CharacterPositionIndicator = "CharacterPositionIndicator";
        public const string CharacterPositionIndicatorParent = "CharacterPositionIndicatorParent";
        public const string CharacterStatsText = "CharacterStatsText";
        public const string Character_DamageDealt = "Character_DamageDealt";
        public const string Character_DomageReceived = "Character_DomageReceived";
        public const string Character_HigestDamage = "Character_HigestDamage";
        public const string Character_NbCrit = "Character_NbCrit";
        public const string Character_NbKill = "Character_NbKill";
        public const string Character_NbPv = "Character_NbPv";
        public const string Character_NbTir = "Character_NbTir";
        // "Character_Précision" is invalid.
        public const string Characters = "Characters";
        public const string Charger = "Charger";
        public const string Chest = "Chest";
        // "ClearShot Camera" is invalid.
        public const string Close = "Close";
        public const string CollectableObjects = "CollectableObjects";
        public const string Confirmation = "Confirmation";
        public const string Container = "Container";
        public const string Container_Cover = "Container_Cover";
        public const string Content = "Content";
        public const string Controller = "Controller";
        public const string Corridor_1 = "Corridor_1";
        public const string Corridor_2 = "Corridor_2";
        public const string Corridor_3 = "Corridor_3";
        public const string Corridor_I = "Corridor_I";
        public const string Corridor_L = "Corridor_L";
        // "Corridor_L (1)" is invalid.
        // "Corridor_L (2)" is invalid.
        public const string Corridor_T = "Corridor_T";
        public const string Corridor_X = "Corridor_X";
        public const string CoursorIndicator = "CoursorIndicator";
        public const string CoursorindicatorParent = "CoursorindicatorParent";
        public const string CreateTargetIcon = "CreateTargetIcon";
        public const string Cube = "Cube";
        // "Cube (1)" is invalid.
        // "Cube (2)" is invalid.
        // "Cube (3)" is invalid.
        // "Cube (4)" is invalid.
        // "Cube (5)" is invalid.
        // "Cube (6)" is invalid.
        // "Cube (7)" is invalid.
        // "Cube (8)" is invalid.
        // "Cube (9)" is invalid.
        public const string Cube_1x1_extended = "Cube_1x1_extended";
        public const string Cube_1x2_extended = "Cube_1x2_extended";
        public const string Cube_2x2_extended = "Cube_2x2_extended";
        public const string CurrentTurnCanvas = "CurrentTurnCanvas";
        public const string Cylinder_16_extended = "Cylinder_16_extended";
        public const string DamageDealt = "DamageDealt";
        public const string Description = "Description";
        // "Directional Light" is invalid.
        // "Directional light" is invalid.
        public const string DomageReceived = "DomageReceived";
        public const string Door = "Door";
        public const string Down = "Down";
        public const string Down_end = "Down_end";
        public const string DungeonCircularCity = "DungeonCircularCity";
        public const string DungeonFloorPlanner = "DungeonFloorPlanner";
        public const string DungeonGrid = "DungeonGrid";
        public const string DungeonGrid2D = "DungeonGrid2D";
        public const string DungeonGridFlow = "DungeonGridFlow";
        public const string DungeonInfiniteCaves = "DungeonInfiniteCaves";
        public const string DungeonIsaac = "DungeonIsaac";
        public const string DungeonIsaac2D = "DungeonIsaac2D";
        public const string DungeonItems = "DungeonItems";
        public const string DungeonMario = "DungeonMario";
        public const string DungeonMaze = "DungeonMaze";
        public const string DungeonMergedMeshes = "DungeonMergedMeshes";
        public const string DungeonRoom = "DungeonRoom";
        public const string DungeonSimpleCity = "DungeonSimpleCity";
        public const string DungeonSnap = "DungeonSnap";
        public const string DungeonSnapGridFlow = "DungeonSnapGridFlow";
        public const string DungeonSnapSideScroller = "DungeonSnapSideScroller";
        public const string DungeonSpatialPartition = "DungeonSpatialPartition";
        // "Défaite (Écran de mort)_0" is invalid.
        // "Défaite Texte" is invalid.
        public const string Elevator = "Elevator";
        public const string EndGameCanvas = "EndGameCanvas";
        public const string EndText = "EndText";
        public const string Enemies = "Enemies";
        public const string Enemy = "Enemy";
        // "Enemy (1)" is invalid.
        // "Enemy (2)" is invalid.
        // "Enemy (3)" is invalid.
        // "Enemy (4)" is invalid.
        // "Enemy (5)" is invalid.
        public const string EnnemiAttack = "EnnemiAttack";
        public const string EnnemiDeath = "EnnemiDeath";
        public const string EnnemiReveal = "EnnemiReveal";
        public const string EnnemiTurnCanvas = "EnnemiTurnCanvas";
        public const string EnnemyHealthBar = "EnnemyHealthBar";
        public const string EventSystem = "EventSystem";
        public const string ExpandPanel = "ExpandPanel";
        public const string Explosion = "Explosion";
        public const string FogOfWar = "FogOfWar";
        public const string FogOfWarParent = "FogOfWarParent";
        public const string Foot_Left = "Foot_Left";
        public const string Foot_Right = "Foot_Right";
        // "FreeLook Camera" is invalid.
        public const string GameManager = "GameManager";
        public const string GameMangaer = "GameMangaer";
        public const string GameObject = "GameObject";
        public const string Gameplay = "Gameplay";
        // "Gameplay - Dom" is invalid.
        public const string GameplayBen = "GameplayBen";
        public const string Grid = "Grid";
        public const string GridParent = "GridParent";
        public const string GridPlane = "GridPlane";
        // "HDRP Compositor" is invalid.
        public const string HPCharacter = "HPCharacter";
        public const string Hand_Left = "Hand_Left";
        public const string Hand_Right = "Hand_Right";
        public const string Handle = "Handle";
        public const string Head = "Head";
        public const string Headgear_Left = "Headgear_Left";
        public const string Headgear_Right = "Headgear_Right";
        public const string HealtBarCanvas = "HealtBarCanvas";
        public const string HealtBarRect = "HealtBarRect";
        public const string HealthBar = "HealthBar";
        public const string HigestDamage = "HigestDamage";
        public const string Hips = "Hips";
        public const string HoverItemCanvas = "HoverItemCanvas";
        public const string Icon = "Icon";
        public const string IconsTutorialCanvas = "IconsTutorialCanvas";
        public const string Image = "Image";
        // "Image (1)" is invalid.
        public const string Index_Distal_Left = "Index_Distal_Left";
        public const string Index_Distal_Right = "Index_Distal_Right";
        public const string Index_Intermediate_Left = "Index_Intermediate_Left";
        public const string Index_Intermediate_Right = "Index_Intermediate_Right";
        public const string Index_Proximal_Left = "Index_Proximal_Left";
        public const string Index_Proximal_Right = "Index_Proximal_Right";
        public const string InputManager = "InputManager";
        public const string InstructionDetails = "InstructionDetails";
        public const string Interactions = "Interactions";
        public const string Item = "Item";
        // "Item Background" is invalid.
        // "Item Checkmark" is invalid.
        // "Item Label" is invalid.
        public const string ItemPanel = "ItemPanel";
        public const string Joint_T_6 = "Joint_T_6";
        public const string Joint_X_6 = "Joint_X_6";
        public const string Joint_Y_6 = "Joint_Y_6";
        public const string LayoutVisualizerPlane = "LayoutVisualizerPlane";
        public const string Leg1 = "Leg1";
        public const string LevelInformation = "LevelInformation";
        public const string LevelMidPoint = "LevelMidPoint";
        public const string LightProbeOverride = "LightProbeOverride";
        public const string LoadOut = "LoadOut";
        public const string LongSas = "LongSas";
        public const string LowerArm_Left = "LowerArm_Left";
        public const string LowerArm_Right = "LowerArm_Right";
        public const string LowerLeg_Left = "LowerLeg_Left";
        public const string LowerLeg_Right = "LowerLeg_Right";
        // "Main Camera" is invalid.
        public const string Map = "Map";
        public const string MarioPlayerPrefab = "MarioPlayerPrefab";
        public const string MarkerReplaceVolume = "MarkerReplaceVolume";
        public const string MeleeAttackTutorialCanvas = "MeleeAttackTutorialCanvas";
        public const string Menu = "Menu";
        // "Menu (1)" is invalid.
        // "Metal Mind" is invalid.
        public const string MiddleRig = "MiddleRig";
        public const string MirrorVolume = "MirrorVolume";
        public const string Module_2x2 = "Module_2x2";
        // "Module_2x2 1" is invalid.
        public const string Module_BigRoom = "Module_BigRoom";
        public const string Module_Boss = "Module_Boss";
        public const string Module_Corridor2 = "Module_Corridor2";
        // "Module_Corridor2 1" is invalid.
        public const string Module_Exit = "Module_Exit";
        public const string Module_Medium = "Module_Medium";
        public const string Module_SpawnRoom = "Module_SpawnRoom";
        public const string Module_Treasure = "Module_Treasure";
        public const string Move = "Move";
        public const string MovementCanvas = "MovementCanvas";
        public const string Name = "Name";
        public const string Navigation2D = "Navigation2D";
        public const string NavigationCity = "NavigationCity";
        public const string NavigationGrid = "NavigationGrid";
        public const string NbCrit = "NbCrit";
        public const string NbKill = "NbKill";
        public const string NbPv = "NbPv";
        public const string NbTir = "NbTir";
        public const string Neck = "Neck";
        public const string NegationVolume = "NegationVolume";
        public const string NegationVolume2D = "NegationVolume2D";
        // "New Game Object" is invalid.
        public const string NewProjectile = "NewProjectile";
        public const string Next = "Next";
        public const string Non = "Non";
        public const string NormalRobotIcon = "NormalRobotIcon";
        public const string NouvellePartie = "NouvellePartie";
        public const string ObjectDescriptionText = "ObjectDescriptionText";
        public const string ObjectImage = "ObjectImage";
        public const string ObjectUIController = "ObjectUIController";
        public const string Oil = "Oil";
        public const string OnAirCameraManager = "OnAirCameraManager";
        public const string Open = "Open";
        public const string OpenTreasure = "OpenTreasure";
        public const string OpenTreasureTutorialCanvas = "OpenTreasureTutorialCanvas";
        public const string Oui = "Oui";
        public const string PBRCharacter = "PBRCharacter";
        public const string P_Barrel = "P_Barrel";
        // "P_Barrel (1)" is invalid.
        // "P_Barrel (2)" is invalid.
        // "P_Barrel (3)" is invalid.
        public const string P_Barrier_01_Corner_L = "P_Barrier_01_Corner_L";
        public const string P_Barrier_01_Corner_L_Mirror = "P_Barrier_01_Corner_L_Mirror";
        public const string P_Barrier_01_Corner_Long = "P_Barrier_01_Corner_Long";
        public const string P_Barrier_01_Corner_Short = "P_Barrier_01_Corner_Short";
        public const string P_Barrier_01_Long = "P_Barrier_01_Long";
        public const string P_Barrier_01_Short = "P_Barrier_01_Short";
        public const string P_Barrier_01_Stairs = "P_Barrier_01_Stairs";
        public const string P_Barrier_02 = "P_Barrier_02";
        // "P_Barrier_02 (1)" is invalid.
        // "P_Barrier_02 (10)" is invalid.
        // "P_Barrier_02 (11)" is invalid.
        // "P_Barrier_02 (12)" is invalid.
        // "P_Barrier_02 (13)" is invalid.
        // "P_Barrier_02 (14)" is invalid.
        // "P_Barrier_02 (15)" is invalid.
        // "P_Barrier_02 (16)" is invalid.
        // "P_Barrier_02 (17)" is invalid.
        // "P_Barrier_02 (18)" is invalid.
        // "P_Barrier_02 (19)" is invalid.
        // "P_Barrier_02 (2)" is invalid.
        // "P_Barrier_02 (3)" is invalid.
        // "P_Barrier_02 (4)" is invalid.
        // "P_Barrier_02 (5)" is invalid.
        // "P_Barrier_02 (6)" is invalid.
        // "P_Barrier_02 (7)" is invalid.
        // "P_Barrier_02 (8)" is invalid.
        // "P_Barrier_02 (9)" is invalid.
        public const string P_Big_Pipes_1m_01 = "P_Big_Pipes_1m_01";
        // "P_Big_Pipes_1m_01 (1)" is invalid.
        // "P_Big_Pipes_1m_01 (2)" is invalid.
        // "P_Big_Pipes_1m_01 (3)" is invalid.
        public const string P_Big_Pipes_1m_02 = "P_Big_Pipes_1m_02";
        public const string P_Big_Pipes_2m = "P_Big_Pipes_2m";
        // "P_Big_Pipes_2m (1)" is invalid.
        // "P_Big_Pipes_2m (2)" is invalid.
        // "P_Big_Pipes_2m (3)" is invalid.
        public const string P_Big_Pipes_connector = "P_Big_Pipes_connector";
        public const string P_Big_Pipes_corner = "P_Big_Pipes_corner";
        // "P_Big_Pipes_corner (1)" is invalid.
        public const string P_Big_Pipes_intersection = "P_Big_Pipes_intersection";
        public const string P_Big_Pipes_slant = "P_Big_Pipes_slant";
        public const string P_Box2 = "P_Box2";
        public const string P_Cabinet_01_Body = "P_Cabinet_01_Body";
        // "P_Cabinet_01_Body (1)" is invalid.
        // "P_Cabinet_01_Body (10)" is invalid.
        // "P_Cabinet_01_Body (11)" is invalid.
        // "P_Cabinet_01_Body (12)" is invalid.
        // "P_Cabinet_01_Body (13)" is invalid.
        // "P_Cabinet_01_Body (14)" is invalid.
        // "P_Cabinet_01_Body (15)" is invalid.
        // "P_Cabinet_01_Body (2)" is invalid.
        // "P_Cabinet_01_Body (3)" is invalid.
        // "P_Cabinet_01_Body (4)" is invalid.
        // "P_Cabinet_01_Body (5)" is invalid.
        // "P_Cabinet_01_Body (6)" is invalid.
        // "P_Cabinet_01_Body (7)" is invalid.
        // "P_Cabinet_01_Body (8)" is invalid.
        // "P_Cabinet_01_Body (9)" is invalid.
        public const string P_CeilingPipes_01 = "P_CeilingPipes_01";
        public const string P_CeilingPipes_01_Corner = "P_CeilingPipes_01_Corner";
        public const string P_CeilingPipes_01_Corner_2 = "P_CeilingPipes_01_Corner_2";
        public const string P_Ceiling_01 = "P_Ceiling_01";
        public const string P_Ceiling_01_Corner = "P_Ceiling_01_Corner";
        public const string P_Ceiling_01_T = "P_Ceiling_01_T";
        public const string P_Ceiling_01_X = "P_Ceiling_01_X";
        public const string P_Ceiling_02 = "P_Ceiling_02";
        public const string P_Chair_01 = "P_Chair_01";
        public const string P_Connector_01 = "P_Connector_01";
        public const string P_Container_01 = "P_Container_01";
        public const string P_Desk_01 = "P_Desk_01";
        public const string P_Details_Hatch_01 = "P_Details_Hatch_01";
        public const string P_Details_Lamp_Small = "P_Details_Lamp_Small";
        public const string P_Details_Lamp_Wide = "P_Details_Lamp_Wide";
        public const string P_Details_Vent_03 = "P_Details_Vent_03";
        public const string P_Details_Wall_Case = "P_Details_Wall_Case";
        public const string P_Device_02 = "P_Device_02";
        public const string P_Device_03 = "P_Device_03";
        public const string P_Device_04 = "P_Device_04";
        public const string P_Device_05 = "P_Device_05";
        public const string P_Extinguisher = "P_Extinguisher";
        public const string P_Extinguisher_Wall = "P_Extinguisher_Wall";
        public const string P_Floor_01 = "P_Floor_01";
        // "P_Floor_01 (1)" is invalid.
        // "P_Floor_01 (10)" is invalid.
        // "P_Floor_01 (11)" is invalid.
        // "P_Floor_01 (12)" is invalid.
        // "P_Floor_01 (13)" is invalid.
        // "P_Floor_01 (2)" is invalid.
        // "P_Floor_01 (3)" is invalid.
        // "P_Floor_01 (4)" is invalid.
        // "P_Floor_01 (5)" is invalid.
        // "P_Floor_01 (6)" is invalid.
        // "P_Floor_01 (7)" is invalid.
        // "P_Floor_01 (8)" is invalid.
        // "P_Floor_01 (9)" is invalid.
        public const string P_Floor_02 = "P_Floor_02";
        public const string P_Floor_02_corner = "P_Floor_02_corner";
        public const string P_Floor_03 = "P_Floor_03";
        public const string P_Floor_04 = "P_Floor_04";
        public const string P_Floor_05 = "P_Floor_05";
        public const string P_Floor_2x1 = "P_Floor_2x1";
        public const string P_Floor_4x1 = "P_Floor_4x1";
        public const string P_Floor_4x2 = "P_Floor_4x2";
        public const string P_Floor_Gate_01 = "P_Floor_Gate_01";
        public const string P_Forklift = "P_Forklift";
        public const string P_Gate = "P_Gate";
        public const string P_Lamp_01 = "P_Lamp_01";
        public const string P_Leveled_Platform_2x2x2 = "P_Leveled_Platform_2x2x2";
        public const string P_Leveled_Platform_2x2x4 = "P_Leveled_Platform_2x2x4";
        public const string P_Leveled_Platform_2x4x2 = "P_Leveled_Platform_2x4x2";
        public const string P_Leveled_Platform_2x4x4 = "P_Leveled_Platform_2x4x4";
        public const string P_Mobile_Device_01 = "P_Mobile_Device_01";
        public const string P_Pallet_01 = "P_Pallet_01";
        public const string P_Pillar = "P_Pillar";
        // "P_Pillar (1)" is invalid.
        // "P_Pillar (2)" is invalid.
        // "P_Pillar (3)" is invalid.
        // "P_Pillar (4)" is invalid.
        public const string P_Pillar_03 = "P_Pillar_03";
        // "P_Pillar_03 (1)" is invalid.
        public const string P_Pillar_2 = "P_Pillar_2";
        public const string P_Platform_01 = "P_Platform_01";
        public const string P_Platform_02 = "P_Platform_02";
        public const string P_Platform_02_Hole = "P_Platform_02_Hole";
        public const string P_Screen_01 = "P_Screen_01";
        public const string P_SimpleOccluder_4x1 = "P_SimpleOccluder_4x1";
        public const string P_Slant_01 = "P_Slant_01";
        public const string P_Slant_01_Corner = "P_Slant_01_Corner";
        public const string P_Slant_01_Detail = "P_Slant_01_Detail";
        public const string P_Slant_01_Window = "P_Slant_01_Window";
        public const string P_Small_Pipe = "P_Small_Pipe";
        public const string P_Small_Pipe_End1 = "P_Small_Pipe_End1";
        public const string P_Small_Pipe_End2 = "P_Small_Pipe_End2";
        public const string P_Small_Pipe_Intersection1 = "P_Small_Pipe_Intersection1";
        public const string P_Small_Pipe_Intersection2 = "P_Small_Pipe_Intersection2";
        public const string P_Small_Pipe_Long = "P_Small_Pipe_Long";
        public const string P_Small_Pipe_Short = "P_Small_Pipe_Short";
        public const string P_Small_Pipe_Turn1 = "P_Small_Pipe_Turn1";
        public const string P_Small_Pipe_Turn2 = "P_Small_Pipe_Turn2";
        public const string P_Small_Pipe_Turn3 = "P_Small_Pipe_Turn3";
        public const string P_Sniper_Rifle_Body = "P_Sniper_Rifle_Body";
        public const string P_Sniper_Rifle_Detail_01 = "P_Sniper_Rifle_Detail_01";
        public const string P_Sniper_Rifle_Magazine = "P_Sniper_Rifle_Magazine";
        public const string P_Sniper_Rifle_Trigger = "P_Sniper_Rifle_Trigger";
        public const string P_Stairs_01 = "P_Stairs_01";
        public const string P_Stairs_02 = "P_Stairs_02";
        public const string P_Table_Screen_table = "P_Table_Screen_table";
        // "P_Table_Screen_table (1)" is invalid.
        // "P_Table_Screen_table (2)" is invalid.
        // "P_Table_Screen_table (3)" is invalid.
        public const string P_Table_Screen_table_big = "P_Table_Screen_table_big";
        // "P_Table_Screen_table_big (1)" is invalid.
        public const string P_Tarpaulin = "P_Tarpaulin";
        public const string P_Turbine_01 = "P_Turbine_01";
        // "P_Turbine_01 (1)" is invalid.
        public const string P_Vent_01 = "P_Vent_01";
        public const string P_Vent_02 = "P_Vent_02";
        public const string P_Wall_01 = "P_Wall_01";
        public const string P_Wall_01_ConnectorHole = "P_Wall_01_ConnectorHole";
        public const string P_Wall_01_Corner = "P_Wall_01_Corner";
        public const string P_Wall_01_Corner_2 = "P_Wall_01_Corner_2";
        public const string P_Wall_01_GateHole = "P_Wall_01_GateHole";
        public const string P_Wall_02_01 = "P_Wall_02_01";
        public const string P_Wall_02_02 = "P_Wall_02_02";
        public const string P_Wall_02_03 = "P_Wall_02_03";
        public const string P_Wall_02_03_Short = "P_Wall_02_03_Short";
        public const string P_Wall_02_GateHole = "P_Wall_02_GateHole";
        public const string P_Wall_03_01 = "P_Wall_03_01";
        public const string P_Wall_03_02 = "P_Wall_03_02";
        public const string P_Wall_03_Corner = "P_Wall_03_Corner";
        public const string P_Wall_03_GateHole = "P_Wall_03_GateHole";
        public const string P_Wall_04_01 = "P_Wall_04_01";
        public const string P_Wall_04_02 = "P_Wall_04_02";
        public const string P_Wall_04_Short = "P_Wall_04_Short";
        public const string P_Wall_04_ledge = "P_Wall_04_ledge";
        public const string P_Wall_04_ledge_corner = "P_Wall_04_ledge_corner";
        public const string P_Wall_04_ledge_half = "P_Wall_04_ledge_half";
        public const string P_Wall_04_ledge_up = "P_Wall_04_ledge_up";
        public const string P_Wall_04_ledge_up_corner = "P_Wall_04_ledge_up_corner";
        public const string P_Wall_04_ledge_up_half = "P_Wall_04_ledge_up_half";
        public const string P_Wall_04_long = "P_Wall_04_long";
        public const string P_Wall_04_long_slant = "P_Wall_04_long_slant";
        public const string P_Wall_04_slant_01 = "P_Wall_04_slant_01";
        public const string P_Wall_04_slant_02 = "P_Wall_04_slant_02";
        public const string P_Wall_04_triangle = "P_Wall_04_triangle";
        public const string P_Wall_Device_01 = "P_Wall_Device_01";
        public const string PaintMode = "PaintMode";
        public const string Panel = "Panel";
        public const string PanelLostHealth = "PanelLostHealth";
        public const string PanelPossibleDamage = "PanelPossibleDamage";
        public const string PanelRemainingHealth = "PanelRemainingHealth";
        // "Particle System" is invalid.
        // "Particle System (1)" is invalid.
        public const string Particule = "Particule";
        public const string PassTurn = "PassTurn";
        // "PasserelleAlternate1 Variant" is invalid.
        // "PasserelleAlternate2 Variant" is invalid.
        public const string PasserelleAlternate3 = "PasserelleAlternate3";
        // "PasserelleAlternate3 Variant" is invalid.
        // "PasserelleAlternate4 Variant" is invalid.
        public const string PasserelleBase = "PasserelleBase";
        public const string PasserelleStairSameWay = "PasserelleStairSameWay";
        public const string PauseMenu = "PauseMenu";
        public const string PauseMenuManager = "PauseMenuManager";
        public const string Pillar = "Pillar";
        public const string Pixel_box_02 = "Pixel_box_02";
        // "Pixel_box_02 (1)" is invalid.
        // "Pixel_box_02 (2)" is invalid.
        // "Pixel_box_02 (3)" is invalid.
        public const string Pixel_box_02_Armature = "Pixel_box_02_Armature";
        public const string PlacementSystem = "PlacementSystem";
        public const string Plane = "Plane";
        // "Plane (1)" is invalid.
        // "Plane (2)" is invalid.
        // "Plane (3)" is invalid.
        public const string PlatformVolume = "PlatformVolume";
        public const string PlatformVolume2D = "PlatformVolume2D";
        public const string PlayableCharacterSpawner1 = "PlayableCharacterSpawner1";
        public const string PlayableCharacterSpawner2 = "PlayableCharacterSpawner2";
        public const string PlayerActionInitiator = "PlayerActionInitiator";
        public const string PlayerCamera = "PlayerCamera";
        public const string PlayerTurnCanvas = "PlayerTurnCanvas";
        // "Player_(HIT PLAY AND DRAG ME AROUND)" is invalid.
        // "Point light" is invalid.
        // "Point light (1)" is invalid.
        public const string PolyartCharacter = "PolyartCharacter";
        // "Post-process Volume" is invalid.
        public const string PrefabHitChance = "PrefabHitChance";
        public const string Projectile = "Projectile";
        public const string ProjectileManager = "ProjectileManager";
        // "Précision" is invalid.
        public const string QuitTutorialCanvas = "QuitTutorialCanvas";
        public const string Quitter = "Quitter";
        public const string Read = "Read";
        public const string RectLight = "RectLight";
        public const string RectLight1 = "RectLight1";
        // "Reflection Probe" is invalid.
        public const string Reload = "Reload";
        public const string Reprendre = "Reprendre";
        public const string RestOfFingers_Distal_Left = "RestOfFingers_Distal_Left";
        public const string RestOfFingers_Distal_Right = "RestOfFingers_Distal_Right";
        public const string RestOfFingers_Intermediate_Left = "RestOfFingers_Intermediate_Left";
        public const string RestOfFingers_Intermediate_Right = "RestOfFingers_Intermediate_Right";
        public const string RestOfFingers_Proximal_Left = "RestOfFingers_Proximal_Left";
        public const string RestOfFingers_Proximal_Right = "RestOfFingers_Proximal_Right";
        public const string Return = "Return";
        public const string ReturnToMainScreen = "ReturnToMainScreen";
        public const string RoadNetwork = "RoadNetwork";
        public const string Robot = "Robot";
        // "Robot (1)" is invalid.
        public const string RobotBlood = "RobotBlood";
        public const string Rotation = "Rotation";
        public const string Rotation_end = "Rotation_end";
        public const string SK_Soldier_Head = "SK_Soldier_Head";
        public const string SK_Soldier_Head_LOD1 = "SK_Soldier_Head_LOD1";
        public const string SK_Soldier_Legs = "SK_Soldier_Legs";
        public const string SK_Soldier_Legs_LOD1 = "SK_Soldier_Legs_LOD1";
        public const string SK_Soldier_Torso = "SK_Soldier_Torso";
        public const string SK_Soldier_Torso_LOD1 = "SK_Soldier_Torso_LOD1";
        public const string SM_Cabinet_01_Body = "SM_Cabinet_01_Body";
        public const string SM_Cabinet_01_Door = "SM_Cabinet_01_Door";
        public const string SM_Extinguisher_Handle = "SM_Extinguisher_Handle";
        public const string SM_Extinguisher_Main = "SM_Extinguisher_Main";
        public const string SM_Extinguisher_u_l = "SM_Extinguisher_u_l";
        public const string SM_Extinguisher_u_r = "SM_Extinguisher_u_r";
        public const string SM_Gate_01_Gate_01_Frame = "SM_Gate_01_Gate_01_Frame";
        public const string SM_Gate_01_Gate_01_Side = "SM_Gate_01_Gate_01_Side";
        // "SM_Gate_01_Gate_01_Side (1)" is invalid.
        public const string SM_Gate_01_Gate_01_Up = "SM_Gate_01_Gate_01_Up";
        public const string SM_Pillar_03_Lower = "SM_Pillar_03_Lower";
        public const string SM_Pillar_03_Upper = "SM_Pillar_03_Upper";
        public const string SM_Turbine_01_Turbine = "SM_Turbine_01_Turbine";
        public const string SM_Turbine_01_Turbine_002 = "SM_Turbine_01_Turbine_002";
        public const string SM_Turbine_01_Turbine_003 = "SM_Turbine_01_Turbine_003";
        public const string SauvegarderEtQuitter = "SauvegarderEtQuitter";
        public const string SauvegarderEtRevenirAuMenu = "SauvegarderEtRevenirAuMenu";
        public const string Scene = "Scene";
        public const string SceneBorder = "SceneBorder";
        public const string SceneController = "SceneController";
        public const string Sci_Fi_Character_08 = "Sci_Fi_Character_08";
        public const string Sci_Fi_Character_08_01 = "Sci_Fi_Character_08_01";
        public const string Sci_Fi_Character_08_03 = "Sci_Fi_Character_08_03";
        public const string Sci_Fi_Character_08_05 = "Sci_Fi_Character_08_05";
        public const string Sci_Fi_Character_08_Arms = "Sci_Fi_Character_08_Arms";
        // "Sci_Fi_Character_08_Arms 1" is invalid.
        public const string Sci_Fi_Character_08_Bag_01 = "Sci_Fi_Character_08_Bag_01";
        public const string Sci_Fi_Character_08_Bag_02 = "Sci_Fi_Character_08_Bag_02";
        public const string Sci_Fi_Character_08_Bag_03 = "Sci_Fi_Character_08_Bag_03";
        public const string Sci_Fi_Character_08_Bag_04 = "Sci_Fi_Character_08_Bag_04";
        public const string Sci_Fi_Character_08_Bags_Belts = "Sci_Fi_Character_08_Bags_Belts";
        public const string Sci_Fi_Character_08_Body = "Sci_Fi_Character_08_Body";
        public const string Sci_Fi_Character_08_Hat_01 = "Sci_Fi_Character_08_Hat_01";
        public const string Sci_Fi_Character_08_Hat_02 = "Sci_Fi_Character_08_Hat_02";
        public const string Sci_Fi_Character_08_Head_01 = "Sci_Fi_Character_08_Head_01";
        public const string Sci_Fi_Character_08_Head_02 = "Sci_Fi_Character_08_Head_02";
        public const string Sci_Fi_Character_08_Head_02_Details = "Sci_Fi_Character_08_Head_02_Details";
        public const string Sci_Fi_Character_08_Shoes_Details_01 = "Sci_Fi_Character_08_Shoes_Details_01";
        public const string Sci_Fi_Character_08_Shoes_Details_02 = "Sci_Fi_Character_08_Shoes_Details_02";
        public const string Scrollbar = "Scrollbar";
        // "Shield Core" is invalid.
        public const string Shoot = "Shoot";
        public const string ShootBarrel = "ShootBarrel";
        public const string ShootFX = "ShootFX";
        public const string ShootingButton = "ShootingButton";
        public const string ShootingText = "ShootingText";
        public const string Shotgun = "Shotgun";
        public const string ShotgunBulletPrefabs = "ShotgunBulletPrefabs";
        public const string ShoulderPadBlade_Left = "ShoulderPadBlade_Left";
        public const string ShoulderPadBlade_Right = "ShoulderPadBlade_Right";
        public const string ShoulderPadBody_Left = "ShoulderPadBody_Left";
        public const string ShoulderPadBody_Right = "ShoulderPadBody_Right";
        public const string ShoulderPadCTRL_Left = "ShoulderPadCTRL_Left";
        public const string ShoulderPadCTRL_Right = "ShoulderPadCTRL_Right";
        public const string Shoulder_Left = "Shoulder_Left";
        public const string Shoulder_Right = "Shoulder_Right";
        // "Sliding Area" is invalid.
        public const string SmallSas = "SmallSas";
        public const string SnapConnection = "SnapConnection";
        // "SnapConnection (1)" is invalid.
        // "SnapConnection (10)" is invalid.
        // "SnapConnection (12)" is invalid.
        // "SnapConnection (13)" is invalid.
        // "SnapConnection (14)" is invalid.
        // "SnapConnection (15)" is invalid.
        // "SnapConnection (16)" is invalid.
        // "SnapConnection (17)" is invalid.
        // "SnapConnection (18)" is invalid.
        // "SnapConnection (19)" is invalid.
        // "SnapConnection (2)" is invalid.
        // "SnapConnection (20)" is invalid.
        // "SnapConnection (21)" is invalid.
        // "SnapConnection (22)" is invalid.
        // "SnapConnection (23)" is invalid.
        // "SnapConnection (24)" is invalid.
        // "SnapConnection (3)" is invalid.
        // "SnapConnection (4)" is invalid.
        // "SnapConnection (5)" is invalid.
        // "SnapConnection (6)" is invalid.
        // "SnapConnection (7)" is invalid.
        // "SnapConnection (8)" is invalid.
        // "SnapConnection (9)" is invalid.
        public const string SnapGridFlowDebugVisualizer = "SnapGridFlowDebugVisualizer";
        public const string SniperRobot = "SniperRobot";
        public const string SniperRobotIcon = "SniperRobotIcon";
        public const string Sniper_Rifle = "Sniper_Rifle";
        public const string Sniper_Rifle_01 = "Sniper_Rifle_01";
        public const string Sniper_Rifle_Body_Mesh = "Sniper_Rifle_Body_Mesh";
        public const string Sniper_Rifle_Cloth_01_Mesh = "Sniper_Rifle_Cloth_01_Mesh";
        public const string Sniper_Rifle_Cloth_02_Mesh = "Sniper_Rifle_Cloth_02_Mesh";
        public const string Sniper_Rifle_Cloth_03_Mesh = "Sniper_Rifle_Cloth_03_Mesh";
        public const string Sniper_Rifle_Detail_01_Mesh = "Sniper_Rifle_Detail_01_Mesh";
        public const string Sniper_Rifle_Detail_02_Mesh = "Sniper_Rifle_Detail_02_Mesh";
        public const string Sniper_Rifle_Detail_03_Mesh = "Sniper_Rifle_Detail_03_Mesh";
        public const string Sniper_Rifle_Glasses_Mesh = "Sniper_Rifle_Glasses_Mesh";
        public const string Sniper_Rifle_Magazine_Mesh = "Sniper_Rifle_Magazine_Mesh";
        public const string Sniper_Rifle_Muflelr_Mesh = "Sniper_Rifle_Muflelr_Mesh";
        public const string Sniper_Rifle_Sight_Mesh = "Sniper_Rifle_Sight_Mesh";
        public const string Sniper_Rifle_Trigger_Mesh = "Sniper_Rifle_Trigger_Mesh";
        public const string Space_Soldier_A = "Space_Soldier_A";
        // "Space_Soldier_A (1)" is invalid.
        // "Space_Soldier_A (2)" is invalid.
        // "Space_Soldier_A (3)" is invalid.
        // "Space_Soldier_A (4)" is invalid.
        // "Space_Soldier_A Variant" is invalid.
        public const string Space_Soldier_A_1 = "Space_Soldier_A_1";
        public const string Space_Soldier_A_2 = "Space_Soldier_A_2";
        public const string Space_Soldier_A_3 = "Space_Soldier_A_3";
        public const string Space_Soldier_A_4 = "Space_Soldier_A_4";
        public const string Space_Soldier_A_LOD1 = "Space_Soldier_A_LOD1";
        public const string Space_Soldier_Ragdoll_A = "Space_Soldier_Ragdoll_A";
        public const string Space_Soldier_Ragdoll_A_LOD1 = "Space_Soldier_Ragdoll_A_LOD1";
        public const string Spark = "Spark";
        public const string SpawnPoint = "SpawnPoint";
        // "SpawnPoint (1)" is invalid.
        // "SpawnPoint (2)" is invalid.
        // "SpawnPoint (3)" is invalid.
        // "SpawnPoint (4)" is invalid.
        // "SpawnPoint (5)" is invalid.
        // "SpawnPoint (6)" is invalid.
        // "SpawnPoint (7)" is invalid.
        public const string SpawnPoints = "SpawnPoints";
        public const string Spawner = "Spawner";
        // "Spawner (1)" is invalid.
        // "Spawner (10)" is invalid.
        // "Spawner (11)" is invalid.
        // "Spawner (12)" is invalid.
        // "Spawner (13)" is invalid.
        // "Spawner (2)" is invalid.
        // "Spawner (3)" is invalid.
        // "Spawner (4)" is invalid.
        // "Spawner (5)" is invalid.
        // "Spawner (6)" is invalid.
        // "Spawner (7)" is invalid.
        // "Spawner (8)" is invalid.
        // "Spawner (9)" is invalid.
        public const string Spawners = "Spawners";
        // "Spawners (1)" is invalid.
        // "Spawners (2)" is invalid.
        // "Spawners (3)" is invalid.
        // "Spawners (4)" is invalid.
        // "Spawners (5)" is invalid.
        // "Spawners (6)" is invalid.
        // "Spawners (7)" is invalid.
        // "Spawners (8)" is invalid.
        // "Spawners (9)" is invalid.
        public const string Sphere = "Sphere";
        public const string Sphere_16_extended = "Sphere_16_extended";
        public const string Sphere_32_extended = "Sphere_32_extended";
        public const string Spine = "Spine";
        public const string SplatmapVisualizer = "SplatmapVisualizer";
        public const string Spotlight = "Spotlight";
        public const string Stats = "Stats";
        // "Succès et Stats" is invalid.
        public const string SwitchWeapon = "SwitchWeapon";
        public const string TakeObject = "TakeObject";
        // "Target Group" is invalid.
        public const string TargetMenu = "TargetMenu";
        public const string TargetingBarrelCanvas = "TargetingBarrelCanvas";
        public const string TargetingCanvas = "TargetingCanvas";
        public const string TargetingEnemyInformation = "TargetingEnemyInformation";
        public const string Template = "Template";
        public const string Text = "Text";
        // "Text (TMP)" is invalid.
        public const string TexteCritique = "TexteCritique";
        public const string ThemeOverrideVolume = "ThemeOverrideVolume";
        public const string ThemeOverrideVolume2D = "ThemeOverrideVolume2D";
        public const string ThirdPersonCamera = "ThirdPersonCamera";
        public const string Thumb_Distal_Left = "Thumb_Distal_Left";
        public const string Thumb_Distal_Right = "Thumb_Distal_Right";
        public const string Thumb_Intermediate_Left = "Thumb_Intermediate_Left";
        public const string Thumb_Intermediate_Right = "Thumb_Intermediate_Right";
        public const string Thumb_Proximal_Left = "Thumb_Proximal_Left";
        public const string Thumb_Proximal_Right = "Thumb_Proximal_Right";
        public const string Title = "Title";
        public const string Toe_Left = "Toe_Left";
        public const string Toe_Right = "Toe_Right";
        public const string Toetip_Left = "Toetip_Left";
        public const string Toetip_Right = "Toetip_Right";
        public const string ToolTip = "ToolTip";
        public const string TopRig = "TopRig";
        public const string TopRightBorder = "TopRightBorder";
        // "Tour ennemi" is invalid.
        public const string Trail = "Trail";
        public const string Treasure = "Treasure";
        // "Treasure (1)" is invalid.
        // "Treasure (2)" is invalid.
        // "Treasure (3)" is invalid.
        public const string TriggerToClose = "TriggerToClose";
        public const string TriggerToOpen = "TriggerToOpen";
        public const string Trigger_Right = "Trigger_Right";
        public const string TurnOrderCanvas = "TurnOrderCanvas";
        public const string TurnTxt = "TurnTxt";
        public const string TutorialCanvas = "TutorialCanvas";
        public const string TutorialManager = "TutorialManager";
        public const string TutorialTrigger = "TutorialTrigger";
        public const string Tutoriel = "Tutoriel";
        public const string UI = "UI";
        public const string UIController = "UIController";
        public const string UP = "UP";
        public const string UP_end = "UP_end";
        public const string UpperArm_Left = "UpperArm_Left";
        public const string UpperArm_Right = "UpperArm_Right";
        public const string UpperLeg_Left = "UpperLeg_Left";
        public const string UpperLeg_Right = "UpperLeg_Right";
        // "Victoire (Écran de victoire)_0" is invalid.
        public const string Viewport = "Viewport";
        // "Virtual Camera" is invalid.
        // "Virtual Camera (1)" is invalid.
        // "Visual Grid" is invalid.
        public const string Wall = "Wall";
        public const string WallBlock = "WallBlock";
        // "WallBlock (1)" is invalid.
        // "WallBlock (10)" is invalid.
        // "WallBlock (11)" is invalid.
        // "WallBlock (12)" is invalid.
        // "WallBlock (13)" is invalid.
        // "WallBlock (14)" is invalid.
        // "WallBlock (15)" is invalid.
        // "WallBlock (16)" is invalid.
        // "WallBlock (17)" is invalid.
        // "WallBlock (2)" is invalid.
        // "WallBlock (3)" is invalid.
        // "WallBlock (4)" is invalid.
        // "WallBlock (5)" is invalid.
        // "WallBlock (6)" is invalid.
        // "WallBlock (7)" is invalid.
        // "WallBlock (8)" is invalid.
        // "WallBlock (9)" is invalid.
        // "Weapon List" is invalid.
        public const string WeaponAndMunitionController = "WeaponAndMunitionController";
        public const string WeaponGrip = "WeaponGrip";
        public const string Weapon_R = "Weapon_R";
        public const string Weapon_Rifle_1 = "Weapon_Rifle_1";
        public const string Weapon_Rifle_1_LOD1 = "Weapon_Rifle_1_LOD1";
        public const string Zoom = "Zoom";
        // "_DungeonDynamicChunks" is invalid.
        // "__DungeonInfinityChunk_Cave" is invalid.
        public const string art_1 = "art_1";
        public const string art_2 = "art_2";
        public const string art_3 = "art_3";
        public const string art_4 = "art_4";
        public const string art_5 = "art_5";
        public const string art_6 = "art_6";
        public const string ball_l = "ball_l";
        public const string ball_r = "ball_r";
        public const string battery = "battery";
        public const string bed = "bed";
        public const string big_screen = "big_screen";
        public const string blank_wall_A = "blank_wall_A";
        public const string blank_wall_B = "blank_wall_B";
        public const string blank_wall_C = "blank_wall_C";
        public const string blank_wall_D = "blank_wall_D";
        public const string bulletin_board_big = "bulletin_board_big";
        public const string bulletin_board_small = "bulletin_board_small";
        public const string bunk_bed = "bunk_bed";
        public const string cabinet = "cabinet";
        public const string cabinet_L = "cabinet_L";
        public const string cabinet_L_cutted = "cabinet_L_cutted";
        public const string calf_l = "calf_l";
        public const string calf_r = "calf_r";
        public const string clavicle_l = "clavicle_l";
        public const string clavicle_r = "clavicle_r";
        public const string cm = "cm";
        public const string column_end = "column_end";
        // "column_end (1)" is invalid.
        // "column_end (2)" is invalid.
        // "column_end (3)" is invalid.
        // "column_end (4)" is invalid.
        // "column_end (5)" is invalid.
        public const string column_kneel = "column_kneel";
        public const string column_kneel_2 = "column_kneel_2";
        public const string column_middle = "column_middle";
        public const string computer_station = "computer_station";
        public const string console = "console";
        public const string console_celing = "console_celing";
        public const string console_screen = "console_screen";
        public const string container_big = "container_big";
        public const string container_small = "container_small";
        public const string corner = "corner";
        // "corner (1)" is invalid.
        // "corner (2)" is invalid.
        // "corner (3)" is invalid.
        public const string critique = "critique";
        public const string decorative_chair = "decorative_chair";
        public const string decorative_chair_LOD0 = "decorative_chair_LOD0";
        public const string decorative_chair_LOD1 = "decorative_chair_LOD1";
        public const string decorative_half_wall_1_LOD = "decorative_half_wall_1_LOD";
        // "decorative_half_wall_1_LOD (1)" is invalid.
        // "decorative_half_wall_1_LOD (10)" is invalid.
        // "decorative_half_wall_1_LOD (2)" is invalid.
        // "decorative_half_wall_1_LOD (3)" is invalid.
        // "decorative_half_wall_1_LOD (4)" is invalid.
        // "decorative_half_wall_1_LOD (5)" is invalid.
        // "decorative_half_wall_1_LOD (6)" is invalid.
        // "decorative_half_wall_1_LOD (7)" is invalid.
        // "decorative_half_wall_1_LOD (8)" is invalid.
        // "decorative_half_wall_1_LOD (9)" is invalid.
        public const string decorative_half_wall_1_LOD0 = "decorative_half_wall_1_LOD0";
        public const string decorative_half_wall_1_LOD1 = "decorative_half_wall_1_LOD1";
        public const string decorative_half_wall_2_LOD = "decorative_half_wall_2_LOD";
        public const string decorative_half_wall_2_LOD0 = "decorative_half_wall_2_LOD0";
        public const string decorative_half_wall_2_LOD1 = "decorative_half_wall_2_LOD1";
        public const string decorative_half_wall_3_LOD = "decorative_half_wall_3_LOD";
        public const string decorative_half_wall_3_LOD0 = "decorative_half_wall_3_LOD0";
        public const string decorative_half_wall_3_LOD1 = "decorative_half_wall_3_LOD1";
        public const string decorative_half_wall_4_LOD = "decorative_half_wall_4_LOD";
        public const string decorative_half_wall_4_LOD0 = "decorative_half_wall_4_LOD0";
        public const string decorative_half_wall_4_LOD1 = "decorative_half_wall_4_LOD1";
        public const string decorative_half_wall_5 = "decorative_half_wall_5";
        public const string decorative_half_wall_6 = "decorative_half_wall_6";
        public const string decorative_pilow = "decorative_pilow";
        public const string decorative_pilow_LOD0 = "decorative_pilow_LOD0";
        public const string decorative_pilow_LOD1 = "decorative_pilow_LOD1";
        public const string decorative_plant = "decorative_plant";
        public const string decorative_plant_desk = "decorative_plant_desk";
        public const string decorative_plant_small = "decorative_plant_small";
        public const string decorative_sofa = "decorative_sofa";
        public const string decorative_sofa_LOD0 = "decorative_sofa_LOD0";
        public const string decorative_sofa_LOD1 = "decorative_sofa_LOD1";
        public const string decorative_table_glass = "decorative_table_glass";
        public const string decorative_table_no_glass = "decorative_table_no_glass";
        public const string decorative_table_no_glass_only_top = "decorative_table_no_glass_only_top";
        public const string decorative_table_small_glass = "decorative_table_small_glass";
        public const string decorative_table_small_no_glass = "decorative_table_small_no_glass";
        public const string decorative_wall_1 = "decorative_wall_1";
        // "decorative_wall_1 (1)" is invalid.
        public const string decorative_wall_1_LOD0 = "decorative_wall_1_LOD0";
        public const string decorative_wall_1_LOD1 = "decorative_wall_1_LOD1";
        public const string decorative_wall_2 = "decorative_wall_2";
        // "decorative_wall_2 (1)" is invalid.
        public const string decorative_wall_2_LOD0 = "decorative_wall_2_LOD0";
        public const string decorative_wall_2_LOD1 = "decorative_wall_2_LOD1";
        public const string decorative_wall_3 = "decorative_wall_3";
        public const string decorative_wall_3_LOD0 = "decorative_wall_3_LOD0";
        public const string decorative_wall_3_LOD1 = "decorative_wall_3_LOD1";
        public const string decorative_wall_4_computer = "decorative_wall_4_computer";
        public const string decorative_wall_4_computer_LOD0 = "decorative_wall_4_computer_LOD0";
        public const string decorative_wall_4_computer_LOD1 = "decorative_wall_4_computer_LOD1";
        public const string decorative_wall_4_no_computer = "decorative_wall_4_no_computer";
        public const string decorative_wall_4_no_computer_LOD0 = "decorative_wall_4_no_computer_LOD0";
        public const string decorative_wall_4_no_computer_LOD1 = "decorative_wall_4_no_computer_LOD1";
        public const string decorative_wall_5 = "decorative_wall_5";
        public const string decorative_wall_6 = "decorative_wall_6";
        public const string decorative_wall_E = "decorative_wall_E";
        public const string decorative_wall_F = "decorative_wall_F";
        public const string decorative_wall_G = "decorative_wall_G";
        public const string decorative_wall_H = "decorative_wall_H";
        public const string desk = "desk";
        public const string desk_no_computer = "desk_no_computer";
        public const string door_1 = "door_1";
        public const string door_1_left = "door_1_left";
        public const string door_1_right = "door_1_right";
        public const string door_2 = "door_2";
        public const string door_2_left = "door_2_left";
        public const string door_2_right = "door_2_right";
        public const string door_3 = "door_3";
        public const string door_3_bottom_A = "door_3_bottom_A";
        public const string door_3_bottom_B = "door_3_bottom_B";
        public const string door_3_top_A = "door_3_top_A";
        public const string door_3_top_B = "door_3_top_B";
        public const string double_bed = "double_bed";
        public const string floor_1 = "floor_1";
        public const string floor_1_LOD0 = "floor_1_LOD0";
        public const string floor_1_LOD1 = "floor_1_LOD1";
        public const string floor_2 = "floor_2";
        // "floor_2 (2)" is invalid.
        // "floor_2 (3)" is invalid.
        // "floor_2 (4)" is invalid.
        public const string floor_2_L = "floor_2_L";
        public const string floor_2_LOD0 = "floor_2_LOD0";
        public const string floor_2_LOD1 = "floor_2_LOD1";
        public const string floor_2_L_LOD0 = "floor_2_L_LOD0";
        public const string floor_2_L_LOD1 = "floor_2_L_LOD1";
        public const string floor_2_blank = "floor_2_blank";
        public const string floor_2_no_lattice = "floor_2_no_lattice";
        // "floor_2_no_lattice (1)" is invalid.
        // "floor_2_no_lattice (2)" is invalid.
        // "floor_2_no_lattice (3)" is invalid.
        // "floor_2_no_lattice (4)" is invalid.
        public const string floor_2_no_lattice_LOD0 = "floor_2_no_lattice_LOD0";
        public const string floor_2_no_lattice_LOD1 = "floor_2_no_lattice_LOD1";
        public const string floor_3 = "floor_3";
        public const string floor_3_LOD0 = "floor_3_LOD0";
        public const string floor_3_LOD1 = "floor_3_LOD1";
        public const string floor_3_no_clamps = "floor_3_no_clamps";
        public const string floor_4 = "floor_4";
        public const string floor_5 = "floor_5";
        // "floor_5 (1)" is invalid.
        // "floor_5 (2)" is invalid.
        // "floor_5 (3)" is invalid.
        // "floor_5 (4)" is invalid.
        // "floor_5 (5)" is invalid.
        // "floor_5 (6)" is invalid.
        // "floor_5 (7)" is invalid.
        // "floor_5 (8)" is invalid.
        // "floor_5 (9)" is invalid.
        public const string floor_6 = "floor_6";
        // "floor_6 (1)" is invalid.
        // "floor_6 (2)" is invalid.
        // "floor_6 (3)" is invalid.
        // "floor_6 (4)" is invalid.
        public const string floor_corner_ornament = "floor_corner_ornament";
        public const string floor_corner_ornament_pipes = "floor_corner_ornament_pipes";
        public const string floor_corner_ornament_pipes_corner = "floor_corner_ornament_pipes_corner";
        public const string foot_l = "foot_l";
        public const string foot_r = "foot_r";
        public const string generator = "generator";
        // "generator (1)" is invalid.
        // "generator 1" is invalid.
        public const string glass_panel_1 = "glass_panel_1";
        // "glass_panel_1 (1)" is invalid.
        public const string glass_panel_1_corner = "glass_panel_1_corner";
        // "glass_panel_1_corner (1)" is invalid.
        // "glass_panel_1_corner (2)" is invalid.
        // "glass_panel_1_corner (3)" is invalid.
        public const string glass_panel_1_door = "glass_panel_1_door";
        public const string glass_panel_1_with_door = "glass_panel_1_with_door";
        public const string hand_l = "hand_l";
        public const string hand_r = "hand_r";
        public const string head = "head";
        public const string head1 = "head1";
        public const string hologram_LOD0 = "hologram_LOD0";
        public const string hologram_LOD1 = "hologram_LOD1";
        public const string hydroponic = "hydroponic";
        // "hydroponic 1" is invalid.
        public const string index_01_l = "index_01_l";
        public const string index_01_r = "index_01_r";
        public const string index_02_l = "index_02_l";
        public const string index_02_r = "index_02_r";
        public const string index_03_l = "index_03_l";
        public const string index_03_r = "index_03_r";
        public const string light_celing_1 = "light_celing_1";
        public const string light_celing_2 = "light_celing_2";
        public const string light_corner_1 = "light_corner_1";
        public const string light_corner_1_blue = "light_corner_1_blue";
        public const string light_wall_1 = "light_wall_1";
        public const string light_wall_1_blue = "light_wall_1_blue";
        public const string light_wall_2 = "light_wall_2";
        public const string light_wall_2_blue = "light_wall_2_blue";
        public const string light_wall_3 = "light_wall_3";
        public const string light_wall_3_blue = "light_wall_3_blue";
        public const string lowerarm_l = "lowerarm_l";
        public const string lowerarm_r = "lowerarm_r";
        public const string lowerarm_twist_01_l = "lowerarm_twist_01_l";
        public const string lowerarm_twist_01_r = "lowerarm_twist_01_r";
        public const string magazine_Right = "magazine_Right";
        public const string middle_01_l = "middle_01_l";
        public const string middle_01_r = "middle_01_r";
        public const string middle_02_l = "middle_02_l";
        public const string middle_02_r = "middle_02_r";
        public const string middle_03_l = "middle_03_l";
        public const string middle_03_r = "middle_03_r";
        public const string neck_01 = "neck_01";
        public const string nombreToucher = "nombreToucher";
        public const string oneWall = "oneWall";
        // "oneWall (1)" is invalid.
        public const string pelvis = "pelvis";
        public const string pilot_seat = "pilot_seat";
        public const string pinky_01_l = "pinky_01_l";
        public const string pinky_01_r = "pinky_01_r";
        public const string pinky_02_l = "pinky_02_l";
        public const string pinky_02_r = "pinky_02_r";
        public const string pinky_03_l = "pinky_03_l";
        public const string pinky_03_r = "pinky_03_r";
        public const string projector = "projector";
        // "projector stars" is invalid.
        public const string projector_2 = "projector_2";
        public const string projector_LOD0 = "projector_LOD0";
        public const string projector_LOD1 = "projector_LOD1";
        public const string ring_01_l = "ring_01_l";
        public const string ring_01_r = "ring_01_r";
        public const string ring_02_l = "ring_02_l";
        public const string ring_02_r = "ring_02_r";
        public const string ring_03_l = "ring_03_l";
        public const string ring_03_r = "ring_03_r";
        public const string root = "root";
        public const string rotor = "rotor";
        public const string sas = "sas";
        // "sas 1" is invalid.
        public const string shelf = "shelf";
        public const string shelf_small = "shelf_small";
        public const string shield_generator = "shield_generator";
        public const string shotgun_pump = "shotgun_pump";
        public const string shotgun_trigger = "shotgun_trigger";
        public const string sign = "sign";
        public const string sing_small = "sing_small";
        public const string spine_01 = "spine_01";
        public const string spine_02 = "spine_02";
        public const string spine_03 = "spine_03";
        public const string stairs_big_with_emmision = "stairs_big_with_emmision";
        // "stairs_big_with_emmision (1)" is invalid.
        public const string stairs_small = "stairs_small";
        public const string stairs_small_corner_no_emmision = "stairs_small_corner_no_emmision";
        public const string stairs_small_corner_with_emmision = "stairs_small_corner_with_emmision";
        public const string stairs_small_no_emmision = "stairs_small_no_emmision";
        public const string stairs_small_with_emmision = "stairs_small_with_emmision";
        public const string storage_container_big = "storage_container_big";
        // "storage_container_big (1)" is invalid.
        // "storage_container_big (10)" is invalid.
        // "storage_container_big (11)" is invalid.
        // "storage_container_big (12)" is invalid.
        // "storage_container_big (13)" is invalid.
        // "storage_container_big (14)" is invalid.
        // "storage_container_big (15)" is invalid.
        // "storage_container_big (16)" is invalid.
        // "storage_container_big (17)" is invalid.
        // "storage_container_big (18)" is invalid.
        // "storage_container_big (19)" is invalid.
        // "storage_container_big (2)" is invalid.
        // "storage_container_big (20)" is invalid.
        // "storage_container_big (21)" is invalid.
        // "storage_container_big (22)" is invalid.
        // "storage_container_big (23)" is invalid.
        // "storage_container_big (24)" is invalid.
        // "storage_container_big (25)" is invalid.
        // "storage_container_big (26)" is invalid.
        // "storage_container_big (27)" is invalid.
        // "storage_container_big (3)" is invalid.
        // "storage_container_big (4)" is invalid.
        // "storage_container_big (5)" is invalid.
        // "storage_container_big (6)" is invalid.
        // "storage_container_big (7)" is invalid.
        // "storage_container_big (8)" is invalid.
        // "storage_container_big (9)" is invalid.
        public const string storage_container_small = "storage_container_small";
        public const string table_2 = "table_2";
        public const string text = "text";
        public const string thigh_l = "thigh_l";
        public const string thigh_r = "thigh_r";
        public const string thumb_01_l = "thumb_01_l";
        public const string thumb_01_r = "thumb_01_r";
        public const string thumb_02_l = "thumb_02_l";
        public const string thumb_02_r = "thumb_02_r";
        public const string thumb_03_l = "thumb_03_l";
        public const string thumb_03_r = "thumb_03_r";
        public const string torpedo = "torpedo";
        // "torpedo (1)" is invalid.
        // "torpedo (10)" is invalid.
        // "torpedo (11)" is invalid.
        // "torpedo (2)" is invalid.
        // "torpedo (3)" is invalid.
        // "torpedo (4)" is invalid.
        // "torpedo (5)" is invalid.
        // "torpedo (6)" is invalid.
        // "torpedo (7)" is invalid.
        // "torpedo (8)" is invalid.
        // "torpedo (9)" is invalid.
        public const string torpedo_docking_station_1_side = "torpedo_docking_station_1_side";
        // "torpedo_docking_station_1_side (1)" is invalid.
        // "torpedo_docking_station_1_side (2)" is invalid.
        // "torpedo_docking_station_1_side (3)" is invalid.
        public const string torpedo_docking_station_2_side = "torpedo_docking_station_2_side";
        // "torpedo_docking_station_2_side (1)" is invalid.
        // "torpedo_docking_station_2_side (2)" is invalid.
        // "torpedo_docking_station_2_side (3)" is invalid.
        public const string upperarm_l = "upperarm_l";
        public const string upperarm_r = "upperarm_r";
        public const string vending_machine = "vending_machine";
        // "vending_machine 1" is invalid.
        public const string wall = "wall";
        public const string wallWithDoor = "wallWithDoor";
        // "wallWithDoor (1)" is invalid.
        public const string wall_big = "wall_big";
        public const string wall_big_LOD0 = "wall_big_LOD0";
        public const string wall_big_LOD1 = "wall_big_LOD1";
        public const string window_big = "window_big";
        // "window_big (1)" is invalid.
        // "window_big (2)" is invalid.
        // "window_big (3)" is invalid.
        // "window_big (4)" is invalid.
        public const string window_big_LOD0 = "window_big_LOD0";
        public const string window_big_LOD1 = "window_big_LOD1";
        public const string window_big_blocker = "window_big_blocker";
        public const string window_big_blocker_2 = "window_big_blocker_2";
        public const string window_big_blocker_3 = "window_big_blocker_3";
        // "window_big_complete (1)" is invalid.
        // "window_big_complete (2)" is invalid.
        // "window_big_complete (3)" is invalid.
        // "window_big_complete (4)" is invalid.
        public const string window_big_corner = "window_big_corner";
        // "window_big_corner (1)" is invalid.
        // "window_big_corner (2)" is invalid.
        // "window_big_corner (3)" is invalid.
        public const string window_big_corner_LOD0 = "window_big_corner_LOD0";
        public const string window_big_corner_LOD1 = "window_big_corner_LOD1";
        public const string window_big_corner_blocker = "window_big_corner_blocker";
        public const string window_big_corner_far = "window_big_corner_far";
        public const string window_big_corner_far_LOD0 = "window_big_corner_far_LOD0";
        public const string window_big_corner_far_LOD1 = "window_big_corner_far_LOD1";
        public const string window_big_corner_far_plug = "window_big_corner_far_plug";
        // "window_big_corner_far_plug (1)" is invalid.
        public const string window_big_corner_plug = "window_big_corner_plug";
        public const string window_big_no_LOD = "window_big_no_LOD";
        public const string window_big_plug = "window_big_plug";
        public const string window_big_supplement = "window_big_supplement";
        public const string window_small = "window_small";
        
    }
}