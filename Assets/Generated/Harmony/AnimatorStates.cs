// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-03-05 11:15

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class AnimatorStates
    {
        public const string Aim = "Aim";
        public const string Aiming = "Aiming";
        // "Change Gun" is invalid.
        public const string Dead = "Dead";
        public const string Death = "Death";
        public const string Death_A = "Death_A";
        public const string Empty = "Empty";
        public const string Find = "Find";
        public const string Fire = "Fire";
        public const string Fist = "Fist";
        public const string GetHit = "GetHit";
        public const string Grounded = "Grounded";
        public const string Hit = "Hit";
        public const string Hit_Reaction = "Hit_Reaction";
        public const string Idle = "Idle";
        public const string MeleeAttack1 = "MeleeAttack1";
        public const string MeleeAttack2 = "MeleeAttack2";
        public const string MeleeAttack3 = "MeleeAttack3";
        public const string MeleeAttack4 = "MeleeAttack4";
        public const string MeleeAttack5 = "MeleeAttack5";
        public const string MeleeAttack6 = "MeleeAttack6";
        // "New State" is invalid.
        public const string PickupObject = "PickupObject";
        public const string Reload_Rifle = "Reload_Rifle";
        public const string Roll = "Roll";
        public const string Salute = "Salute";
        public const string Shoot_Rifle = "Shoot_Rifle";
        public const string Sprint = "Sprint";
        // "Take Gun" is invalid.
        // "Turbine Animation" is invalid.
        public const string Turbine_1 = "Turbine_1";
        public const string Turbine_2 = "Turbine_2";
        public const string Use = "Use";
        public const string door_1_close = "door_1_close";
        public const string door_1_closed = "door_1_closed";
        public const string door_1_open = "door_1_open";
        public const string door_1_opened = "door_1_opened";
        public const string door_2_close = "door_2_close";
        public const string door_2_closed = "door_2_closed";
        public const string door_2_open = "door_2_open";
        public const string door_2_opened = "door_2_opened";
        public const string door_3_close = "door_3_close";
        public const string door_3_closed = "door_3_closed";
        public const string door_3_open = "door_3_open";
        public const string door_3_opened = "door_3_opened";
        public const string glass_door_close = "glass_door_close";
        public const string glass_door_closed = "glass_door_closed";
        public const string glass_door_open = "glass_door_open";
        public const string glass_door_opened = "glass_door_opened";
        
    }
}