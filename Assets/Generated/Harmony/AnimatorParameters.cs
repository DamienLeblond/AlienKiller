// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-03-05 11:15

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class AnimatorParameters
    {
        public static readonly int Aiming = Animator.StringToHash("Aiming");
        public static readonly int ChangeGun = Animator.StringToHash("ChangeGun");
        public static readonly int Crouch = Animator.StringToHash("Crouch");
        public static readonly int Dead = Animator.StringToHash("Dead");
        public static readonly int Fire = Animator.StringToHash("Fire");
        public static readonly int GetHit = Animator.StringToHash("GetHit");
        public static readonly int Hit = Animator.StringToHash("Hit");
        public static readonly int Idle = Animator.StringToHash("Idle");
        public static readonly int IsWeaponFist = Animator.StringToHash("IsWeaponFist");
        public static readonly int Jump = Animator.StringToHash("Jump");
        public static readonly int OnGround = Animator.StringToHash("OnGround");
        public static readonly int Pickup = Animator.StringToHash("Pickup");
        public static readonly int Reloading = Animator.StringToHash("Reloading");
        public static readonly int Roll = Animator.StringToHash("Roll");
        public static readonly int RunStop = Animator.StringToHash("RunStop");
        public static readonly int Shoot = Animator.StringToHash("Shoot");
        public static readonly int Speed = Animator.StringToHash("Speed");
        public static readonly int Sprint = Animator.StringToHash("Sprint");
        public static readonly int StopUse = Animator.StringToHash("StopUse");
        public static readonly int Uncover = Animator.StringToHash("Uncover");
        public static readonly int Use = Animator.StringToHash("Use");
        public static readonly int X = Animator.StringToHash("X");
        public static readonly int Y = Animator.StringToHash("Y");
        public static readonly int character_nearby = Animator.StringToHash("character_nearby");
        public static readonly int in_room_combat = Animator.StringToHash("in_room_combat");
        
    }
}